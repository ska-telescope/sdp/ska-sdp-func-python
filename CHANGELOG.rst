Changelog
=========

1.0.1
-----

- Fix WCS assisgnment 
  (`MR76 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/76>`__)

1.0.0
-----

- Use ska-ser-sphinx-theme for RTD documentation build
  (`MR74 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/74>`__)
- Updated dependencies, including ska-sdp-datamodels 1.0.0 (for list, see pyproject.toml on the MR;
  `MR74 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/74>`__)
- xradio-compatible invert interfaces updated and code refactored
  (`MR71 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/71>`__,
  `MR73 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/73>`__)
- Preserve xarray.DataArray attributes when subtracting visibilities
  (`MR72 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/72>`__)
- xradio-compatible invert distributes in time, frequency, and
  polarization
  (`MR70 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/70>`__)
- Allow W-towers gridder in xradio-compatible invert function
  (`MR69 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/69>`__)
- Added first version of imaging functions for xradio Datasets
  (`MR68 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/68>`__)
- Update some test functions from UnitTest to PyTest Framework. Add new
  test scripts for Adaptive Optics
  (`MR67 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/67>`__)

0.5.1
-----

- Add functions for downloading primary beam FITS file from Gitlab
  Repository
  (`MR66 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/66>`__)

.. _section-1:

0.5.0
-----

- Migrate primary beam related functions from rascil-main
  (`MR62 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/62>`__)
- Add functions in image operation to support Imaging-QA
  (`MR62 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/62>`__)
- Added MSv4 processing function for converting between visibility
  polarization representations
  (`MR63 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/63>`__)
- Added MSv4 processing functions for continuum subtraction and uvlin
  (`MR55 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/55>`__)
- Add flagger and bugfix in averaging and rfi masking
  (`MR61 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/61>`__)

.. _section-2:

0.4.1
-----

- Upgrade to xarray 2024.7 and Scipy 1.14
  (`MR56 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/56>`__)

.. _section-3:

0.4.0
-----

- Update ska-sdp-datamodels to 0.3.0, astropy to 6.1, and other
  dependencies
  (`MR54 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/54>`__)
- Fix using Visibility imaging_weight and uvw_lambda attributes
  (`MR54 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/54>`__)
- Added alternative polarised gain solvers for solve_gaintable
  (`MR48 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/48>`__)
- Added preprocessing functions including averaging and RFI masking
  (`MR53 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/53>`__)
- Replace old GPU gridder in wg.py with ska-sdp-func GPU gridder; Fixes
  Issues #1, #2, #4
  (`MR51 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/51>`__)
- Added function solve_ionosphere which solves for ionospheric delays
  (`MR33 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/33>`__)

.. _section-4:

0.3.2
-----

- Bugfix for apply_gaintable for npol=1 and nchan>1
  (`MR47 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/47>`__)

.. _section-5:

0.3.1
-----

- Fix the usage of SourceCatalog in Photutils>1.9
  (`MR46 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/46>`__)
- Fix apply_gaintable for multiple channels
  (`MR44 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/44>`__)

.. _section-6:

0.3.0
-----

- Add the function dp3_predict which wraps DP3 predict steps
  (`MR39 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/39>`__)
- Add functions process_visibilities and create_dpinfo to ease the use
  of DP3 steps
  (`MR38 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/38>`__)

.. _section-7:

0.2.4
-----

- Update xarray to newest version and resolved dependencies
  (`MR37 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/37>`__)

.. _section-8:

0.2.3
-----

- Added searching best reference antenna for gain calibration
  (`MR36 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/36>`__)

.. _section-9:

0.2.2
-----

- Fixing partial gain solver errors
  (`MR34 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/34>`__)

.. _section-10:

0.2.1
-----

- Fixing function inputs in chain_calibration.py
  (`MR31 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/31>`__)
- Added utility functions to support rechannelisation of bandpass and
  delay solutions for CBF beamformer calibration
  (`MR29 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/29>`__)

.. _section-11:

0.2.0
-----

- Fix mapping of calibration techniques between rascil and DP3, add more
  extensive tests
  (`MR30 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/30>`__)

.. _section-12:

0.1.5
-----

- Bug fix for calculating f2 of multi-channels
  (`MR26 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/26>`__)

.. _section-13:

0.1.4
-----

- Migrate function to perform DP3 calibration from rascil-main
  (`MR24 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/24>`__)
- Bug fix for weight_visibility function modified input visibility
  (`MR23 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/23>`__)

.. _section-14:

0.1.3
-----

- Bug fix for calculating weighing with conjugate visibilities
  (`MR20 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/20>`__)
- Add function expand_polarizations, needed to add the option of
  calibration with DP3 in rascil
  (`MR22 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/22>`__)

.. _section-15:

0.1.2
-----

- Refactored various functions related to deconvolution and calibration
  to decrease complexity
  (`MR12 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/12>`__)
- Fixed some bugs and tests for functions with image creation,
  apply_gaintable and advise_wide_field
  (`MR11 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/11>`__,
  `MR14 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/14>`__,
  `MR15 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/15>`__)
- Reviewed and updated processing functions documentation
  (`MR8 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/8>`__,
  `MR13 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/13>`__)
- Rearranged files and renamed directories
  (`MR4 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/4>`__,
  `MR5 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/5>`__,
  `MR7 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/7>`__)
- Migrated processing functions from RASCIL
  (`MR2 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/2>`__,
  `MR6 <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python/-/merge_requests/6>`__)
