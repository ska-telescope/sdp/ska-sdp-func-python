.. _dp3:

DP3 support
===========

This repository offers the possibility to use 
`DP3 <https://dp3.readthedocs.io/en/latest/>`__ steps with a
:external+ska-sdp-datamodels:doc:`Visibility <api/ska_sdp_datamodels.visibility.Visibility>`
object as input/output.

Any step can be used via the function :py:func:`ska_sdp_func_python.util.dp3_utils.process_visibilities`,
where the desired step is given via the ``step`` parameter. Wrappers are defined for the DP3
Predict and Gaincal steps.

Some examples are reported in the  :ref:`Usage examples <dp3_usage>` section.
