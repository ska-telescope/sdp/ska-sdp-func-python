# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "SDP Python Processing Functions"
copyright = "2022-2025 SKA SDP Developers"
author = "SKA SDP Developers"
version = "1.0.1"
release = "1.0.1"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx_automodapi.automodapi",
    "sphinx_automodapi.smart_resolver",
]

exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"

# -- Extension configuration -------------------------------------------------

intersphinx_mapping = {
    "ska-sdp-datamodels": (
        "https://developer.skao.int/projects/ska-sdp-datamodels/en/latest/",
        None,
    ),
    "xradio": ("https://xradio.readthedocs.io/en/latest/", None),
    "xarray": ("https://docs.xarray.dev/en/latest/", None),
}
