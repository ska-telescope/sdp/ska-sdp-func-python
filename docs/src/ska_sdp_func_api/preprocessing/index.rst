.. _ska_sdp_func_python_preprocessing:

.. py:currentmodule:: ska_sdp_func_python.preprocessing

Preprocessing
*************

.. toctree::
  :maxdepth: 3

.. automodapi::    ska_sdp_func_python.preprocessing.averaging
  :no-inheritance-diagram:

.. automodapi::    ska_sdp_func_python.preprocessing.rfi_masks
  :no-inheritance-diagram:

.. automodapi::    ska_sdp_func_python.preprocessing.flagger
  :no-inheritance-diagram:
