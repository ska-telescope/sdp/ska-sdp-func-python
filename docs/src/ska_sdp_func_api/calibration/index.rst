.. _ska_sdp_func_python_calibration:

.. py:currentmodule:: ska_sdp_func_python.calibration

Calibration
***********

Calibration is performed by fitting observed visibilities to a model visibility.

The scalar equation to be minimised is:

.. math:: S = \sum_{t,f}^{}{\sum_{i,j}^{}{w_{t,f,i,j}\left| V_{t,f,i,j}^{\text{obs}} - J_{i}{J_{j}^{*}V}_{t,f,i,j}^{\text{mod}} \right|}^{2}}

The least squares fit algorithm uses an iterative substitution (or relaxation) algorithm from Larry D'Addario in the
late seventies.

An extension of this approach using 2x2 antenna-based Jones matrices
:math:`J_i` and coherency matrices :math:`V_{t,f,i,j}` has been adapted from
the MWA RealTime System (Mitchell et at., 2008, IEEE JSTSP, 2,
JSTSP.2008.2005327) and can be enabled with
:py:func:`ska_sdp_func_python.calibration.solvers.solve_gaintable` option
``solver="jones_substitution"``:

.. math:: S = \sum_{t,f}^{}{\sum_{i,j}^{}{w_{t,f,i,j}\left| V_{t,f,i,j}^{\text{obs}} - J_{i}V_{t,f,i,j}^{\text{mod}}J_{j}^{\dagger} \right|_F}^{2}}.

:math:`\left|A\right|_F^2` is the squared Frobenius norm of matrix A, and
within each solver iteration each Jones matrix undergoes independent
optimisation relative to the others. Normal-equation-based solvers are also
available, which carry out joint optimisation within each iteration. These
are more computationally intensive but can have better convergence properties.

.. toctree::
  :maxdepth: 3

.. automodapi::    ska_sdp_func_python.calibration.chain_calibration
  :no-inheritance-diagram:

.. automodapi::    ska_sdp_func_python.calibration.ionosphere_solvers
  :no-inheritance-diagram:

.. automodapi::    ska_sdp_func_python.calibration.ionosphere_utils
  :no-inheritance-diagram:

.. automodapi::    ska_sdp_func_python.calibration.jones
  :no-inheritance-diagram:

.. automodapi::    ska_sdp_func_python.calibration.operations
  :no-inheritance-diagram:

.. automodapi::    ska_sdp_func_python.calibration.solvers
  :no-inheritance-diagram:

.. automodapi::    ska_sdp_func_python.calibration.alternative_solvers
  :no-inheritance-diagram:

.. automodapi::    ska_sdp_func_python.calibration.solver_utils
  :no-inheritance-diagram:

