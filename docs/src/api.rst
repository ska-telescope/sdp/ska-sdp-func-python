.. _api:

API
===

.. toctree::
   :maxdepth: 1
   :caption: Python-based Processing Functions

   ska_sdp_func_api/index.rst
   ska_sdp_func_api/index_xradio.rst


Indexes
+++++++

* :ref:`genindex`
* :ref:`modindex`
