

Design
======

The processing function library implements common functionality
required for the implementation of radio astronomy (specifically
interferometry) processing for the SKAO.

Guinding principles:

- We are taking a functional paradigm, which means that we have a
  number of functions that from inputs generate outputs in a
  deterministic fashion (referential transparency).

- Functions should always be built as far as possible so they can work
  on subsets of data in isolation. This is to enable users to
  distribute processing. The processing function library does not in
  itself implement distribution (outside of supporting Dask arrays).

Processing functions are defined for two
different groups of data models:

SDP data models
***************

See :ref:`ska_sdp_func_api`

The original processing function set developed as part of the SKA SDP
consortium work part of RASCIL used SKA SDP specific data models, see
:py:mod:`ska_sdp_datamodels`. This is using :py:class:`xarray.Dataset`
behind wrapper classes to represent data.

xradio data models
******************

See :ref:`ska_sdp_func_api_xradio`

New "xradio" data models defined later in a collaboration between SKAO
and NRAO (see :ref:`/overview.ipynb#xradio-schemas`). Envisioned to
become a new shared standard for radio astronomy data, used by
next-generation pipelines developed for new radio astronomy
observatories. As such, it is much more general than the SDP data
models - for instance, it also caters towards single dish and VLBI use
cases, which will not all be supported in ``ska-sdp-func-python``.

.. note::
   Both the ``xradio`` data model definitions and the
   processing function implementations are still subject to active
   development, and therefore not quite stabilised yet.
