"""
Function for applying RFI masks to the flags
"""

__all__ = [
    "apply_rfi_masks",
]

import logging

import numpy as np
from ska_sdp_datamodels.visibility.vis_model import Visibility

logger = logging.getLogger(__name__)


def apply_rfi_masks(data: Visibility, rfi_frequency_masks=None) -> Visibility:
    """
    Function to convert RFI masks presented in the form of frequency ranges
    into channel indices and apply them to flags

    :param data: SKA data model visibility
    :param rfi_frequency_masks: n by 2 array including n pairs
           giving the frequency ranges (Hz) to be masked
    :return: updated visibility with flags
    """

    if rfi_frequency_masks is None or len(rfi_frequency_masks) == 0:
        logger.warning("No mask was provided")
    else:
        if (
            rfi_frequency_masks.shape[1] == 1
            or rfi_frequency_masks.shape[1] > 2
            or rfi_frequency_masks.ndim > 2
        ):
            logger.error("RFI masks should be an array of size n by 2")
        else:
            num_masks = rfi_frequency_masks.shape[0]
            flags = data["flags"].values.astype(bool)
            chan_freq = data["frequency"].values

            out_of_range = False

            for i in range(num_masks):
                mask_range_down = np.where(
                    chan_freq >= rfi_frequency_masks[i, 0]
                )[0]
                mask_range_up = np.where(
                    chan_freq <= rfi_frequency_masks[i, 1]
                )[0]

                if mask_range_down.size == 0 or mask_range_up.size == 0:
                    out_of_range = True
                else:
                    flags[
                        :, :, mask_range_down[0] : mask_range_up[-1] + 1, :
                    ] = True
            if out_of_range:
                logger.warning(
                    "some masks are fully outside of the channel frequencies"
                )

            data["flags"].values = flags
    return data
