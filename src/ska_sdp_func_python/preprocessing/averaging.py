# pylint: disable=protected-access
"""
Functions for averaging visibilities and other affected quantities:
In case of averaging in frequency direction flags, weights,
channel frequencies, channel weights should be processed accordingly
In case of averaging in time direction flags, weights, time,
integration time, uvw's should be processed accordingly.
"""

__all__ = [
    "averaging_frequency",
    "averaging_time",
]

import logging

import numpy
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility.vis_model import Visibility

log = logging.getLogger("func-python-logger")


def averaging_frequency(
    data: Visibility, freqstep, flag_threshold=0.5
) -> Visibility:
    """
    Averaging in the frequency direction.
    New visibilities are the summation of the unflagged
    old ones with weights taken into account divided by
    the sum of unflagged weights.
    New weights are the sum of the old ones.
    New flags are True if the number of old True flags are above a
    threshold (within the bin of freqstep data points).
    New channel frequencies are the averages of the old ones.
    New bandwidths are the sums of the old ones.

    :param data: SKA data model visibility
    :param freqstep: integer, number of channels to average
    :param flag_threshold: the threshold on the fraction of old
           flags (in the bin of size freqstep) to flag the new (averaged)
           data point (should be between 0 and 1)
    :return: resulting (averaged) SKA data model visibility
    """
    vis = data["vis"].values.astype(numpy.complex64)
    flags = data["flags"].values.astype(bool)
    weights = data["weight"].values.astype(numpy.float32)
    uvw = data["uvw"].values
    chan_freq = data["frequency"].values
    chan_width = data["channel_bandwidth"].values
    time = data["time"].values
    phase_centre = data.phasecentre

    num_times = vis.shape[0]
    num_baselines = vis.shape[1]
    num_freqs = vis.shape[2]
    num_pols = vis.shape[3]

    flagged_weights = numpy.multiply(
        numpy.logical_not(flags).astype(int), weights
    )
    flagged_weighted_vis = numpy.multiply(vis, flagged_weights)

    quotient = num_freqs // freqstep
    mod = num_freqs % freqstep
    many_flags = int(flag_threshold * freqstep)
    many_flags_mod = int(flag_threshold * mod)
    vis_q = flagged_weighted_vis[:, :, 0 : quotient * freqstep, :]

    # two cases where the number of channels is divisble
    # and non-divisble by freqstep are considered in the
    # following.
    # _q in the variable's name represents the quotient part
    # the channels.
    # _m in the variable's name represents the remainder of
    # the channels.
    # _freq in the variable's name refers to averaging in the
    # frequency direction

    if mod == 0:
        weights_avg_freq = numpy.sum(
            flagged_weights.reshape(
                num_times, num_baselines, quotient, freqstep, num_pols
            ),
            axis=3,
        )
        sum_vis = numpy.sum(
            flagged_weighted_vis.reshape(
                num_times, num_baselines, quotient, freqstep, num_pols
            ),
            axis=3,
        )
        vis_avg_frq = numpy.divide(
            sum_vis,
            weights_avg_freq,
            out=numpy.zeros_like(sum_vis),
            where=weights_avg_freq != 0,
        )
        flags_avg_frq = (
            numpy.where(
                numpy.sum(
                    (flags.astype(int)).reshape(
                        num_times, num_baselines, quotient, freqstep, num_pols
                    ),
                    axis=3,
                )
                >= many_flags,
                1,
                0,
            )
        ).astype(bool)

        chan_freq_avg_freq = numpy.average(
            chan_freq.reshape(quotient, freqstep), axis=1
        )
        chan_width_avg_freq = numpy.sum(
            chan_width.reshape(quotient, freqstep), axis=1
        )

    if mod != 0:
        log.warning(
            "The number of channels is not divisible by freqstep. \
            Therefore the last channel will have a different \
            bandwidth from the rest of channels"
        )
        vis_q = flagged_weighted_vis[:, :, 0 : quotient * freqstep, :]
        vis_mod = flagged_weighted_vis[
            :, :, quotient * freqstep : num_freqs, :
        ]
        flags_q = flags[:, :, 0 : quotient * freqstep, :]
        flags_mod = flags[:, :, quotient * freqstep : num_freqs, :]
        weights_q = flagged_weights[:, :, 0 : quotient * freqstep, :]
        weights_mod = flagged_weights[:, :, quotient * freqstep : num_freqs, :]

        weights_avg_freq_q = numpy.sum(
            weights_q[:, :, 0 : quotient * freqstep].reshape(
                num_times, num_baselines, quotient, freqstep, num_pols
            ),
            axis=3,
        )
        sum_vis = numpy.sum(
            vis_q.reshape(
                num_times, num_baselines, quotient, freqstep, num_pols
            ),
            axis=3,
        )

        vis_avg_frq_q = numpy.divide(
            sum_vis,
            weights_avg_freq_q,
            out=numpy.zeros_like(sum_vis),
            where=weights_avg_freq_q != 0,
        )
        flags_avg_frq_q = (
            numpy.where(
                numpy.sum(
                    (flags_q.astype(int)).reshape(
                        num_times, num_baselines, quotient, freqstep, num_pols
                    ),
                    axis=3,
                )
                >= many_flags,
                1,
                0,
            )
        ).astype(bool)

        chan_freq_avg_freq_q = numpy.average(
            chan_freq[0 : quotient * freqstep].reshape(quotient, freqstep),
            axis=1,
        )
        chan_width_avg_freq_q = numpy.sum(
            chan_width[0 : quotient * freqstep].reshape(quotient, freqstep),
            axis=1,
        )
        weights_avg_freq_m = numpy.sum(
            weights_mod[:, :, 0 : quotient * freqstep], axis=2
        ).reshape((num_times, num_baselines, 1, num_pols))
        sum_vis = numpy.sum(vis_mod, axis=2).reshape(
            (num_times, num_baselines, 1, num_pols)
        )
        vis_avg_frq_m = numpy.divide(
            sum_vis,
            weights_avg_freq_m,
            out=numpy.zeros_like(sum_vis),
            where=weights_avg_freq_m != 0,
        )

        flags_avg_frq_m = (
            numpy.where(
                numpy.sum((flags_mod.astype(int)), axis=2) >= many_flags_mod,
                1,
                0,
            ).reshape((num_times, num_baselines, 1, num_pols))
        ).astype(bool)

        chan_freq_avg_freq_m = numpy.average(
            chan_freq[quotient * freqstep : num_freqs]
        )
        chan_width_avg_freq_m = numpy.sum(
            chan_width[quotient * freqstep : num_freqs]
        )

        vis_avg_frq = numpy.concatenate((vis_avg_frq_q, vis_avg_frq_m), axis=2)
        flags_avg_frq = numpy.concatenate(
            (flags_avg_frq_q, flags_avg_frq_m), axis=2
        )
        weights_avg_freq = numpy.concatenate(
            (weights_avg_freq_q, weights_avg_freq_m), axis=2
        )
        chan_freq_avg_freq = numpy.concatenate(
            (chan_freq_avg_freq_q, numpy.array([chan_freq_avg_freq_m]))
        )
        chan_width_avg_freq = numpy.concatenate(
            (chan_width_avg_freq_q, numpy.array([chan_width_avg_freq_m]))
        )

    avg_visibility = Visibility.constructor(
        frequency=chan_freq_avg_freq,
        channel_bandwidth=chan_width_avg_freq,
        phasecentre=phase_centre,
        configuration=data.configuration,
        uvw=uvw,
        time=time,
        vis=vis_avg_frq,
        weight=weights_avg_freq,
        integration_time=data["integration_time"],
        flags=flags_avg_frq,
        baselines=data["baselines"],
        polarisation_frame=PolarisationFrame(data._polarisation_frame),
        source=data.attrs["source"],
        scan_id=data.attrs["scan_id"],
        scan_intent=data.attrs["scan_intent"],
        execblock_id=data.attrs["execblock_id"],
        meta=data.attrs["meta"],
    )

    return avg_visibility


def averaging_time(
    data: Visibility, timestep, flag_threshold=0.5
) -> Visibility:
    """
    Averaging in the time direction.
    New visibilities are the summation of the unflagged
    old ones with weights taken into account divided by
    the sum of unflagged weights.
    New weights are the sum of the old ones.
    New flags are True if the number of old True flags
    are above a threshold (within the bin of freqstep data points).
    New uvw's are the average of the old ones
    New time is the average of the old ones
    New integration time is the sum of the old ones

    :param data: SKA data model visibility
    :param timestep: integer, number of time samples to average
    :param flag_threshold: the threshold on the fraction of old
           flags (in the bin of size timestep) to flag the new (averaged)
           data point (should be between 0 and 1)
    :return: resulting (averaged) SKA data model visibility
    """
    vis = data["vis"].values.astype(numpy.complex64)
    flags = data["flags"].values.astype(numpy.bool_)
    weights = data["weight"].values.astype(numpy.float32)
    uvw = data["uvw"].values
    chan_freq = data["frequency"].values
    chan_width = data["channel_bandwidth"].values
    time = data["time"].values
    integration_time = data["integration_time"].values
    phasecentre = data.phasecentre

    num_times = vis.shape[0]
    num_baselines = vis.shape[1]
    num_freqs = vis.shape[2]
    num_pols = vis.shape[3]

    flagged_weights = numpy.multiply(
        numpy.logical_not(flags).astype(int), weights
    )
    flagged_weighted_vis = numpy.multiply(vis, flagged_weights)

    quotient = num_times // timestep
    mod = num_times % timestep
    many_flags = int(flag_threshold * timestep)
    many_flags_mod = int(flag_threshold * mod)
    vis_q = flagged_weighted_vis[0 : quotient * timestep, :, :, :]

    # two cases where the number of time samples is divisble
    # and non-divisble by timestep are considered in the
    # following.
    # _q in the variable's name represents the quotient part
    # the time samples.
    # _m in the variable's name represents the remainder of
    # the time samples.
    # _t in the variable's name refers to averaging in the
    # time direction

    if mod == 0:
        weights_avg_t = numpy.sum(
            flagged_weights.reshape(
                quotient, timestep, num_baselines, num_freqs, num_pols
            ),
            axis=1,
        )
        sum_vis = numpy.sum(
            flagged_weighted_vis.reshape(
                quotient, timestep, num_baselines, num_freqs, num_pols
            ),
            axis=1,
        )
        vis_avg_t = numpy.divide(
            sum_vis,
            weights_avg_t,
            out=numpy.zeros_like(sum_vis),
            where=weights_avg_t != 0,
        )
        flags_avg_t = (
            numpy.where(
                numpy.sum(
                    (flags.astype(int)).reshape(
                        quotient, timestep, num_baselines, num_freqs, num_pols
                    ),
                    axis=1,
                )
                >= many_flags,
                1,
                0,
            )
        ).astype(bool)

        uvw_avg_t = numpy.average(
            uvw.reshape(quotient, timestep, num_baselines, 3), axis=1
        )
        time_avg_t = numpy.average(time.reshape(quotient, timestep), axis=1)
        integration_time_avg_t = numpy.sum(
            integration_time.reshape(quotient, timestep), axis=1
        )

    if mod != 0:
        log.warning(
            "The number of time samples is not divisible by timestep. \
                Therefore the last time slot is shorter"
        )
        vis_q = flagged_weighted_vis[0 : quotient * timestep, :, :, :]
        vis_mod = flagged_weighted_vis[
            quotient * timestep : num_times, :, :, :
        ]
        flags_q = flags[0 : quotient * timestep, :, :, :]
        flags_mod = flags[quotient * timestep : num_times, :, :, :]
        weights_q = flagged_weights[0 : quotient * timestep, :, :, :]
        weights_mod = flagged_weights[quotient * timestep : num_times, :, :, :]
        uvw_q = uvw[0 : quotient * timestep, :, :]
        uvw_mod = uvw[quotient * timestep : num_times, :, :]
        time_q = time[0 : quotient * timestep]
        time_mod = time[quotient * timestep : num_times]
        integration_time_t_q = integration_time[0 : quotient * timestep]
        integration_time_t_mod = integration_time[
            quotient * timestep : num_times
        ]

        weights_avg_t_q = numpy.sum(
            weights_q.reshape(
                quotient, timestep, num_baselines, num_freqs, num_pols
            ),
            axis=1,
        )
        sum_vis = numpy.sum(
            vis_q.reshape(
                quotient, timestep, num_baselines, num_freqs, num_pols
            ),
            axis=1,
        )
        vis_avg_t_q = numpy.divide(
            sum_vis,
            weights_avg_t_q,
            out=numpy.zeros_like(sum_vis),
            where=weights_avg_t_q != 0,
        )
        flags_avg_t_q = (
            numpy.where(
                numpy.sum(
                    (flags_q.astype(int)).reshape(
                        quotient, timestep, num_baselines, num_freqs, num_pols
                    ),
                    axis=1,
                )
                >= many_flags,
                1,
                0,
            )
        ).astype(bool)

        uvw_avg_t_q = numpy.average(
            uvw_q.reshape(quotient, timestep, num_baselines, 3), axis=1
        )
        time_avg_t_q = numpy.average(
            time_q.reshape(quotient, timestep), axis=1
        )
        integration_time_avg_t_q = numpy.sum(
            integration_time_t_q.reshape(quotient, timestep), axis=1
        )

        weights_avg_t_m = numpy.sum(weights_mod, axis=0).reshape(
            (1, num_baselines, num_freqs, num_pols)
        )

        sum_vis = numpy.sum(vis_mod, axis=0).reshape(
            (1, num_baselines, num_freqs, num_pols)
        )
        vis_avg_t_m = numpy.divide(
            sum_vis,
            weights_avg_t_m,
            out=numpy.zeros_like(sum_vis),
            where=weights_avg_t_m != 0,
        )
        flags_avg_t_m = (
            numpy.where(
                numpy.sum((flags_mod.astype(int)), axis=0) >= many_flags_mod,
                1,
                0,
            ).reshape((1, num_baselines, num_freqs, num_pols))
        ).astype(bool)

        uvw_avg_t_m = numpy.average(uvw_mod, axis=0).reshape(
            1, num_baselines, 3
        )
        time_avg_t_m = numpy.average(time_mod)
        integration_time_avg_t_m = numpy.sum(integration_time_t_mod)

        vis_avg_t = numpy.concatenate((vis_avg_t_q, vis_avg_t_m), axis=0)
        flags_avg_t = numpy.concatenate((flags_avg_t_q, flags_avg_t_m), axis=0)
        weights_avg_t = numpy.concatenate(
            (weights_avg_t_q, weights_avg_t_m), axis=0
        )
        uvw_avg_t = numpy.concatenate((uvw_avg_t_q, uvw_avg_t_m), axis=0)
        time_avg_t = numpy.concatenate(
            (time_avg_t_q, numpy.array([time_avg_t_m]))
        )
        integration_time_avg_t = numpy.concatenate(
            (integration_time_avg_t_q, numpy.array([integration_time_avg_t_m]))
        )

    avg_visibility = Visibility.constructor(
        frequency=chan_freq,
        channel_bandwidth=chan_width,
        phasecentre=phasecentre,
        configuration=data.configuration,
        uvw=uvw_avg_t,
        time=time_avg_t,
        vis=vis_avg_t,
        weight=weights_avg_t,
        integration_time=integration_time_avg_t,
        flags=flags_avg_t,
        baselines=data["baselines"],
        polarisation_frame=PolarisationFrame(data._polarisation_frame),
        source=data.attrs["source"],
        scan_id=data.attrs["scan_id"],
        scan_intent=data.attrs["scan_intent"],
        execblock_id=data.attrs["execblock_id"],
        meta=data.attrs["meta"],
    )

    return avg_visibility
