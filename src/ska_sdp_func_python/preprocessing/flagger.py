"""
Function for flagging the visibilities using FluctuFlagger
"""

__all__ = [
    "rfi_flagger",
]

import numpy
from ska_sdp_datamodels.visibility.vis_model import Visibility


def rfi_flagger(
    data: Visibility,
    alpha=0.5,
    threshold_magnitude=3.5,
    threshold_variation=3.5,
    threshold_broadband=3.5,
    sampling=8,
    window=0,
    window_median_history=10,
) -> Visibility:
    """
    This is an interface for the dynamic flagger
    algorithm (FluctuFlagger) based in SKA Processing
    Functions Library (PFL) to directely use SKA
    data model as input and output.

    :param data: SKA data model visibility

    :param alpha: a value between 0 and 1 used in computation of
        the rate of fluctuations in the visibility magnitudes; the
        higher the value of alpha, the higher weight for the more
        recent 'rate of changes' in the visibility magnitudes.

    :param threshold_magnitude: threshold for the modified z-score
        (median-based) of visibility magnitudes to be flagged. The
        recommended value in statistical texts is 3.5.

    :param threshold_variation: threshold for the modified z-score
        of transit scores (see the description of parameter alpha);
        recommended value is 3.5

    :param threshold_broadband: threshold for the modified z-score of
        a window of medians of the visibility magnitudes in the
        recent time slots; recommended value is 3.5

    :param sampling: intervals at which samples from visibility
        magnitudes are taken for computing medians. This approximation
        reduces the cost of computation

    :param window: the number of channels to be flagged on each
        side of a flagged visibility by the statistical approaches
        above.

    :param window_median_history: window of time slots that the
        medians of the recent time samples are maintained (see the
        description of parameter threshold_broadband).

    :return: flagged SKA data model visibility
    """

    # pylint: disable=import-outside-toplevel
    try:
        from ska_sdp_func.visibility import flagger_dynamic_threshold
    except ModuleNotFoundError as err:
        raise ModuleNotFoundError("ska_sdp_func is not installed!") from err

    vis = numpy.ascontiguousarray(data["vis"].values, dtype=numpy.complex64)
    flags = numpy.ascontiguousarray(data["flags"].values, dtype=numpy.int32)

    flagger_dynamic_threshold(
        vis,
        flags,
        alpha,
        threshold_magnitude,
        threshold_variation,
        threshold_broadband,
        sampling,
        window,
        window_median_history,
    )

    data["flags"].values = flags.astype(bool)

    return data
