# pylint: disable=too-many-arguments, too-many-locals, too-many-lines

"""
Image operations visible to the Execution Framework as Components.
"""

__all__ = [
    "convert_clean_beam_to_degrees",
    "convert_clean_beam_to_pixels",
    "convert_polimage_to_stokes",
    "convert_stokes_to_polimage",
    "fft_image_to_griddata_with_wcs",
    "ifft_griddata_to_image",
    "pad_image",
    "scale_and_rotate_image",
    "reproject_image",
]

import copy
import logging
import warnings

import numpy
from astropy.wcs import WCS, FITSFixedWarning
from reproject import reproject_interp
from scipy.ndimage import affine_transform
from ska_sdp_datamodels.gridded_visibility import create_griddata_from_image
from ska_sdp_datamodels.image.image_model import Image
from ska_sdp_datamodels.science_data_model.polarisation_functions import (
    convert_circular_to_stokes,
    convert_linear_to_stokes,
    convert_stokes_to_circular,
    convert_stokes_to_linear,
)
from ska_sdp_datamodels.science_data_model.polarisation_model import (
    PolarisationFrame,
)

from ska_sdp_func_python.fourier_transforms import fft, ifft

warnings.simplefilter("ignore", FITSFixedWarning)
log = logging.getLogger("func-python-logger")


def convert_clean_beam_to_degrees(im, beam_pixels):
    """Convert clean beam in pixels to deg, deg, deg.

    :param im: Image
    :param beam_pixels: Beam size in pixels
    :return: dict e.g. {"bmaj":0.1, "bmin":0.05, "bpa":-60.0}.
             Units are deg, deg, deg
    """
    # cellsize in radians
    cellsize = numpy.deg2rad(im.image_acc.wcs.wcs.cdelt[1])
    to_mm = numpy.sqrt(8.0 * numpy.log(2.0))
    if beam_pixels[1] > beam_pixels[0]:
        clean_beam = {
            "bmaj": numpy.rad2deg(beam_pixels[1] * cellsize * to_mm),
            "bmin": numpy.rad2deg(beam_pixels[0] * cellsize * to_mm),
            "bpa": numpy.rad2deg(beam_pixels[2]),
        }
    else:
        clean_beam = {
            "bmaj": numpy.rad2deg(beam_pixels[0] * cellsize * to_mm),
            "bmin": numpy.rad2deg(beam_pixels[1] * cellsize * to_mm),
            "bpa": numpy.rad2deg(beam_pixels[2]) + 90.0,
        }
    return clean_beam


def convert_clean_beam_to_pixels(model, clean_beam):
    """Convert clean beam to pixels.

    :param model: Model image containing beam information
    :param clean_beam: e.g. {"bmaj":0.1, "bmin":0.05, "bpa":-60.0}.
                Units are deg, deg, deg
    :return: Beam size in pixels
    """
    to_mm = numpy.sqrt(8.0 * numpy.log(2.0))
    # Cellsize in radians
    cellsize = numpy.deg2rad(model.image_acc.wcs.wcs.cdelt[1])
    # Beam in pixels
    beam_pixels = (
        numpy.deg2rad(clean_beam["bmin"]) / (cellsize * to_mm),
        numpy.deg2rad(clean_beam["bmaj"]) / (cellsize * to_mm),
        numpy.deg2rad(clean_beam["bpa"]),
    )
    return beam_pixels


def convert_stokes_to_polimage(
    im: Image, polarisation_frame: PolarisationFrame
):
    """Convert a stokes image in IQUV to polarisation_frame.

    For example::
        impol = convert_stokes_to_polimage(imIQUV, PolarisationFrame('linear'))

    :param im: Image to be converted
    :param polarisation_frame: desired polarisation frame
    :returns: Complex image

    See also
        :py:func:`ska_sdp_func_python.image.operations.convert_polimage_to_stokes`
        :py:func:`ska_sdp_datamodels.polarisation.convert_circular_to_stokes`
        :py:func:`ska_sdp_datamodels.polarisation.convert_linear_to_stokes`
    """  # pylint: disable=line-too-long

    if polarisation_frame == PolarisationFrame("linear"):
        cimarr = convert_stokes_to_linear(im["pixels"].data)
        # Need to make sure image data is copied (cimarr in this case)
        return Image.constructor(
            data=cimarr,
            polarisation_frame=polarisation_frame,
            wcs=im.image_acc.wcs,
        )
    if polarisation_frame == PolarisationFrame("linearnp"):
        cimarr = convert_stokes_to_linear(im["pixels"].data)
        return Image.constructor(
            data=cimarr,
            polarisation_frame=polarisation_frame,
            wcs=im.image_acc.wcs,
        )
    if polarisation_frame == PolarisationFrame("circular"):
        cimarr = convert_stokes_to_circular(im["pixels"].data)
        return Image.constructor(
            data=cimarr,
            polarisation_frame=polarisation_frame,
            wcs=im.image_acc.wcs,
        )
    if polarisation_frame == PolarisationFrame("circularnp"):
        cimarr = convert_stokes_to_circular(im["pixels"].data)
        return Image.constructor(
            data=cimarr,
            polarisation_frame=polarisation_frame,
            wcs=im.image_acc.wcs,
        )
    if polarisation_frame == PolarisationFrame("stokesI"):
        return Image.constructor(
            data=im["pixels"].data.astype("complex"),
            polarisation_frame=PolarisationFrame("stokesI"),
            wcs=im.image_acc.wcs,
        )

    raise ValueError(f"Cannot convert stokes to {polarisation_frame.type}")


def convert_polimage_to_stokes(im: Image, complex_image=False):
    """Convert a polarisation image to stokes IQUV (complex).

    For example:
        imIQUV = convert_polimage_to_stokes(impol)

    :param im: Complex Image in linear or circular
    :param complex_image: boolean for complex image, (default False)
    :returns: Complex or Real image

    See also
        :py:func:`ska_sdp_func_python.image.operations.convert_stokes_to_polimage`
        :py:func:`ska_sdp_datamodels.polarisation.convert_stokes_to_circular`
        :py:func:`ska_sdp_datamodels.polarisation.convert_stokes_to_linear`

    """  # pylint: disable=line-too-long
    assert im["pixels"].data.dtype == "complex", im["pixels"].data.dtype

    def _to_required(data):
        if complex_image:
            return data

        return numpy.real(data)

    if im.image_acc.polarisation_frame == PolarisationFrame("linear"):
        cimarr = convert_linear_to_stokes(im["pixels"].data)
        return Image.constructor(
            data=_to_required(cimarr),
            polarisation_frame=PolarisationFrame("stokesIQUV"),
            wcs=im.image_acc.wcs,
        )
    if im.image_acc.polarisation_frame == PolarisationFrame("linearnp"):
        cimarr = convert_linear_to_stokes(im["pixels"].data)
        return Image.constructor(
            data=_to_required(cimarr),
            polarisation_frame=PolarisationFrame("stokesIQ"),
            wcs=im.image_acc.wcs,
        )
    if im.image_acc.polarisation_frame == PolarisationFrame("circular"):
        cimarr = convert_circular_to_stokes(im["pixels"].data)
        return Image.constructor(
            data=_to_required(cimarr),
            polarisation_frame=PolarisationFrame("stokesIQUV"),
            wcs=im.image_acc.wcs,
        )
    if im.image_acc.polarisation_frame == PolarisationFrame("circularnp"):
        cimarr = convert_circular_to_stokes(im["pixels"].data)
        return Image.constructor(
            data=_to_required(cimarr),
            polarisation_frame=PolarisationFrame("stokesIV"),
            wcs=im.image_acc.wcs,
        )
    if im.image_acc.polarisation_frame == PolarisationFrame("stokesI"):
        return Image.constructor(
            data=_to_required(im["pixels"].data),
            polarisation_frame=PolarisationFrame("stokesI"),
            wcs=im.image_acc.wcs,
        )

    raise ValueError(
        f"Cannot convert {im.image_acc.polarisation_frame.type} to stokes"
    )


def fft_image_to_griddata_with_wcs(im):
    """WCS-aware FFT of a canonical image

    The only transforms supported are:
        RA--SIN, DEC--SIN <-> UU, VV
        XX, YY <-> KX, KY

    For example::

        import create_test_image, fft_image_to_griddata_with_wcs
        im = create_test_image()
        print(im)
            Image:
                Shape: (1, 1, 256, 256)
                WCS: WCS Keywords
            Number of WCS axes: 4
            CTYPE : 'RA---SIN'  'DEC--SIN'  'STOKES'  'FREQ'
            CRVAL : 0.0  35.0  1.0  100000000.0
            CRPIX : 129.0  129.0  1.0  1.0
            PC1_1 PC1_2 PC1_3 PC1_4  : 1.0  0.0  0.0  0.0
            PC2_1 PC2_2 PC2_3 PC2_4  : 0.0  1.0  0.0  0.0
            PC3_1 PC3_2 PC3_3 PC3_4  : 0.0  0.0  1.0  0.0
            PC4_1 PC4_2 PC4_3 PC4_4  : 0.0  0.0  0.0  1.0
            CDELT : -0.000277777791  0.000277777791  1.0  100000.0
            NAXIS : 0  0
                Polarisation frame: stokesI
        print(fft_image_to_griddata_with_wcs(im))
            Image:
                Shape: (1, 1, 256, 256)
                WCS: WCS Keywords
            Number of WCS axes: 4
            CTYPE : 'UU'  'VV'  'STOKES'  'FREQ'
            CRVAL : 0.0  0.0  1.0  100000000.0
            CRPIX : 129.0  129.0  1.0  1.0
            PC1_1 PC1_2 PC1_3 PC1_4  : 1.0  0.0  0.0  0.0
            PC2_1 PC2_2 PC2_3 PC2_4  : 0.0  1.0  0.0  0.0
            PC3_1 PC3_2 PC3_3 PC3_4  : 0.0  0.0  1.0  0.0
            PC4_1 PC4_2 PC4_3 PC4_4  : 0.0  0.0  0.0  1.0
            CDELT : -805.7218610503596  805.7218610503596  1.0  100000.0
            NAXIS : 0  0
                Polarisation frame: stokesI

    :param im:
    :return:

    See also
        :py:func:`ska_sdp_func_python.fourier_transforms.fft_support.fft`
        :py:func:`ska_sdp_func_python.fourier_transforms.fft_support.ifft`
    """
    assert im.attrs["data_model"] == "Image"

    assert len(im["pixels"].data.shape) == 4
    wcs = im.image_acc.wcs

    if wcs.wcs.ctype[0] == "RA---SIN" and wcs.wcs.ctype[1] == "DEC--SIN":
        ft_types = ["UU", "VV"]
    elif wcs.wcs.ctype[0] == "XX" and wcs.wcs.ctype[1] == "YY":
        ft_types = ["KX", "KY"]
    elif (
        wcs.wcs.ctype[0] == "AZELGEO long"
        and wcs.wcs.ctype[1] == "AZELGEO lati"
    ):
        ft_types = ["KX", "KY"]
    else:
        raise NotImplementedError(
            "Cannot FFT specified axes {wcs.wcs.ctype[0]}, {wcs.wcs.ctype[1]}"
        )

    gd = create_griddata_from_image(im, ft_types=ft_types)
    gd["pixels"].data = ifft(im["pixels"].data.astype("complex"))
    return gd


def ifft_griddata_to_image(gd, template):
    """WCS-aware IFFT of a canonical image

    The only transforms supported are:
        RA--SIN, DEC--SIN <-> UU, VV
        XX, YY <-> KX, KY

    For example::

        import
            create_test_image, fft_image_to_griddata
        im = create_test_image()
        print(im)
            Image:
                Shape: (1, 1, 256, 256)
                WCS: WCS Keywords
            Number of WCS axes: 4
            CTYPE : 'RA---SIN'  'DEC--SIN'  'STOKES'  'FREQ'
            CRVAL : 0.0  35.0  1.0  100000000.0
            CRPIX : 129.0  129.0  1.0  1.0
            PC1_1 PC1_2 PC1_3 PC1_4  : 1.0  0.0  0.0  0.0
            PC2_1 PC2_2 PC2_3 PC2_4  : 0.0  1.0  0.0  0.0
            PC3_1 PC3_2 PC3_3 PC3_4  : 0.0  0.0  1.0  0.0
            PC4_1 PC4_2 PC4_3 PC4_4  : 0.0  0.0  0.0  1.0
            CDELT : -0.000277777791  0.000277777791  1.0  100000.0
            NAXIS : 0  0
                Polarisation frame: stokesI
        print(fft_image_to_griddata(im))
            Image:
                Shape: (1, 1, 256, 256)
                WCS: WCS Keywords
            Number of WCS axes: 4
            CTYPE : 'UU'  'VV'  'STOKES'  'FREQ'
            CRVAL : 0.0  0.0  1.0  100000000.0
            CRPIX : 129.0  129.0  1.0  1.0
            PC1_1 PC1_2 PC1_3 PC1_4  : 1.0  0.0  0.0  0.0
            PC2_1 PC2_2 PC2_3 PC2_4  : 0.0  1.0  0.0  0.0
            PC3_1 PC3_2 PC3_3 PC3_4  : 0.0  0.0  1.0  0.0
            PC4_1 PC4_2 PC4_3 PC4_4  : 0.0  0.0  0.0  1.0
            CDELT : -805.7218610503596  805.7218610503596  1.0  100000.0
            NAXIS : 0  0
                Polarisation frame: stokesI

    :param gd: Input GridData
    :param template_image: Template output image
    :return: Image

    """
    assert len(gd["pixels"].data.shape) == 4
    wcs = gd.griddata_acc.griddata_wcs
    template_wcs = template.image_acc.wcs
    ft_wcs = copy.deepcopy(template_wcs)

    if wcs.wcs.ctype[0] == "UU" and wcs.wcs.ctype[1] == "VV":
        ft_wcs.wcs.ctype[0] = template_wcs.wcs.ctype[0]
        ft_wcs.wcs.ctype[1] = template_wcs.wcs.ctype[1]
    elif wcs.wcs.ctype[0] == "KX" and wcs.wcs.ctype[1] == "KY":
        ft_wcs.wcs.ctype[0] = template_wcs.wcs.ctype[0]
        ft_wcs.wcs.ctype[1] = template_wcs.wcs.ctype[1]
    elif wcs.wcs.ctype[0] == "UU_AZELGEO" and wcs.wcs.ctype[1] == "VV_AZELGEO":
        ft_wcs.wcs.ctype[0] = template_wcs.wcs.ctype[0]
        ft_wcs.wcs.ctype[1] = template_wcs.wcs.ctype[1]

    else:
        raise NotImplementedError(
            f"Cannot IFFT specified "
            f"axes {wcs.wcs.ctype[0]}, {wcs.wcs.ctype[1]}"
        )

    ft_data = fft(gd["pixels"].data.astype("complex"))
    return Image.constructor(
        data=ft_data,
        polarisation_frame=gd.griddata_acc.polarisation_frame,
        wcs=template_wcs,
    )


def pad_image(im: Image, shape):
    """Pad an image to desired shape, adding equally to all edges

    Appropriate for standard 4D image with axes (freq, pol, y, x).
    Only pads in y, x

    The wcs crpix is adjusted appropriately.

    :param im: Image to be padded
    :param shape: Shape in 4 dimensions
    :return: Padded image
    """

    if im["pixels"].data.shape == shape:
        return im
    newwcs = copy.deepcopy(im.image_acc.wcs)
    newwcs.wcs.crpix[0] = (
        im.image_acc.wcs.wcs.crpix[0]
        + shape[3] // 2
        - im["pixels"].data.shape[3] // 2
    )
    newwcs.wcs.crpix[1] = (
        im.image_acc.wcs.wcs.crpix[1]
        + shape[2] // 2
        - im["pixels"].data.shape[2] // 2
    )

    for axis, _ in enumerate(im["pixels"].data.shape):
        if shape[axis] < im["pixels"].data.shape[axis]:
            data_shape = im["pixels"].data.shape
            raise ValueError(
                f"Padded shape {shape} is smaller than "
                f"input shape {data_shape}"
            )

    newdata = numpy.zeros(shape, dtype=im["pixels"].dtype)
    ystart = shape[2] // 2 - im["pixels"].data.shape[2] // 2
    yend = ystart + im["pixels"].data.shape[2]
    xstart = shape[3] // 2 - im["pixels"].data.shape[3] // 2
    xend = xstart + im["pixels"].data.shape[3]
    newdata[..., ystart:yend, xstart:xend] = im["pixels"][...]
    return Image.constructor(
        data=newdata,
        polarisation_frame=im.image_acc.polarisation_frame,
        wcs=newwcs,
    )


def scale_and_rotate_image(im, angle=0.0, scale=None, order=5):
    """Scale and then rotate an image in x, y axes

    Applies scale then rotates

    :param im: Image
    :param angle: Angle in radians
    :param scale: Scale [scale_x, scale_y]
    :param order: Order of interpolation (0-5)
    :return:
    """
    nchan, npol, ny, nx = im["pixels"].data.shape
    c_in = 0.5 * numpy.array([ny, nx])
    c_out = 0.5 * numpy.array([ny, nx])
    rot = numpy.array(
        [
            [numpy.cos(angle), -numpy.sin(angle)],
            [numpy.sin(angle), numpy.cos(angle)],
        ]
    )
    inv_rot = rot.T
    if scale is None:
        scale = [1.0, 1.0]

    newim = Image.constructor(
        data=numpy.zeros_like(im["pixels"].data),
        polarisation_frame=im.image_acc.polarisation_frame,
        wcs=im.image_acc.wcs,
        clean_beam=im.attrs["clean_beam"],
    )
    inv_scale = numpy.diag(scale)
    inv_transform = numpy.dot(inv_scale, inv_rot)
    offset = c_in - numpy.dot(inv_transform, c_out)
    for chan in range(nchan):
        for pol in range(npol):
            if im["pixels"].data.dtype == "complex":
                newim["pixels"].data[chan, pol] = affine_transform(
                    im["pixels"].data[chan, pol].real,
                    inv_transform,
                    offset=offset,
                    order=order,
                    output_shape=(ny, nx),
                ).astype("float") + 1.0j * affine_transform(
                    im["pixels"].data[chan, pol].imag,
                    inv_transform,
                    offset=offset,
                    order=order,
                    output_shape=(ny, nx),
                ).astype(
                    "float"
                )
            elif im["pixels"].data.dtype == "float":
                newim["pixels"].data[chan, pol] = affine_transform(
                    im["pixels"].data[chan, pol].real,
                    inv_transform,
                    offset=offset,
                    order=order,
                    output_shape=(ny, nx),
                ).astype("float")
            else:
                data_type = im["pixels"].data.dtype
                raise ValueError(f"Cannot process data type {data_type}")

    return newim


def reproject_image(im: Image, newwcs: WCS, shape=None) -> (Image, Image):
    """Re-project an image to a new coordinate system

    Currently uses the reproject python package. This seems to have some
    features do be careful using this method.
    For timeslice imaging griddata is used.

    :param im: Image to be reprojected
    :param newwcs: New WCS
    :param shape: Desired shape
    :return: Reprojected Image, Footprint Image
    """

    if len(im["pixels"].shape) == 4:
        nchan, npol = im["pixels"].shape[:2]
        if im["pixels"].data.dtype == "complex":
            rep_real = numpy.zeros(shape, dtype="float")
            rep_imag = numpy.zeros(shape, dtype="float")
            foot = numpy.zeros(shape, dtype="float")
            for chan in range(nchan):
                for pol in range(npol):
                    rep_real[chan, pol], foot[chan, pol] = reproject_interp(
                        (
                            im["pixels"].data.real[chan, pol],
                            im.image_acc.wcs.sub(2),
                        ),
                        newwcs.sub(2),
                        shape[2:],
                        order="bicubic",
                    )
                    rep_imag[chan, pol], foot[chan, pol] = reproject_interp(
                        (
                            im["pixels"].data.imag[chan, pol],
                            im.image_acc.wcs.sub(2),
                        ),
                        newwcs.sub(2),
                        shape[2:],
                        order="bicubic",
                    )
            rep = rep_real + 1j * rep_imag
        else:
            rep = numpy.zeros(shape, dtype="float")
            foot = numpy.zeros(shape, dtype="float")
            for chan in range(nchan):
                for pol in range(npol):
                    rep[chan, pol], foot[chan, pol] = reproject_interp(
                        (
                            im["pixels"].data[chan, pol],
                            im.image_acc.wcs.sub(2),
                        ),
                        newwcs.sub(2),
                        shape[2:],
                        order="bicubic",
                    )

        if numpy.sum(foot.data) < 1e-12:
            log.warning("reproject_image: no valid points in reprojection")
    elif len(im["pixels"].data.shape) == 2:
        if im["pixels"].data.dtype == "complex":
            rep_real, foot = reproject_interp(
                (im["pixels"].data.real, im.image_acc.wcs),
                newwcs,
                shape,
                order="bicubic",
            )
            rep_imag, foot = reproject_interp(
                (im["pixels"].data.imag, im.image_acc.wcs),
                newwcs,
                shape,
                order="bicubic",
            )
            rep = rep_real + 1j * rep_imag
        else:
            rep, foot = reproject_interp(
                (im["pixels"].data, im.image_acc.wcs),
                newwcs,
                shape,
                order="bicubic",
            )

        if numpy.sum(foot.data) < 1e-12:
            log.warning("reproject_image: no valid points in reprojection")

    else:
        tmp = im["pixels"].shape
        raise ValueError(f"Cannot reproject image with shape {tmp}")
    rep = numpy.nan_to_num(rep)
    foot = numpy.nan_to_num(foot)
    return (
        Image.constructor(rep, im.image_acc.polarisation_frame, newwcs),
        Image.constructor(foot, im.image_acc.polarisation_frame, newwcs),
    )
