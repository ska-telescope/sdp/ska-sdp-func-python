"""
Polarization conversion functions.
"""

__all__ = ["convert_polarization"]

from collections.abc import Iterable
from typing import Literal

import numpy as np
import xarray


def convert_polarization(
    xds: xarray.Dataset,
    output_pols: Iterable[str],
) -> xarray.Dataset:
    """
    Convert the polarization representation of an xradio Dataset to another,
    returning a new Dataset.

    NOTE: Conversion of visibilities between polarization frames is invertible,
    but the conversion of associated weights and flags generally is not and
    results in loss of information. Always avoid chaining polarization
    conversions.

    :param xds: Input xradio Dataset
    :param output_pols: List of desired polarization codes, in the order they
        will appear in the output dataset polarization axis.
    :returns: A new xradio dataset where the visibilities, weights and flags
        have been converted to a new polarization representation.

    :raises ValueError: if output_pols is invalid, or if the requested
        conversion is not possible.

    Example usage
    -------------

    .. code-block:: python

        # Convert to full Stokes representation
        xds_conv = convert_polarization(xds, ["I", "Q", "U", "V"])

        # Partial pol frames are supported, order does not matter
        xds_conv = convert_polarization(xds, ["Q", "I"])  # OK
    """
    input_pols = list(xds.VISIBILITY.polarization.values)
    output_pols = list(output_pols)

    if len(output_pols) > len(set(output_pols)):
        raise ValueError("Output polarizations contain duplicates")

    input_frame = _get_polarization_frame(input_pols)
    output_frame = _get_polarization_frame(output_pols)

    # If both input and output frames are the same, it all reduces to slicing
    if input_frame == output_frame:
        if not set(output_pols).issubset(input_pols):
            raise ValueError(
                f"Conversion from {input_frame} to {output_frame} reduces to "
                f"slicing, but output pols {output_pols} are not a subset "
                f"of input pols {input_pols}"
            )
        return xds.sel({"polarization": output_pols})

    vis_conv = _make_visibility_conversion_dataarray(
        input_frame,
        input_pols,
        output_frame,
        output_pols,
        dtype=xds.VISIBILITY.dtype,
    )
    output_vis = _apply_conversion_matrix(xds.VISIBILITY, vis_conv)

    # An output polarization is flagged if any input polarization that
    # contributes to it is flagged.
    flag_conv = (vis_conv != 0).astype("int8")  # use int8 for speed
    output_flag = _apply_conversion_matrix(xds.FLAG, flag_conv) != 0

    # Weights are homogeneous to inverses of variances, and variances add
    # linearly. Complex modulus of coefficients must be taken before squaring
    # them in the case of complex random variables:
    # https://en.wikipedia.org/wiki/Complex_random_variable#Variance_and_pseudo-variance
    # If a visibility vector Y linearly relates to another visibility vector
    # X via Y = AX where A is a complex-valued matrix, then elements of the
    # weights of Y (call them W[Y_i]) are given by:
    # W[Y_i]^{-1} = \sum_j |A_{ij}|^2 W[X_j]^{-1}
    variance_conv = (abs(vis_conv) ** 2).astype(xds.WEIGHT.dtype)

    # NOTE: we need to handle zero weights very carefully here. If we blindly
    # apply the formula above, we run into a problem when we have A_ij = 0
    # with W[X_j] = 0. In this case |A_{ij}|^2 W[X_j]^{-1} yields NaN, which
    # propagates to the whole sum.
    # However, A_ij = 0 means that W[X_j] has no contribution to the
    # output weight W[Y_i], and the result we want is thus:
    # |A_{ij}|^2 W[X_j]^{-1} = 0.
    # The easiest way of achieving this is to add a small constant to the
    # weights before inverting them. We choose the smallest possible number
    # representable with the weights data type.
    eps = np.finfo(xds.WEIGHT.dtype).smallest_normal
    output_weight = 1.0 / _apply_conversion_matrix(
        1.0 / (xds.WEIGHT + eps), variance_conv
    )

    # We need to create a new Dataset object because the length of the
    # polarization dimension can change. xds.assign() won't work in this case.
    data_vars = dict(xds.data_vars) | {
        "VISIBILITY": output_vis,
        "WEIGHT": output_weight,
        "FLAG": output_flag,
    }
    coords = dict(xds.coords) | {"polarization": output_pols}
    return xarray.Dataset(data_vars=data_vars, coords=coords, attrs=xds.attrs)


CODES_BY_FRAME = {
    "linear": ["XX", "XY", "YX", "YY"],
    "circular": ["RR", "RL", "LR", "LL"],
    "stokes": ["I", "Q", "U", "V"],
}


# Conversion matrices A such that:
# Y = AX
# Where Y = data in new polarization frame, X = in old polarization frame
# Rows map to new pol frame, columns map to old pol frame.
LINEAR_TO_STOKES = [
    [0.5 + 0.0j, 0.0 + 0.0j, 0.0 + 0.0j, 0.5 + 0.0j],
    [0.5 + 0.0j, 0.0 + 0.0j, 0.0 + 0.0j, -0.5 + 0.0j],
    [0.0 + 0.0j, 0.5 + 0.0j, 0.5 + 0.0j, 0.0 + 0.0j],
    [0.0 + 0.0j, -0.0 - 0.5j, 0.0 + 0.5j, 0.0 + 0.0j],
]


STOKES_TO_LINEAR = [
    [1.0 + 0.0j, 1.0 + 0.0j, 0.0 + 0.0j, 0.0 + 0.0j],
    [0.0 + 0.0j, 0.0 + 0.0j, 1.0 + 0.0j, 0.0 + 1.0j],
    [0.0 + 0.0j, 0.0 + 0.0j, 1.0 + 0.0j, 0.0 - 1.0j],
    [1.0 + 0.0j, -1.0 - 0.0j, 0.0 - 0.0j, 0.0 - 0.0j],
]

CIRCULAR_TO_STOKES = [
    [0.5 + 0.0j, 0.0 + 0.0j, 0.0 + 0.0j, 0.5 + 0.0j],
    [0.0 + 0.0j, 0.5 + 0.0j, 0.5 + 0.0j, 0.0 + 0.0j],
    [0.0 + 0.0j, -0.0 - 0.5j, 0.0 + 0.5j, 0.0 + 0.0j],
    [0.5 + 0.0j, 0.0 + 0.0j, 0.0 + 0.0j, -0.5 + 0.0j],
]

STOKES_TO_CIRCULAR = [
    [1.0 + 0.0j, 0.0 + 0.0j, 0.0 + 0.0j, 1.0 + 0.0j],
    [0.0 + 0.0j, 1.0 + 0.0j, 0.0 + 1.0j, 0.0 + 0.0j],
    [0.0 + 0.0j, 1.0 + 0.0j, 0.0 - 1.0j, 0.0 + 0.0j],
    [1.0 + 0.0j, 0.0 - 0.0j, 0.0 - 0.0j, -1.0 - 0.0j],
]

LINEAR_TO_CIRCULAR = [
    [0.5 + 0.0j, 0.0 - 0.5j, 0.0 + 0.5j, 0.5 + 0.0j],
    [0.5 + 0.0j, 0.0 + 0.5j, 0.0 + 0.5j, -0.5 + 0.0j],
    [0.5 + 0.0j, 0.0 - 0.5j, 0.0 - 0.5j, -0.5 + 0.0j],
    [0.5 + 0.0j, 0.0 + 0.5j, 0.0 - 0.5j, 0.5 + 0.0j],
]

CIRCULAR_TO_LINEAR = [
    [0.5 + 0.0j, 0.5 + 0.0j, 0.5 + 0.0j, 0.5 + 0.0j],
    [0.0 + 0.5j, 0.0 - 0.5j, 0.0 + 0.5j, 0.0 - 0.5j],
    [-0.0 - 0.5j, -0.0 - 0.5j, 0.0 + 0.5j, 0.0 + 0.5j],
    [0.5 + 0.0j, -0.5 + 0.0j, -0.5 + 0.0j, 0.5 + 0.0j],
]


VIS_CONV_MATRICES = {
    ("linear", "stokes"): LINEAR_TO_STOKES,
    ("stokes", "linear"): STOKES_TO_LINEAR,
    ("circular", "stokes"): CIRCULAR_TO_STOKES,
    ("stokes", "circular"): STOKES_TO_CIRCULAR,
    ("linear", "circular"): LINEAR_TO_CIRCULAR,
    ("circular", "linear"): CIRCULAR_TO_LINEAR,
}


def _get_polarization_frame(
    pol_codes: list[str],
) -> Literal["linear", "circular", "stokes"]:
    try:
        return next(
            key
            for key, val in CODES_BY_FRAME.items()
            if set(pol_codes).issubset(val)
        )
    except StopIteration as err:
        lines = [
            f"Invalid set of polarizations {pol_codes}, "
            "must be a subset of either: "
        ]
        lines.extend([f"* {pol_list}" for pol_list in CODES_BY_FRAME.values()])
        msg = "\n".join(lines)
        raise ValueError(msg) from err


def _make_visibility_conversion_dataarray(
    input_frame: str,
    input_pols: list[str],
    output_frame: str,
    output_pols: list[str],
    *,
    dtype: np.dtype,
) -> xarray.DataArray:
    # Create full conversion DataArray
    conv = np.asarray(
        VIS_CONV_MATRICES[(input_frame, output_frame)], dtype=dtype
    )
    conv = xarray.DataArray(
        data=conv,
        coords={
            "polarization": ("polarization", CODES_BY_FRAME[input_frame]),
            "new_polarization": (
                "new_polarization",
                CODES_BY_FRAME[output_frame],
            ),
        },
        dims=["new_polarization", "polarization"],
    )

    # Assert we've got all the input polarizations required to compute the
    # desired output polarizations
    # e.g. we can't compute Stokes V without cross polarizations
    required_input_pols = (
        conv.sel({"new_polarization": output_pols})
        .where(conv != 0, drop=True)
        .polarization
    )
    required_input_pols = list(required_input_pols.values)

    if not set(required_input_pols).issubset(input_pols):
        raise ValueError(
            f"Conversion to {output_pols} from {input_frame} frame requires "
            f"{required_input_pols} but only {input_pols} are available"
        )

    # Select the portion of the matrix that we need.
    # We don't need to worry about the order of the input or output pol codes,
    # rows and columns of the matrix are magically reordered by xarray here.
    return conv.sel(
        {"polarization": input_pols, "new_polarization": output_pols}
    )


def _apply_conversion_matrix(
    arr: xarray.DataArray, convmat: xarray.DataArray
) -> xarray.DataArray:
    # NOTE: It is important to preserve the attributes of the input
    # visibility/flag/weight DataArray
    with xarray.set_options(keep_attrs=True):
        return xarray.dot(arr, convmat, dim=["polarization"]).rename(
            {"new_polarization": "polarization"}
        )
