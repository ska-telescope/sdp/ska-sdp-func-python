"""
Function for performing uv continuum subtraction
"""

__all__ = ["remove_continuum_visibility", "subtract_visibility"]

import logging

import numpy
import xarray

log = logging.getLogger("func-python-logger")


def polyfit_within_threshold(
    x: numpy.array,
    data: numpy.array,
    flag: numpy.array,
    degree: int,
    threshold: float,
):
    """
    Performs polyfit on data within thresholded sigma.
    :param x: X coordinate
    :param data: Data
    :param flag: Existing data flag
    :param degree: Order of polynomial to fit
    :param threshold: Sigma threshold
    :return: Polyfit values
    """
    q25, q50, q75 = numpy.quantile(data, [0.25, 0.5, 0.75])
    sigma = (q75 - q25) / 1.35

    outlier_flag = numpy.logical_or(abs(data - q50) > threshold * sigma, flag)

    soln = numpy.polynomial.polynomial.polyfit(
        x, data, degree, w=(1.0 - outlier_flag)
    )

    return numpy.polynomial.polynomial.polyval(x, soln)


def fit_model(
    vis: numpy.array,
    flag: numpy.array,
    channel_mask: slice = None,
    degree: int = 1,
    threshold: float = 2.5,
) -> numpy.array:
    """
    Fits the continuum line and returns model data
    :param vis: Visibilities data to fit
    :param flag: Flag column of visibilities
    :param channel_mask: Channels to be excluded
    :param degree: Order of polynomial to fit
    :param threshold: Sigma threshold
    :return: Model continuum line data
    """

    x = numpy.arange(vis.shape[0])
    yreal = vis.real
    yimag = vis.imag

    _flag = numpy.copy(flag)
    if channel_mask is not None:
        _flag[channel_mask] = True

    model_real = polyfit_within_threshold(x, yreal, _flag, degree, threshold)
    model_imag = polyfit_within_threshold(x, yimag, _flag, degree, threshold)

    return xarray.DataArray(model_real + 1j * model_imag, dims=["frequency"])


def subtract_visibility(
    target_observation: xarray.Dataset, src_observation: xarray.Dataset
):
    """
    Subtract model visibility from visibility,
    returning dataset with new visibility.

    :param target_observation: The target observation dataset from which
        visibility needs to be subtracted
    :param src_observation: The observtion dataset providing the visibilities
        to be subtracted
    :return: New processing set with subtracted visibilities
    """
    cont_sub_vis = target_observation.VISIBILITY - src_observation.VISIBILITY
    cont_sub_vis = cont_sub_vis.assign_attrs(
        target_observation.VISIBILITY.attrs
    )

    return target_observation.assign({"VISIBILITY": cont_sub_vis})


def model_continuum_visibility(
    observation: xarray.Dataset,
    mask: slice,
    degree: int,
    threshold: float,
):
    """
    Calculate model visibilities

    :param observation: An observtion dataset obtained from a processing set
    :param mask: Channels to be excluded,
    :param degree: Degree of fit,
    :param threshold: Threshold in sigma to reject outliers.
    :return: New processing set with continuum subtracted
             visibilities and model visibilities
    """
    return observation.assign(
        {
            "VISIBILITY": xarray.apply_ufunc(
                fit_model,
                observation.VISIBILITY,
                observation.FLAG,
                input_core_dims=[["frequency"], ["frequency"]],
                output_core_dims=[["frequency"]],
                vectorize=True,
                kwargs={
                    "degree": degree,
                    "channel_mask": mask,
                    "threshold": threshold,
                },
            )
        }
    )


def remove_continuum_visibility(
    observation: xarray.Dataset,
    mask: slice = None,
    degree: int = 1,
    threshold: float = 2.5,
) -> xarray.Dataset:
    """
    Perform uv continuum subtraction.

    :param observation: An observtion dataset obtained from a processing set
    :param mask: Channels to be excluded, **Default:** None
    :param degree: Degree of fit, **Default:** 1
    :param threshold: Threshold in sigma to reject outliers. **Default:** 2.5
    :return: New processing set with continuum subtracted
             visibilities and model visibilities
    """
    log.info("Running continumm subtraction with fit")

    model_visibility = model_continuum_visibility(
        observation,
        mask,
        degree,
        threshold,
    )

    return subtract_visibility(observation, model_visibility)
