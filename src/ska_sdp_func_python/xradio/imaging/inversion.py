"""
High-level processing functions for imaging xradio Visibility datasets.
"""

from collections.abc import Iterable

import dask.array as da
import numpy as np
import xarray
from numpy.typing import NDArray

from .image_params import ImageParams
from .invert_backend import InvertBackend


def invert(
    xds: xarray.Dataset,
    backend: InvertBackend,
    *,
    num_pixels_l: int,
    num_pixels_m: int,
    pixel_size_radians: float,
) -> xarray.Dataset:
    """
    Invert an xradio visibility dataset into a dirty image cube, using one of
    the supported invert backends/gridders. The time and frequency resolution
    of the output image cube, as well as the distribution pattern, are
    controlled by the chunking of the input dataset. See notes at the end.

    .. warning::
        This function is experimental and its interface may change.

    :param xds: Input xradio visibility Dataset.
    :param backend: Configuration options for the invert backend to use,
        e.g. pass a `DuccInvert` instance to use DUCC. Supported backends:

        * DUCC

        * W-towers. ska-sdp-func must be installed.
          Restrictions: square images only, requires visibilities with
          uniformly spaced frequency channels.
    :param num_pixels_l: Number of pixels in the l direction (east-west).
        Must be an even number.
    :param num_pixels_m: Number of pixels in the m direction (north-south).
        Must be an even number.
    :param pixel_size_radians: The size of a pixel in radians at the centre of
        the field of view.

    :returns: The resulting dirty image cube as an Image xarray Dataset.

    .. note::
        * The output image has a number of time and frequency channels equal
          to the number of time and frequency chunks of the input dataset.
          Polarization channels are always imaged separately, regardless of
          chunking.

        * This function uses ``xarray.map_blocks()`` under the hood, and
          generates one task per (time, frequency, polarization) chunk.

        * Chunking along the ``baseline_id`` dimension is not allowed (yet),
          as it would require dealing with groups of baselines in the Image
          data model.

        * Natural weighting is used; support for other weighting modes will be
          added later.
    """
    _assert_phase_center_is_fixed(xds)

    # NOTE: raising an error is better than rechunking silently
    if len(xds.chunksizes["baseline_id"]) > 1:
        raise ValueError(
            "Input dataset may not have multiple chunks along the baseline_id "
            "dimension; the Image data model does not currently support this. "
            "Please re-chunk accordingly first."
        )

    if not isinstance(backend, InvertBackend):
        raise TypeError("backend must be an InvertBackend instance")

    image_params = ImageParams(num_pixels_l, num_pixels_m, pixel_size_radians)

    # Run invert, creating one task per chunk. The call to map_blocks is lazy.
    # The DataArray returned by map_blocks has all the necessary named
    # dimensions, but no coordinates or attributes yet
    template = create_map_blocks_template(xds, backend, image_params)
    image_dataarray = xarray.map_blocks(
        _execute_invert,
        xds,
        args=(backend, image_params),
        template=template,
    )
    image = image_dataarray.to_dataset(name="SKY")

    # Add all coordinates
    image["time"] = _mean_by_blocks_1d(xds.time, xds.chunksizes["time"])
    image["frequency"] = _mean_by_blocks_1d(
        xds.frequency, xds.chunksizes["frequency"]
    )
    image["polarization"] = xds.polarization
    image["l"] = backend.l_coords(image_params)
    image["m"] = backend.m_coords(image_params)

    # Copy over the required attributes
    # TODO: in the future, we need to either schema check the input
    # visibilities, in case some attributes are missing.
    image["time"].attrs = _relevant_time_attrs(xds)
    image["frequency"].attrs = _relevant_frequency_attrs(xds)
    image.attrs = _create_image_dataset_attrs(xds)
    return image


def _execute_invert(
    xds: xarray.Dataset,
    backend: InvertBackend,
    image_params: ImageParams,
) -> xarray.DataArray:
    """
    Execute invert with the user-requested backend, ensuring that the output
    has all expected dimensions but no coordinates yet.
    """
    # pylint: disable=protected-access
    result: xarray.DataArray = backend._execute(xds, image_params)
    # The function we called does not return data with time and freq dims,
    # we add them (with dimension 1) so that `map_blocks` receives all the
    # expected output dims.
    result = result.expand_dims(("time", "frequency"))
    # We strip any coordinate values that may have been set, we deal with that
    # in a single place later.
    result = result.drop_vars(result.coords.keys())
    return result


def generate_psf(
    xds: xarray.Dataset,
    backend: InvertBackend,
    *,
    num_pixels_l: int,
    num_pixels_m: int,
    pixel_size_radians: float,
) -> xarray.Dataset:
    """
    Generate the PSF cube associated with the given xradio visibility dataset,
    using one of the supported invert backends/gridders. Calls `invert` with
    the exact same options, except with visibility values replaced by ones.
    See notes at the end for details.

    .. warning::
        This function is experimental and its interface may change.
        Natural weighting is used; support for other weighting modes will be
        added later.

    :param xds: Input xradio visibility Dataset.
    :param backend: Configuration options for the invert backend to use,
        e.g. pass a `DuccInvert` instance to use DUCC. See `invert` for
        available backends.
    :param num_pixels_l: Number of pixels in the l direction (east-west).
        Must be an even number.
    :param num_pixels_m: Number of pixels in the m direction (north-south).
        Must be an even number.
    :param pixel_size_radians: The size of a pixel in radians at the centre of
        the field of view.

    :returns: The resulting PSF as an Image xarray Dataset.

    .. note::
        The PSFs for different polarization channels will often be very
        similar, any differences between them can only be caused by differences
        in visibility weights. The choice is left to the caller to select
        only one pol before calling this function, and use that PSF as an
        approximation for the other pols.
    """
    xds_ones = xds.assign(
        {
            "VISIBILITY": xarray.ones_like(xds.VISIBILITY),
        }
    )
    return invert(
        xds_ones,
        backend,
        num_pixels_l=num_pixels_l,
        num_pixels_m=num_pixels_m,
        pixel_size_radians=pixel_size_radians,
    )


def create_map_blocks_template(
    xds: xarray.Dataset,
    backend: InvertBackend,
    image_params: ImageParams,
) -> xarray.DataArray:
    """
    Returns the appropriate `template` parameter for the xarray.map_blocks()
    call in the invert function; does not set coordinates and attributes, they
    are set later on.
    """
    # Order the l and m dimensions following the output data order
    dim_names = (
        "time",
        "frequency",
        "polarization",
    ) + backend.output_dim_order

    # Number of time and frequency bands in the output must match the number of
    # input chunks
    shape_dict = {
        name: len(xds.chunksizes[name]) for name in ("time", "frequency")
    }
    shape_dict.update(
        {
            "l": image_params.num_pixels_l,
            "m": image_params.num_pixels_m,
            "polarization": len(xds.polarization),
        }
    )
    shape = tuple(shape_dict[name] for name in dim_names)

    # Align output chunks with input chunks, this is a requirement of
    # `map_blocks()`. This means that our image chunk sizes must
    # be 1 along the time and frequency dimensions.
    # Chunking along l or m is not an option, because the gridders we wrap
    # produce the whole image in one go.
    chunk_sizes_dict = {
        "time": 1,
        "frequency": 1,
        # We're allowing the input to have any chunking pattern along the
        # pol axis, we just mirror that in the output.
        "polarization": xds.chunksizes["polarization"][0],
        "l": image_params.num_pixels_l,
        "m": image_params.num_pixels_m,
    }

    chunk_sizes = tuple(chunk_sizes_dict[name] for name in dim_names)
    array = da.zeros(shape, chunks=chunk_sizes)
    return xarray.DataArray(data=array, dims=dim_names, attrs=None)


def _mean_by_blocks_1d(arr: NDArray, block_sizes: Iterable[int]) -> NDArray:
    if not arr.ndim == 1:
        raise ValueError
    if not sum(block_sizes) == arr.size:
        raise ValueError
    start = 0
    result = []
    for b in block_sizes:
        result.append(arr[start : start + b].mean())
        start += b
    return np.asarray(result)


def _dict_subset(dictionary: dict, keys: Iterable) -> dict:
    return {key: value for key, value in dictionary.items() if key in keys}


def _create_image_dataset_attrs(visibility_xds: xarray.Dataset) -> dict:
    """
    The attributes dictionary to be assigned to the output Image.
    """
    reference = _get_phase_center_info_dict(visibility_xds)
    direction = {"reference": reference}
    return {"direction": direction}


def _assert_phase_center_is_fixed(visibility_xds: xarray.Dataset) -> dict:
    """
    Raise ValueError if the visibility dataset has a variable phase centre.
    """
    phase_center_da: xarray.DataArray = (
        visibility_xds.VISIBILITY.field_and_source_xds.FIELD_PHASE_CENTER
    )
    if not set(phase_center_da.dims) == {"sky_dir_label"}:
        raise ValueError(
            "Cannot process visibility dataset with variable phase centre"
        )


def _get_phase_center_info_dict(visibility_xds: xarray.Dataset) -> dict:
    """
    Get phase center information as a dictionary ready to be assigned to the
    output Image attributes.
    """
    _assert_phase_center_is_fixed(visibility_xds)
    phase_center: xarray.DataArray = (
        visibility_xds.VISIBILITY.field_and_source_xds.FIELD_PHASE_CENTER
    )

    coord_names: list[str] = list(phase_center.sky_dir_label.values)

    if not set(coord_names) == {"ra", "dec"}:
        raise ValueError(
            "Phase centre coordinates must be specified in equatorial frame "
            "(RA / Dec). Frame conversion will be added later."
        )

    # c0 and c1 map to "ra" and "dec" in some order
    # The order should always be ["ra", "dec"] but you never know
    c0, c1 = coord_names

    # Cast from np.floatXX to float to avoid possible issues down the line
    coord_values = {
        c0: float(phase_center.values[0]),
        c1: float(phase_center.values[1]),
    }
    ra = coord_values["ra"]
    dec = coord_values["dec"]

    return {
        "type": "sky_coord",
        "frame": phase_center.attrs["frame"],  # e.g. "fk5"
        "units": phase_center.attrs["units"],  # e.g. ["rad", "rad"]
        "value": [ra, dec],
    }


def _relevant_time_attrs(
    xds: xarray.Dataset,
) -> dict:
    """
    The subset of "time" coordinate attributes of the input visibility xds
    that we need to copy over to the output Image.
    """
    keys = {"type", "scale", "units", "format"}
    return _dict_subset(xds.time.attrs, keys)


def _relevant_frequency_attrs(
    xds: xarray.Dataset,
) -> dict:
    """
    The subset of "frequency" coordinate attributes of the input visibility xds
    that we need to copy over to the output Image.
    """
    keys = {"frame", "type", "units"}
    return _dict_subset(xds.frequency.attrs, keys)
