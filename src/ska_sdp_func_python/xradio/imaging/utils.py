"""
Misc functions useful across the imaging module.
"""

from collections.abc import Callable

from numpy.typing import NDArray


def assert_or_value_error(condition: bool, message: str) -> None:
    """
    If `condition` is False, raise ValueError with `message`.
    """
    if not condition:
        raise ValueError(message)


def float32_result(func: Callable[..., NDArray]) -> Callable[..., NDArray]:
    """
    Decorator that wraps a function that returns a numpy array, casting its
    result float32 dtype. Useful in case `func` has multiple return
    statements, and to enforce the same output type from all gridders.
    """

    def wrapped(*args, **kwargs):
        return func(*args, **kwargs).astype("float32")

    return wrapped
