"""
W-towers invert backend.
"""

# pylint:disable=no-member
import importlib
from dataclasses import dataclass, fields
from functools import cached_property
from typing import Literal, Optional

import numpy as np
import xarray
from numpy.typing import NDArray

from .image_params import ImageParams
from .invert_backend import InvertBackend
from .utils import assert_or_value_error, float32_result


# pylint:disable=too-many-instance-attributes
@dataclass(frozen=True)
class WtowersInvert(InvertBackend):
    """
    Validate and store user options for the "end-to-end" W-towers gridder.
    Most parameters should be left to their default values, unless there is a
    very good reason not to. `subgrid_size` is the main performance parameter
    that can be tweaked.

    .. warning:

        Image shearing is not supported yet; the parameters `shear_u` and
        `shear_v` of the W-towers gridding kernel are set to 0.

    :param subgrid_size: Sub-grid size in pixels. Must be even and at least 16.
    :param support: Kernel support size along u and v. Must be even and at
        least 4.
    :param w_support: Kernel support size along w. Must be even and at least 4.
    :param oversampling: The oversampling factor for the uv-kernel.
    :param w_oversampling: The oversampling factor for the w-kernel.
    :param subgrid_frac: Fraction of the subgrid that will be used. Must not
        exceed 1. A value of 0.66 is recommended.
    :param padding_factor: Image padding factor; in practice, image a field
        of view `padding_factor` times larger than requested, then discard the
        edges. Higher values increase image accuracy and compute cost.
    :param nthreads: Number of threads used for parallel processing. `None`
        means use all threads available on the CPU. Be careful when
        using values other than 1 in a dask context; the scheduler assumes that
        each task requires 1 thread unless told otherwise using e.g. dask
        resources.
    :param verbosity: Print additional output to stdout based on value.
        0 means print nothing;
        1 or higher means print a timing report;
        2 or higher means print the number of visibilities per subgrid
    """

    subgrid_size: int = 256
    support: int = 8
    w_support: int = 8
    oversampling: int = 16384
    w_oversampling: int = 16384
    subgrid_frac: float = 2 / 3
    padding_factor: float = 1.2
    nthreads: Optional[int] = 1
    verbosity: int = 0

    def __post_init__(self):
        # That way, the user knows as soon as they try making a WtowersInvert
        # instance.
        try:
            importlib.import_module("ska_sdp_func")
        except ModuleNotFoundError as err:
            raise ModuleNotFoundError(
                "Inversion using W-towers requires ska-sdp-func to be "
                "installed"
            ) from err

        assert_or_value_error(
            self.subgrid_size % 2 == 0 and self.subgrid_size >= 16,
            "subgrid_size must be even and >= 16",
        )
        assert_or_value_error(
            self.support >= 4 and self.support % 2 == 0,
            "support must be even and >= 4",
        )
        assert_or_value_error(
            self.w_support >= 4 and self.support % 2 == 0,
            "w_support must be even and >= 4",
        )
        assert_or_value_error(
            0 < self.subgrid_frac <= 1, "subgrid_frac must be > 0 and <= 1"
        )
        assert_or_value_error(
            self.padding_factor >= 1, "padding_factor must be >= 1"
        )
        assert_or_value_error(
            self.nthreads >= 1 or self.nthreads is None,
            "nthreads must be >= 1 or None",
        )

    @property
    def output_dim_order(self) -> tuple[Literal["l", "m"]]:
        return ("l", "m")

    @property
    def l_step_sign(self) -> Literal[-1, +1]:
        return -1

    @property
    def m_step_sign(self) -> Literal[-1, +1]:
        return -1

    def _execute(
        self, xds: xarray.Dataset, image_params: ImageParams
    ) -> xarray.DataArray:
        return invert_wtowers(xds, self, image_params)


def assert_channel_frequencies_valid_and_uniformly_spaced(freqs_hz: NDArray):
    """
    Self-explanatory.
    """
    assert_or_value_error(
        freqs_hz.ndim == 1, "Channel frequencies array must be 1D"
    )
    assert_or_value_error(
        freqs_hz.size > 0, "Channel frequencies array must not be empty"
    )
    if freqs_hz.size > 1:
        freq_step = (freqs_hz[-1] - freqs_hz[0]) / (freqs_hz.size - 1)
        assert_or_value_error(
            np.allclose(np.diff(freqs_hz), freq_step),
            "W-towers requires channel frequencies to be uniformly spaced",
        )


class WtowersFullParams:
    """
    Based on user input, computes and stores the full set of W-towers
    parameters. NOTE: this is part of the private interface.
    """

    def __init__(
        self, backend: WtowersInvert, img: ImageParams, freqs_hz: NDArray
    ):
        if not img.num_pixels_l == img.num_pixels_m:
            raise ValueError("W-towers invert supports only square images")

        self._backend = backend
        self._img = img

        assert_channel_frequencies_valid_and_uniformly_spaced(freqs_hz)
        self._freq0_hz = freqs_hz[0]
        self._dfreq_hz = 0.0
        if freqs_hz.size > 1:
            self._dfreq_hz = (freqs_hz[-1] - freqs_hz[0]) / (freqs_hz.size - 1)

        # Expose the attributes of WtowersInvert directly on this object.
        # We exclude user options that need updating based on image parameters
        attribute_names = {f.name for f in fields(WtowersInvert)} - {
            "subgrid_size"
        }
        for name in attribute_names:
            setattr(self, name, getattr(self._backend, name))

    @property
    def subgrid_size(self) -> int:
        """
        Actual subgrid size to use, it can't exceed the padded image size.
        """
        return min(self._backend.subgrid_size, self.num_pixels_padded)

    @property
    def freq0_hz(self) -> float:
        """
        Centre frequency of the first channel in Hz.
        """
        return self._freq0_hz

    @property
    def dfreq_hz(self) -> float:
        """
        Frequency step between channels in Hz.
        """
        return self._dfreq_hz

    @property
    def num_pixels(self) -> int:
        """
        Number of pixels across the output (square) image.
        """
        return self._img.num_pixels_l

    @property
    def num_pixels_padded(self) -> int:
        """
        Number of pixels across the intermediate padded output (square) image.
        Its margins get discarded.
        """
        try:
            from ska_sdp_func.fourier_transforms import padded_fft_size
        except ModuleNotFoundError:
            pass

        return padded_fft_size(self.num_pixels, self.padding_factor)

    @property
    def margin(self) -> int:
        """
        Number of pixels that should be discarded from the padded image, on
        each of the 4 sides.
        """
        # NOTE: works because num_pixels_padded and num_pixels are always even
        return (self.num_pixels_padded - self.num_pixels) // 2

    @property
    def fov(self) -> float:
        """
        Field of view in direction cosines.
        """
        return self.num_pixels * self._img.pixel_size_lm

    @property
    def theta(self) -> float:
        """
        Padded field of view in direction cosines.
        """
        return self.num_pixels_padded * self._img.pixel_size_lm

    @cached_property
    def w_step(self) -> float:
        """
        Spacing between w-planes inside a w-tower, in units of wavelengths.
        """
        try:
            from ska_sdp_func.grid_data import determine_w_step
        except ModuleNotFoundError:
            pass

        return determine_w_step(self.theta, self.fov, shear_u=0.0, shear_v=0.0)

    @cached_property
    def w_tower_height(self) -> float:
        """
        Height of a w-tower in units of `w_step`.
        """
        try:
            from ska_sdp_func.grid_data import determine_max_w_tower_height
        except ModuleNotFoundError:
            pass

        return determine_max_w_tower_height(
            self.subgrid_size,
            self.theta,
            self.fov,
            self.w_step,
            self.support,
            self.oversampling,
            self.w_support,
            self.w_oversampling,
            subgrid_frac=self.subgrid_frac,
        )


def _reshape_vis_like_dataarray(arr: xarray.DataArray) -> xarray.DataArray:
    """
    Prepare visibility-shaped arrays for processing with W-towers, that is:
    VISIBILITY, WEIGHT and FLAG.

    W-towers wants the data in (row, freq) order, where a "row" is a
    (time, baseline) combination. It also wants the data contiguous
    in memory (C order).
    """
    return (
        # Combine time and baseline into new "row" dimension
        arr.stack({"row": ("time", "baseline_id")})
        # Stacked dimension "row" is automatically placed at the end, but
        # we want a specific dim order
        .transpose("polarization", "row", "frequency")
        # Make contiguous in memory
        .astype(arr.dtype, order="C")
    )


def _reshape_uvw_dataarray(arr: xarray.DataArray) -> xarray.DataArray:
    """
    Prepare UVW array for processing with W-towers.
    """
    return (
        arr.stack({"row": ("time", "baseline_id")})
        .transpose("row", "uvw_label")
        .astype(arr.dtype, order="C")
    )


def invert_wtowers(
    xds: xarray.Dataset,
    backend: WtowersInvert,
    image_params: ImageParams,
) -> xarray.DataArray:
    """
    Invert an xradio visibility dataset using the "end-to-end" W-towers
    gridder. For each polarization  channel, all visibilities are gridded on
    the same grid. Returns a DataArray with 3 dimensions (polarization, l, m),
    without any coordinate values or attributes.
    """

    wtowers_full_params = WtowersFullParams(
        backend, image_params, xds.frequency.values
    )

    image_dataarray: xarray.DataArray = xarray.apply_ufunc(
        _invert_wtowers_singlepol,
        _reshape_vis_like_dataarray(xds.VISIBILITY),
        _reshape_vis_like_dataarray(xds.WEIGHT),
        _reshape_vis_like_dataarray(xds.FLAG),
        _reshape_uvw_dataarray(xds.UVW),
        input_core_dims=[
            ("row", "frequency"),
            ("row", "frequency"),
            ("row", "frequency"),
            ("row", "uvw_label"),
        ],
        output_core_dims=[tuple(backend.output_dim_order)],
        vectorize=True,
        kwargs={"params": wtowers_full_params},
        # When the underlying arrays are dask-backed, these arguments make the
        # application of the function lazy.
        dask="parallelized",
        dask_gufunc_kwargs={
            "output_sizes": {
                "l": image_params.num_pixels_l,
                "m": image_params.num_pixels_m,
            },
            # Unless allow_rechunk=True,
            # "chunk sizes need to match and core dimensions are to consist
            # only of one chunk."
            # We have to set it to True for the function to work when the
            # input has multiple chunks along time, baseline_id or freq,
            # because:
            # - We're gridding the entire input dataset onto the same grid
            # - The gridder needs all the data in memory before being called
            "allow_rechunk": True,
        },
        # Providing `output_dtypes` prevents apply_ufunc from doing automatic
        # output dtype inference, which it does by running the function on
        # small arrays of zeros, and that could crash in some cases.
        output_dtypes=["float32"],
    )
    return image_dataarray


@float32_result
def _invert_wtowers_singlepol(
    vis: NDArray,
    weight: NDArray,
    flag: NDArray,
    uvw: NDArray,
    *,
    params: WtowersFullParams,
) -> NDArray:
    """
    Simple wrapper for the W-towers end-to-end invert function
    `wstack_wtower_grid_all()`, which also:
    - Takes care of padding and un-padding
    - Normalises the image data by the total gridding weight
    - Ensures the desired number of threads is used

    Expected array shapes are the same as what `wstack_wtower_grid_all()`
    expects.
    """
    try:
        # Both modules are part of the same optional dependency group
        from ska_sdp_func.grid_data import wstack_wtower_grid_all
        from threadpoolctl import threadpool_limits
    except ModuleNotFoundError:
        pass

    # Take into account data weights and flags simultaneously.
    # Makes it easier to normalize the image.
    effective_weights = weight * np.logical_not(flag)
    total_weight = effective_weights.sum()

    # If all the data is effectively masked, the image is only zeros
    if not total_weight > 0.0:
        return np.zeros(
            shape=(
                params.num_pixels,
                params.num_pixels,
            ),
            dtype="float32",
        )

    image_arr = np.zeros(
        (params.num_pixels_padded, params.num_pixels_padded), dtype="float32"
    )

    with threadpool_limits(limits=params.nthreads, user_api="openmp"):
        wstack_wtower_grid_all(
            effective_weights * vis,
            params.freq0_hz,
            params.dfreq_hz,
            # NOTE: Flipping the sign of w is required, because:
            # * MSv2 assumes that image -> vis requires a positive-signed
            #   Fourier transform, i.e. what is typically called an inverse FT.
            # * W-towers was developed under the opposite assumption
            uvw * [1, 1, -1],
            params.subgrid_size,
            params.theta,
            params.w_step,
            0.0,  # shear_u
            0.0,  # shear_v
            params.support,
            params.oversampling,
            params.w_support,
            params.w_oversampling,
            params.subgrid_frac,
            params.w_tower_height,
            params.verbosity,
            image_arr,
        )

    # Un-pad
    m = params.margin
    image_arr = image_arr[m:-m, m:-m]

    # Normalise
    return image_arr / effective_weights.sum()
