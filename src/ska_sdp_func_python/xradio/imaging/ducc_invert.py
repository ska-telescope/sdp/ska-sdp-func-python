"""
DUCC invert backend.
"""

import os
from dataclasses import dataclass
from typing import Literal, Optional

import numpy as np
import xarray

# pylint:disable=import-error,no-name-in-module
from ducc0.misc import resize_thread_pool

# pylint:disable=import-error,no-name-in-module
from ducc0.wgridder import ms2dirty
from numpy.typing import NDArray

from .image_params import ImageParams
from .invert_backend import InvertBackend
from .utils import assert_or_value_error, float32_result


@dataclass(frozen=True)
class DuccInvert(InvertBackend):
    """
    Validate and store user options for DUCC invert.

    :param float epsilon: Accuracy at which the computation should be done.
    :param int nthreads: The number of threads to use for parallel processing.
        `None` means use all threads available on the CPU. Be careful when
        using values other than 1 in a dask context; the scheduler assumes that
        each task requires 1 thread unless told otherwise using e.g. dask
        resources.
    :param bool do_wstacking: Whether to enable W-stacking for gridding. Always
        leave set to True, except for special testing purposes.
    """

    epsilon: float = 1e-4
    nthreads: Optional[int] = 1
    do_wstacking: bool = True

    def __post_init__(self):
        assert_or_value_error(
            self.epsilon > 0, "epsilon parameter for DUCC must be > 0"
        )
        actual_nthreads = (
            int(self.nthreads)
            if self.nthreads is not None and self.nthreads >= 1
            else os.cpu_count()
        )
        # Have to do this because this dataclass is "officially" immutable
        object.__setattr__(self, "nthreads", actual_nthreads)

    @property
    def output_dim_order(self) -> tuple[Literal["l", "m"]]:
        return ("l", "m")

    @property
    def l_step_sign(self) -> Literal[-1, +1]:
        return -1

    @property
    def m_step_sign(self) -> Literal[-1, +1]:
        return -1

    def _execute(
        self, xds: xarray.Dataset, image_params: ImageParams
    ) -> xarray.DataArray:
        return invert_ducc(xds, self, image_params)


def invert_ducc(
    xds: xarray.Dataset,
    backend: DuccInvert,
    image_params: ImageParams,
) -> xarray.DataArray:
    """
    Invert an xradio visibility dataset using DUCC. For each polarization
    channel, all visibilities are gridded on the same grid. Returns a
    DataArray with 3 dimensions (polarization, l, m), without any coordinate
    values or attributes.
    """
    image_dataarray: xarray.DataArray = xarray.apply_ufunc(
        _invert_ducc_singlepol,
        _reshape_vis_like_dataarray(xds.VISIBILITY),
        _reshape_vis_like_dataarray(xds.WEIGHT),
        _reshape_vis_like_dataarray(xds.FLAG),
        _reshape_uvw_dataarray(xds.UVW),
        # NOTE: implicitly assuming that frequency is in Hz
        xds.frequency,
        input_core_dims=[
            ("row", "frequency"),
            ("row", "frequency"),
            ("row", "frequency"),
            ("row", "uvw_label"),
            ("frequency",),
        ],
        output_core_dims=[tuple(backend.output_dim_order)],
        vectorize=True,
        kwargs={
            "image_params": image_params,
            "backend": backend,
        },
        # When the underlying arrays are dask-backed, the arguments below make
        # the application of the function lazy.
        dask="parallelized",
        dask_gufunc_kwargs={
            "output_sizes": {
                "l": image_params.num_pixels_l,
                "m": image_params.num_pixels_m,
            },
            # Unless allow_rechunk=True,
            # "chunk sizes need to match and core dimensions are to consist
            # only of one chunk."
            # We have to set it to True for the function to work when the
            # input has multiple chunks along time, baseline_id or freq,
            # because:
            # - We're gridding the entire input dataset onto the same grid
            # - The gridder needs all the data in memory before being called
            "allow_rechunk": True,
        },
        # Providing `output_dtypes` prevents apply_ufunc from doing automatic
        # output dtype inference, which it does by running the function on
        # small arrays of zeros, and that could crash in some cases.
        output_dtypes=["float32"],
    )
    return image_dataarray


def _reshape_vis_like_dataarray(arr: xarray.DataArray) -> xarray.DataArray:
    """
    Prepare visibility-shaped arrays for processing with DUCC, that is:
    VISIBILITY, WEIGHT and FLAG.

    DUCC wants the data in (row, freq) order, where a "row" is a
    (time, baseline) combination. It also prefers the data to be contiguous
    in memory (C order).
    """
    return (
        # Combine time and baseline into new "row" dimension
        arr.stack({"row": ("time", "baseline_id")})
        # Stacked dimension "row" is automatically placed at the end, but
        # we want a specific dim order
        .transpose("polarization", "row", "frequency")
        # Make contiguous in memory
        .astype(arr.dtype, order="C")
    )


def _reshape_uvw_dataarray(arr: xarray.DataArray) -> xarray.DataArray:
    """
    Prepare UVW array for processing with DUCC.
    """
    return (
        arr.stack({"row": ("time", "baseline_id")})
        .transpose("row", "uvw_label")
        .astype(arr.dtype, order="C")
    )


def _available_threads() -> int:
    try:
        from dask.distributed import get_worker

        worker = get_worker()
        return worker.state.nthreads
    except (ImportError, ValueError):
        return os.cpu_count()


@float32_result
def _invert_ducc_singlepol(
    vis: NDArray,
    weight: NDArray,
    flag: NDArray,
    uvw: NDArray,
    freq: NDArray,
    *,
    image_params: ImageParams,
    backend: DuccInvert,
) -> NDArray:
    """
    Simple wrapper for the DUCC invert function `ms2dirty()`, which also
    - normalises the image data by the total gridding weight
    - ensures the desired number of DUCC threads is used

    Expected array shapes are the same as what `ms2dirty()` expects.
    """
    # Take into account data weights and flags simultaneously.
    # Makes it easier to normalize the image.
    effective_weights = weight * np.logical_not(flag)
    total_weight = effective_weights.sum()

    # If all the data is effectively masked, the image is only zeros
    if not total_weight > 0.0:
        return np.zeros(
            shape=(
                image_params.num_pixels_l,
                image_params.num_pixels_m,
            ),
            dtype="float32",
        )

    # NOTE: If OMP_NUM_THREADS is set, DUCC caps its number of threads to that
    # value by default, and dask workers are usually launched with
    # OMP_NUM_THREADS=1. This call allows us to get the number of threads we
    # want, but no more than what's actually available.
    resize_thread_pool(_available_threads())

    image_arr = ms2dirty(
        uvw,
        freq,
        vis,
        effective_weights,
        image_params.num_pixels_l,
        image_params.num_pixels_m,
        image_params.pixel_size_lm,
        image_params.pixel_size_lm,
        epsilon=backend.epsilon,
        do_wstacking=backend.do_wstacking,
        nthreads=backend.nthreads,
        mask=None,  # already accounted for in `effective_weights`
    )

    # Normalise
    return image_arr / total_weight
