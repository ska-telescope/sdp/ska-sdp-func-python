"""
ImageParams class.
"""

import math
from dataclasses import dataclass

from .utils import assert_or_value_error


@dataclass(frozen=True)
class ImageParams:
    """
    Validate and store requested image dimensions and pixel size.
    """

    num_pixels_l: int
    num_pixels_m: int
    pixel_size_radians: float

    def __post_init__(self):
        assert_or_value_error(
            self.pixel_size_radians > 0, "Pixel size must be > 0"
        )
        assert_or_value_error(
            self.num_pixels_l > 0,
            "Number of pixels along l dimension must be > 0",
        )
        assert_or_value_error(
            self.num_pixels_m > 0,
            "Number of pixels along m dimension must be > 0",
        )
        assert_or_value_error(
            self.num_pixels_l % 2 == 0,
            "Number of pixels along l dimension must be even",
        )
        assert_or_value_error(
            self.num_pixels_m % 2 == 0,
            "Number of pixels along m dimension must be even",
        )

    @property
    def pixel_size_lm(self) -> float:
        """
        Pixel size in direction cosines, that is in (l, m) coordinates.
        """

        # NOTE: This formula assumes that:
        # 1. `pixel_size_radians` is the angle subtended by the reference pixel
        #    on the sky.
        # 2. The origin of the tangent plane (in practice, the phase centre)
        #    lies at the centre of the reference pixel.
        return 2 * math.sin(self.pixel_size_radians / 2.0)
