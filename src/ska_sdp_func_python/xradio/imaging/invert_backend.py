"""
Base class for invert backends.
"""

import abc
from typing import Literal

import numpy as np
import xarray
from numpy.typing import NDArray

from .image_params import ImageParams


class InvertBackend(abc.ABC):
    """
    Abstract base class for invert backends. Exists mainly because different
    gridders/invert backends have different conventions for:
    - Image orientation
    - Memory layout of the image array returned
    """

    @property
    @abc.abstractmethod
    def output_dim_order(self) -> tuple[Literal["l", "m"]]:
        """
        Order (in C convention) of "l" and "m" dimensions in the output 2D
        images. The last dimension is the one that is contiguous in memory.
        """

    @property
    @abc.abstractmethod
    def l_step_sign(self) -> Literal[-1, +1]:
        """
        Sign of the `l` coordinate step to use when tagging the image array
        axes returned by the gridder.
        """

    @property
    @abc.abstractmethod
    def m_step_sign(self) -> Literal[-1, +1]:
        """
        Sign of the `m` coordinate step to use when tagging the image array
        axes returned by the gridder.
        """

    def l_coords(self, image_params: ImageParams) -> NDArray:
        """
        Coordinate values along the l dimension.
        """
        npix = image_params.num_pixels_l
        step = image_params.pixel_size_lm
        sign = self.l_step_sign
        return (sign * step) * (np.arange(npix) - npix // 2)

    def m_coords(self, image_params: ImageParams) -> NDArray:
        """
        Coordinate values along the m dimension.
        """
        npix = image_params.num_pixels_m
        step = image_params.pixel_size_lm
        sign = self.m_step_sign
        return (sign * step) * (np.arange(npix) - npix // 2)

    @abc.abstractmethod
    def _execute(
        self, xds: xarray.Dataset, image_params: ImageParams
    ) -> xarray.DataArray:
        """
        Internally used to run invert using this backend.
        Returns a DataArray with 3 dimensions (polarization, l, m)
        containing the image data.
        """
