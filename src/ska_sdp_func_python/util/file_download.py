"""
File download utilities.
"""

import logging
import os
import time

import requests

log = logging.getLogger("func-python-logger")


def _download_file(url, local_filename):
    """
    Download a file from a URL to a local file.

    :param url: The URL to download from.
    :param local_filename: The local file to save the download to.
    :return: True if the download was successful, False otherwise.
    """
    try:
        # Ensure the directory exists
        os.makedirs(os.path.dirname(local_filename), exist_ok=True)

        response = requests.get(url, stream=True, timeout=(5, 20))
        response.raise_for_status()  # Check if the request was successful
        with open(local_filename, "wb") as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)
        msg = f"Downloaded {local_filename} successfully."
        log.info(msg)
    except requests.exceptions.RequestException as err:
        msg = f"Failed to download {local_filename}: {err}"
        log.error(msg)
        return False
    return True


def download_with_retry(url, local_filename, retries=1):
    """
    Download a file with retries on failure.

    :param url: The URL to download from.
    :local_filename: The local file to save the download to.
    :retries: The number of retries to attempt on failure.
    """

    for attempt in range(retries + 1):
        if _download_file(url, local_filename):
            break
        if attempt < retries:
            msg = f"Retrying download ({attempt + 1}/{retries})..."
            log.warning(msg)
            time.sleep(2)  # Wait for 2 seconds before retrying
    else:
        msg = f"Failed to download {local_filename} after {retries} retries."
        log.error(msg)
