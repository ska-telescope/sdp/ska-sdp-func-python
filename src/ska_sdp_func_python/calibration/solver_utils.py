"""
Utility functions used by solvers. Based on the Yandasoft algorithm.
"""

# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals
# pylint: disable=too-many-statements
# pylint: disable=invalid-name
# flake8: noqa: E501

__all__ = [
    "gen_cdm",
    "gen_coherency_products",
    "gen_pol_matrix",
    "update_design_matrix",
]

import numpy as np
from numpy import conj, imag, real


def update_design_matrix(
    A,
    dv,
    wobs,
    wmdl,
    gX,
    gY,
    dXY,
    dYX,
    ant1,
    ant2,
):
    """
    Update design matrix for a set of visibilities.

    Update the normal equation design matrix with the first derivatives of the
    real and imaginary parts of linearly polarised visibilities for each
    baseline with respect to all relevant gain and leakage free parameters.

    Based on the approach used in ASKAPSoft/YANDASoft gain and leakage solvers.

    :param A: Normal equation design matrix to update. Numpy array with
        shape [8 x num_visibility, 8 x num_antenna]. The factors of
        8 come from 4 linear polarisations multiplied by the two complex
        components (stored sequentially as real,imag).
    :param wobs: weighted visibilities. Complex Numpy array with
        shape [nvis, 2, 2]
    :param wmdl: weighted model visibilities. Complex Numpy array with
        shape [nvis, 2, 2]
    :param gX,gY,dXY,dYX: current estimates of the complex gains and
        leakage terms. Complex Numpy arrays with shape [nant, 2, 2]
    :param ant1: first antenna in each baseline with shape [nvis]
    :param ant2: second antenna in each baseline with shape [nvis]

    """
    nbl = len(ant1)
    nants = len(gX)

    # visibility indices (i.e. design matrix rows)
    kXX = 2 * np.arange(nbl)
    kYY = kXX + 2 * nbl
    kXY = kXX + 4 * nbl
    kYX = kXX + 6 * nbl

    # parameter indices (i.e. design matrix columns)
    iXX = 2 * ant1
    iYY = iXX + 2 * nants
    iXY = iXX + 4 * nants
    iYX = iXX + 6 * nants

    jXX = 2 * ant2
    jYY = jXX + 2 * nants
    jXY = jXX + 4 * nants
    jYX = jXX + 6 * nants

    XX = wmdl[:, 0, 0]
    YY = wmdl[:, 1, 1]
    XY = wmdl[:, 0, 1]
    YX = wmdl[:, 1, 0]
    gXi = gX[ant1]
    gYi = gY[ant1]
    dXYi = dXY[ant1]
    dYXi = dYX[ant1]
    gXj = gX[ant2]
    gYj = gY[ant2]
    dXYj = dXY[ant2]
    dYXj = dYX[ant2]

    # update data vector elements

    wres = wobs - wmdl

    dv[kXX] += np.real(wres[:, 0, 0])
    dv[kYY] += np.real(wres[:, 1, 1])
    dv[kXY] += np.real(wres[:, 0, 1])
    dv[kYX] += np.real(wres[:, 1, 0])

    dv[kXX + 1] += np.imag(wres[:, 0, 0])
    dv[kYY + 1] += np.imag(wres[:, 1, 1])
    dv[kXY + 1] += np.imag(wres[:, 0, 1])
    dv[kYX + 1] += np.imag(wres[:, 1, 0])

    # update design matrix elements

    # pylint: disable=line-too-long
    # derivitives of M * [XX,XY,YX,YY].T with respect to gains and leakages,
    # where M is the 4x4 tensor product of the two Jones matrices:
    # +gXi     *(gXj)',      +gXi     *(gXj*dXYj)', +gXi*dXYi*(gXj)',      +gXi*dXYi*(gXj*dXYj)',
    # -gXi     *(gYj*dYXj)', +gXi     *(gYj)',      -gXi*dXYi*(gYj*dYXj)', +gXi*dXYi*(gYj)',
    # -gYi*dYXi*(gXj)',      -gYi*dYXi*(gXj*dXYj)', +gYi     *(gXj)',      +gYi     *(gXj*dXYj)',
    # +gYi*dYXi*(gYj*dYXj)', -gYi*dYXi*(gYj)',      -gYi     *(gYj*dYXj)', +gYi     *(gYj)',

    # dV/d{gXi}

    tmpXX = (
        +conj(gXj) * XX
        + conj(gXj * dXYj) * XY
        + dXYi * conj(gXj) * YX
        + dXYi * conj(gXj * dXYj) * YY
    )
    tmpXY = (
        -conj(gYj * dYXj) * XX
        + conj(gYj) * XY
        - dXYi * conj(gYj * dYXj) * YX
        + dXYi * conj(gYj) * YY
    )

    A[kXX, iXX] += +real(tmpXX)
    A[kXY, iXX] += +real(tmpXY)

    A[kXX + 1, iXX] += +imag(tmpXX)
    A[kXY + 1, iXX] += +imag(tmpXY)

    A[kXX, iXX + 1] += -imag(tmpXX)  # imag sign:
    A[kXY, iXX + 1] += -imag(tmpXY)

    A[kXX + 1, iXX + 1] += +real(tmpXX)  # imag sign:
    A[kXY + 1, iXX + 1] += +real(tmpXY)

    # dV/d{gXj}

    tmpXX = (
        +gXi * XX
        + gXi * conj(dXYj) * XY
        + gXi * dXYi * YX
        + gXi * dXYi * conj(dXYj) * YY
    )
    tmpYX = (
        -gYi * dYXi * XX
        - gYi * dYXi * conj(dXYj) * YX
        + gYi * YX
        + gYi * conj(dXYj) * YY
    )

    A[kXX, jXX] += +real(tmpXX)
    A[kYX, jXX] += +real(tmpYX)

    A[kXX + 1, jXX] += +imag(tmpXX)
    A[kYX + 1, jXX] += +imag(tmpYX)

    A[kXX, jXX + 1] += +imag(tmpXX)  # imag sign:
    A[kYX, jXX + 1] += +imag(tmpYX)

    A[kXX + 1, jXX + 1] += -real(tmpXX)  # imag sign:
    A[kYX + 1, jXX + 1] += -real(tmpYX)

    # dV/d{gYi}

    tmpYX = (
        -dYXi * conj(gXj) * XX
        - dYXi * conj(gXj * dXYj) * YX
        + conj(gXj) * YX
        + conj(gXj * dXYj) * YY
    )
    tmpYY = (
        +dYXi * conj(gYj * dYXj) * XX
        - dYXi * conj(gYj) * XY
        - conj(gYj * dYXj) * YX
        + conj(gYj) * YY
    )

    A[kYX, iYY] += +real(tmpYX)
    A[kYY, iYY] += +real(tmpYY)

    A[kYX + 1, iYY] += +imag(tmpYX)
    A[kYY + 1, iYY] += +imag(tmpYY)

    A[kYX, iYY + 1] += -imag(tmpYX)
    A[kYY, iYY + 1] += -imag(tmpYY)

    A[kYX + 1, iYY + 1] += +real(tmpYX)
    A[kYY + 1, iYY + 1] += +real(tmpYY)

    # dV/d{gYj}

    tmpXY = (
        -gXi * conj(dYXj) * XX
        + gXi * XY
        - gXi * dXYi * conj(dYXj) * YX
        + gXi * dXYi * YY
    )
    tmpYY = (
        +gYi * dYXi * conj(dYXj) * XX
        - gYi * dYXi * XY
        - gYi * conj(dYXj) * YX
        + gYi * YY
    )

    A[kXY, jYY] += +real(tmpXY)
    A[kYY, jYY] += +real(tmpYY)

    A[kXY + 1, jYY] += +imag(tmpXY)
    A[kYY + 1, jYY] += +imag(tmpYY)

    A[kXY, jYY + 1] += +imag(tmpXY)
    A[kYY, jYY + 1] += +imag(tmpYY)

    A[kXY + 1, jYY + 1] += -real(tmpXY)
    A[kYY + 1, jYY + 1] += -real(tmpYY)

    # dV/d{dXYi}

    tmpXX = +gXi * conj(gXj) * YX + gXi * conj(gXj * dXYj) * YY
    tmpXY = -gXi * conj(gYj * dYXj) * YX + gXi * conj(gYj) * YY

    A[kXX, iXY] += +real(tmpXX)
    A[kXY, iXY] += +real(tmpXY)

    A[kXX + 1, iXY] += +imag(tmpXX)
    A[kXY + 1, iXY] += +imag(tmpXY)

    A[kXX, iXY + 1] += -imag(tmpXX)  # imag sign:
    A[kXY, iXY + 1] += -imag(tmpXY)

    A[kXX + 1, iXY + 1] += +real(tmpXX)  # imag sign:
    A[kXY + 1, iXY + 1] += +real(tmpXY)

    # dV/d{dXYj}

    tmpXX = +gXi * conj(gXj) * XY + gXi * dXYi * conj(gXj) * YY
    tmpYX = -gYi * dYXi * conj(gXj) * YX + gYi * conj(gXj) * YY

    A[kXX, jXY] += +real(tmpXX)
    A[kYX, jXY] += +real(tmpYX)

    A[kXX + 1, jXY] += +imag(tmpXX)
    A[kYX + 1, jXY] += +imag(tmpYX)

    A[kXX, jXY + 1] += +imag(tmpXX)  # imag sign:
    A[kYX, jXY + 1] += +imag(tmpYX)

    A[kXX + 1, jXY + 1] += -real(tmpXX)  # imag sign:
    A[kYX + 1, jXY + 1] += -real(tmpYX)

    # dV/d{dYXi}

    tmpYX = -gYi * conj(gXj) * XX - gYi * conj(gXj * dXYj) * YX
    tmpYY = +gYi * conj(gYj * dYXj) * XX - gYi * conj(gYj) * XY

    A[kYX, iYX] += +real(tmpYX)
    A[kYY, iYX] += +real(tmpYY)

    A[kYX + 1, iYX] += +imag(tmpYX)
    A[kYY + 1, iYX] += +imag(tmpYY)

    A[kYX, iYX + 1] += -imag(tmpYX)
    A[kYY, iYX + 1] += -imag(tmpYY)

    A[kYX + 1, iYX + 1] += +real(tmpYX)
    A[kYY + 1, iYX + 1] += +real(tmpYY)

    # dV/d{dYXj}

    tmpXY = -gXi * conj(gYj) * XX - gXi * dXYi * conj(gYj) * YX
    tmpYY = +gYi * dYXi * conj(gYj) * XX - gYi * conj(gYj) * YX

    A[kXY, jYX] += +real(tmpXY)
    A[kYY, jYX] += +real(tmpYY)

    A[kXY + 1, jYX] += +imag(tmpXY)
    A[kYY + 1, jYX] += +imag(tmpYY)

    A[kXY, jYX + 1] += +imag(tmpXY)
    A[kYY, jYX + 1] += +imag(tmpYY)

    A[kXY + 1, jYX + 1] += -real(tmpXY)
    A[kYY + 1, jYX + 1] += -real(tmpYY)


def gen_cdm(gXi, gYi, dXYi, dYXi, gXj, gYj, dXYj, dYXj):
    """
    Generate Complex Diff matrix for baseline i-j.

    Generate a 4x4 matrix containing the derivatives with respect to each
    relevant gain and leakage free parameter for this baseline. Polarisation
    ordering is XX XY YX YY.

    Based on the approach used in ASKAPSoft/YANDASoft gain and leakage solvers.

    :param gXi,gYi,dXYi,dYXi: current estimates of the complex gain and leakage
        terms for the first antenna in the baseline
    :param gXj,gYj,dXYj,dYXj: current estimates of the complex gain and leakage
        terms for the second antenna in the baseline
    :return: List containing the 16 derivative 4x4 matrices

    """
    dfdgXiRe = real(
        np.array(
            [
                [
                    +conj(gXj),
                    +conj(gXj * dXYj),
                    +dXYi * conj(gXj),
                    +dXYi * conj(gXj * dXYj),
                ],
                [
                    -conj(gYj * dYXj),
                    +conj(gYj),
                    -dXYi * conj(gYj * dYXj),
                    +dXYi * conj(gYj),
                ],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )
    ) + 1j * imag(
        np.array(
            [
                [
                    +conj(gXj),
                    +conj(gXj * dXYj),
                    +dXYi * conj(gXj),
                    +dXYi * conj(gXj * dXYj),
                ],
                [
                    -conj(gYj * dYXj),
                    +conj(gYj),
                    -dXYi * conj(gYj * dYXj),
                    +dXYi * conj(gYj),
                ],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )
    )

    dfdgXiIm = -imag(
        np.array(
            [
                [
                    +conj(gXj),
                    +conj(gXj * dXYj),
                    +dXYi * conj(gXj),
                    +dXYi * conj(gXj * dXYj),
                ],
                [
                    -conj(gYj * dYXj),
                    +conj(gYj),
                    -dXYi * conj(gYj * dYXj),
                    +dXYi * conj(gYj),
                ],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )
    ) + 1j * real(
        np.array(
            [
                [
                    +conj(gXj),
                    +conj(gXj * dXYj),
                    +dXYi * conj(gXj),
                    +dXYi * conj(gXj * dXYj),
                ],
                [
                    -conj(gYj * dYXj),
                    +conj(gYj),
                    -dXYi * conj(gYj * dYXj),
                    +dXYi * conj(gYj),
                ],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )
    )

    dfdgXjRe = real(
        np.array(
            [
                [
                    +gXi,
                    +gXi * conj(dXYj),
                    +gXi * dXYi,
                    +gXi * dXYi * conj(dXYj),
                ],
                [0, 0, 0, 0],
                [
                    -gYi * dYXi,
                    -gYi * dYXi * conj(dXYj),
                    +gYi,
                    +gYi * conj(dXYj),
                ],
                [0, 0, 0, 0],
            ]
        )
    ) + 1j * imag(
        np.array(
            [
                [
                    +gXi,
                    +gXi * conj(dXYj),
                    +gXi * dXYi,
                    +gXi * dXYi * conj(dXYj),
                ],
                [0, 0, 0, 0],
                [
                    -gYi * dYXi,
                    -gYi * dYXi * conj(dXYj),
                    +gYi,
                    +gYi * conj(dXYj),
                ],
                [0, 0, 0, 0],
            ]
        )
    )

    dfdgXjIm = imag(
        np.array(
            [
                [
                    +gXi,
                    +gXi * conj(dXYj),
                    +gXi * dXYi,
                    +gXi * dXYi * conj(dXYj),
                ],
                [0, 0, 0, 0],
                [
                    -gYi * dYXi,
                    -gYi * dYXi * conj(dXYj),
                    +gYi,
                    +gYi * conj(dXYj),
                ],
                [0, 0, 0, 0],
            ]
        )
    ) - 1j * real(
        np.array(
            [
                [
                    +gXi,
                    +gXi * conj(dXYj),
                    +gXi * dXYi,
                    +gXi * dXYi * conj(dXYj),
                ],
                [0, 0, 0, 0],
                [
                    -gYi * dYXi,
                    -gYi * dYXi * conj(dXYj),
                    +gYi,
                    +gYi * conj(dXYj),
                ],
                [0, 0, 0, 0],
            ]
        )
    )

    dfdgYiRe = real(
        np.array(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [
                    -dYXi * conj(gXj),
                    -dYXi * conj(gXj * dXYj),
                    +conj(gXj),
                    +conj(gXj * dXYj),
                ],
                [
                    +dYXi * conj(gYj * dYXj),
                    -dYXi * conj(gYj),
                    -conj(gYj * dYXj),
                    +conj(gYj),
                ],
            ]
        )
    ) + 1j * imag(
        np.array(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [
                    -dYXi * conj(gXj),
                    -dYXi * conj(gXj * dXYj),
                    +conj(gXj),
                    +conj(gXj * dXYj),
                ],
                [
                    +dYXi * conj(gYj * dYXj),
                    -dYXi * conj(gYj),
                    -conj(gYj * dYXj),
                    +conj(gYj),
                ],
            ]
        )
    )

    dfdgYiIm = -imag(
        np.array(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [
                    -dYXi * conj(gXj),
                    -dYXi * conj(gXj * dXYj),
                    +conj(gXj),
                    +conj(gXj * dXYj),
                ],
                [
                    +dYXi * conj(gYj * dYXj),
                    -dYXi * conj(gYj),
                    -conj(gYj * dYXj),
                    +conj(gYj),
                ],
            ]
        )
    ) + 1j * real(
        np.array(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [
                    -dYXi * conj(gXj),
                    -dYXi * conj(gXj * dXYj),
                    +conj(gXj),
                    +conj(gXj * dXYj),
                ],
                [
                    +dYXi * conj(gYj * dYXj),
                    -dYXi * conj(gYj),
                    -conj(gYj * dYXj),
                    +conj(gYj),
                ],
            ]
        )
    )

    dfdgYjRe = real(
        np.array(
            [
                [0, 0, 0, 0],
                [
                    -gXi * conj(dYXj),
                    +gXi,
                    -gXi * dXYi * conj(dYXj),
                    +gXi * dXYi,
                ],
                [0, 0, 0, 0],
                [
                    +gYi * dYXi * conj(dYXj),
                    -gYi * dYXi,
                    -gYi * conj(dYXj),
                    +gYi,
                ],
            ]
        )
    ) + 1j * imag(
        np.array(
            [
                [0, 0, 0, 0],
                [
                    -gXi * conj(dYXj),
                    +gXi,
                    -gXi * dXYi * conj(dYXj),
                    +gXi * dXYi,
                ],
                [0, 0, 0, 0],
                [
                    +gYi * dYXi * conj(dYXj),
                    -gYi * dYXi,
                    -gYi * conj(dYXj),
                    +gYi,
                ],
            ]
        )
    )

    dfdgYjIm = imag(
        np.array(
            [
                [0, 0, 0, 0],
                [
                    -gXi * conj(dYXj),
                    +gXi,
                    -gXi * dXYi * conj(dYXj),
                    +gXi * dXYi,
                ],
                [0, 0, 0, 0],
                [
                    +gYi * dYXi * conj(dYXj),
                    -gYi * dYXi,
                    -gYi * conj(dYXj),
                    +gYi,
                ],
            ]
        )
    ) - 1j * real(
        np.array(
            [
                [0, 0, 0, 0],
                [
                    -gXi * conj(dYXj),
                    +gXi,
                    -gXi * dXYi * conj(dYXj),
                    +gXi * dXYi,
                ],
                [0, 0, 0, 0],
                [
                    +gYi * dYXi * conj(dYXj),
                    -gYi * dYXi,
                    -gYi * conj(dYXj),
                    +gYi,
                ],
            ]
        )
    )

    dfddXYiRe = real(
        np.array(
            [
                [0, 0, +gXi * conj(gXj), +gXi * conj(gXj * dXYj)],
                [0, 0, -gXi * conj(gYj * dYXj), +gXi * conj(gYj)],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )
    ) + 1j * imag(
        np.array(
            [
                [0, 0, +gXi * conj(gXj), +gXi * conj(gXj * dXYj)],
                [0, 0, -gXi * conj(gYj * dYXj), +gXi * conj(gYj)],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )
    )

    dfddXYiIm = -imag(
        np.array(
            [
                [0, 0, +gXi * conj(gXj), +gXi * conj(gXj * dXYj)],
                [0, 0, -gXi * conj(gYj * dYXj), +gXi * conj(gYj)],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )
    ) + 1j * real(
        np.array(
            [
                [0, 0, +gXi * conj(gXj), +gXi * conj(gXj * dXYj)],
                [0, 0, -gXi * conj(gYj * dYXj), +gXi * conj(gYj)],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        )
    )

    dfddXYjRe = real(
        np.array(
            [
                [0, +gXi * conj(gXj), 0, +gXi * dXYi * conj(gXj)],
                [0, 0, 0, 0],
                [0, -gYi * dYXi * conj(gXj), 0, +gYi * conj(gXj)],
                [0, 0, 0, 0],
            ]
        )
    ) + 1j * imag(
        np.array(
            [
                [0, +gXi * conj(gXj), 0, +gXi * dXYi * conj(gXj)],
                [0, 0, 0, 0],
                [0, -gYi * dYXi * conj(gXj), 0, +gYi * conj(gXj)],
                [0, 0, 0, 0],
            ]
        )
    )

    dfddXYjIm = imag(
        np.array(
            [
                [0, +gXi * conj(gXj), 0, +gXi * dXYi * conj(gXj)],
                [0, 0, 0, 0],
                [0, -gYi * dYXi * conj(gXj), 0, +gYi * conj(gXj)],
                [0, 0, 0, 0],
            ]
        )
    ) - 1j * real(
        np.array(
            [
                [0, +gXi * conj(gXj), 0, +gXi * dXYi * conj(gXj)],
                [0, 0, 0, 0],
                [0, -gYi * dYXi * conj(gXj), 0, +gYi * conj(gXj)],
                [0, 0, 0, 0],
            ]
        )
    )

    dfddYXiRe = real(
        np.array(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [-gYi * conj(gXj), -gYi * conj(gXj * dXYj), 0, 0],
                [+gYi * conj(gYj * dYXj), -gYi * conj(gYj), 0, 0],
            ]
        )
    ) + 1j * imag(
        np.array(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [-gYi * conj(gXj), -gYi * conj(gXj * dXYj), 0, 0],
                [+gYi * conj(gYj * dYXj), -gYi * conj(gYj), 0, 0],
            ]
        )
    )

    dfddYXiIm = -imag(
        np.array(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [-gYi * conj(gXj), -gYi * conj(gXj * dXYj), 0, 0],
                [+gYi * conj(gYj * dYXj), -gYi * conj(gYj), 0, 0],
            ]
        )
    ) + 1j * real(
        np.array(
            [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [-gYi * conj(gXj), -gYi * conj(gXj * dXYj), 0, 0],
                [+gYi * conj(gYj * dYXj), -gYi * conj(gYj), 0, 0],
            ]
        )
    )

    dfddYXjRe = real(
        np.array(
            [
                [0, 0, 0, 0],
                [-gXi * conj(gYj), 0, -gXi * dXYi * conj(gYj), 0],
                [0, 0, 0, 0],
                [+gYi * dYXi * conj(gYj), 0, -gYi * conj(gYj), 0],
            ]
        )
    ) + 1j * imag(
        np.array(
            [
                [0, 0, 0, 0],
                [-gXi * conj(gYj), 0, -gXi * dXYi * conj(gYj), 0],
                [0, 0, 0, 0],
                [+gYi * dYXi * conj(gYj), 0, -gYi * conj(gYj), 0],
            ]
        )
    )

    dfddYXjIm = imag(
        np.array(
            [
                [0, 0, 0, 0],
                [-gXi * conj(gYj), 0, -gXi * dXYi * conj(gYj), 0],
                [0, 0, 0, 0],
                [+gYi * dYXi * conj(gYj), 0, -gYi * conj(gYj), 0],
            ]
        )
    ) - 1j * real(
        np.array(
            [
                [0, 0, 0, 0],
                [-gXi * conj(gYj), 0, -gXi * dXYi * conj(gYj), 0],
                [0, 0, 0, 0],
                [+gYi * dYXi * conj(gYj), 0, -gYi * conj(gYj), 0],
            ]
        )
    )

    return [
        dfdgXiRe,
        dfdgXiIm,
        dfdgXjRe,
        dfdgXjIm,
        dfdgYiRe,
        dfdgYiIm,
        dfdgYjRe,
        dfdgYjIm,
        dfddXYiRe,
        dfddXYiIm,
        dfddXYjRe,
        dfddXYjIm,
        dfddYXiRe,
        dfddYXiIm,
        dfddYXjRe,
        dfddYXjIm,
    ]


def gen_pol_matrix(gXi, gYi, dXYi, dYXi, gXj, gYj, dXYj, dYXj):
    """
    Generate the 4x4 gain matrix for baseline i-j.

    Note: has XX XY YX YY ordering

    :param gXi,gYi,dXYi,dYXi: current estimates of the complex gain and leakage
        terms for the first antenna in the baseline
    :param gXj,gYj,dXYj,dYXj: current estimates of the complex gain and leakage
        terms for the second antenna in the baseline
    :return: complex np array containing the 4x4 gain matrix

    """
    M = [
        [
            +gXi * conj(gXj),
            +gXi * conj(gXj * dXYj),
            +gXi * dXYi * conj(gXj),
            +gXi * dXYi * conj(gXj * dXYj),
        ],
        [
            -gXi * conj(gYj * dYXj),
            +gXi * conj(gYj),
            -gXi * dXYi * conj(gYj * dYXj),
            +gXi * dXYi * conj(gYj),
        ],
        [
            -gYi * dYXi * conj(gXj),
            -gYi * dYXi * conj(gXj * dXYj),
            +gYi * conj(gXj),
            +gYi * conj(gXj * dXYj),
        ],
        [
            +gYi * dYXi * conj(gYj * dYXj),
            -gYi * dYXi * conj(gYj),
            -gYi * conj(gYj * dYXj),
            +gYi * conj(gYj),
        ],
    ]

    return np.array(M)


def gen_coherency_products(Som, Smm, vobs, vmdl, wgt, ant, ant1, ant2):
    """
    Accumulate jones_substitution products for antenna ant.

    Generate the 2x2 accumulations of coherency matrix products for antenna
    ant. Using a boolean mask for each antenna and expanding all of the matrix
    multiplication operations is faster than looping over baselines.

    :param Som: accumulation matrices for observed vis multiplied by the
        Hermitian transpose of model vis [nant, nfreq, 2, 2]
    :param Smm: accumulation matrices for model vis multiplied by the
        Hermitian transpose of model vis [nant, nfreq, 2, 2]
    :param vobs: observed vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param vmdl: model vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param wgt: vis weights ndarray [ntime, nbaseline, nfreq]
    :param ant: integer index of antenna to accumulate
    :param ant1: ndarray containing the first antenna in each baseline
    :param ant2: ndarray containing the second antenna in each baseline

    """
    # update sums for station 1
    ind = ant1 == ant  # all baselines with ant as the first antenna
    ind *= ant2 != ant  # make sure they are cross-correlations

    Som += np.einsum(
        "tbf,tbfpi,tbfqi->pq",
        wgt[:, ind, :],
        vobs[:, ind, :, :, :],
        vmdl[:, ind, :, :, :].conj(),
    )

    Smm += np.einsum(
        "tbf,tbfpi,tbfqi->pq",
        wgt[:, ind, :],
        vmdl[:, ind, :, :, :],
        vmdl[:, ind, :, :, :].conj(),
    )

    # update sums for station 2
    ind = ant2 == ant  # all baselines with ant as the second antenna
    ind *= ant1 != ant  # make sure they are cross-correlations

    Som += np.einsum(
        "tbf,tbfip,tbfiq->pq",
        wgt[:, ind, :],
        vobs[:, ind, :, :, :].conj(),
        vmdl[:, ind, :, :, :],
    )

    Smm += np.einsum(
        "tbf,tbfip,tbfiq->pq",
        wgt[:, ind, :],
        vmdl[:, ind, :, :, :].conj(),
        vmdl[:, ind, :, :, :],
    )
