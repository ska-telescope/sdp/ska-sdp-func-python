"""
Alternative functions to solve for antenna/station gain.
"""

# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals
# pylint: disable=invalid-name
# flake8: noqa E203 # if I fix these flake8 white spaces, black complains...

__all__ = ["solve_with_alternative_algorithm"]

import logging

import numpy
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import lsmr
from ska_sdp_datamodels.visibility.vis_model import Visibility

from ska_sdp_func_python.calibration.solver_utils import (
    gen_cdm,
    gen_coherency_products,
    gen_pol_matrix,
    update_design_matrix,
)

log = logging.getLogger("func-python-logger")


def _jones_phase_referencing(
    jones,
    refant,
):
    """
    Simple phase referencing for gain and leakage terms.
    This may impact other terms like xy-phase, so use with care.

    :param jones: Numpy array containing Jones matrices to be updated
    :param refant: Reference antenna (default 0).
    """

    angleX = numpy.angle(jones[refant, :, 0, 0])
    angleY = numpy.angle(jones[refant, :, 1, 1])
    jones[..., 0, 0] *= numpy.exp(-1j * angleX)
    jones[..., 0, 1] *= numpy.exp(-1j * angleY)
    jones[..., 1, 0] *= numpy.exp(-1j * angleX)
    jones[..., 1, 1] *= numpy.exp(-1j * angleY)


def solve_with_alternative_algorithm(
    solver,
    vis: Visibility,
    modelvis: Visibility,
    gain_table,
    niter=100,
    row=0,
    tol=1e-6,
):
    """
    Solve this row (time slice) of the gain table.

    :param solver: Calibration solver to use. Options are:
        "jones_substitution", "normal_equations" and "normal_equations_presum"
    :param vis: Visibility containing the observed data_models
    :param modelvis: Visibility containing the visibility predicted by a model
    :param gain_table: Gaintable to be updated
    :param niter: Maximum number of iterations (default=100)
    :param row: Time slice to be calibrated (default=0)
    :param tol: Iteration stops when the fractional change
        in the gain solution is below this tolerance (default=1e-6)
    :return: GainTable gain_table, containing solution
    """
    gain = gain_table["gain"].data[row, ...]
    _, nchan_gt, nrec1, nrec2 = gain.shape
    ntime, nbl, nchan_vis, npol_vis = vis.vis.shape
    assert nrec1 == 2
    assert nrec1 == nrec2
    assert nrec1 * nrec2 == npol_vis
    assert nchan_gt in (1, nchan_vis)

    # incorporate flags into weights
    wgt = vis.weight.data * (1 - vis.flags.data)
    # flag the whole Jones matrix if any element is flagged
    wgt *= numpy.all(wgt > 0, axis=-1, keepdims=True)
    # reduce the dimension to a single weight per matrix
    #  - could weight pols separately, but may be better not to
    wgt = wgt[..., 0]

    vmdl = modelvis.vis.data.reshape(ntime, nbl, nchan_vis, 2, 2)
    vobs = vis.vis.data.reshape(ntime, nbl, nchan_vis, 2, 2)

    # Update model if a starting solution is given.
    I2 = numpy.eye(2)
    ant1 = vis.antenna1.data
    ant2 = vis.antenna2.data
    if numpy.any(gain[..., :, :] != I2):
        vmdl = numpy.einsum(
            "bfpi,tbfij,bfqj->tbfpq",
            gain[ant1],
            vmdl,
            gain[ant2].conj(),
        )

    log.debug(
        "solve_with_alternative_algorithm: "
        + "solving for %d chan in %d sub-band[s] using solver %s",
        nchan_vis,
        nchan_gt,
        solver,
    )

    for ch in range(nchan_gt):
        # select channels to average over. Just the current one if solving
        # each channel separately, or all of them if this is a joint solution.
        chan_vis = [ch] if nchan_gt == nchan_vis else range(nchan_vis)

        log.debug(
            "solve_with_alternative_algorithm: "
            + "sub-band %d, processing %d channels:",
            ch,
            len(chan_vis),
        )

        # could handle this with classes or similar, but keep it simple for now
        if solver == "jones_substitution":
            _jones_sub_solve(
                vobs[:, :, chan_vis],
                vmdl[:, :, chan_vis],
                wgt[:, :, chan_vis],
                ant1,
                ant2,
                gain,
                ch,
                niter,
                tol,
            )

        elif solver == "normal_equations":
            _normal_equation_solve(
                vobs[:, :, chan_vis],
                vmdl[:, :, chan_vis],
                wgt[:, :, chan_vis],
                ant1,
                ant2,
                gain,
                ch,
                niter,
                tol,
            )

        elif solver == "normal_equations_presum":
            _normal_equation_solve_with_presumming(
                vobs[:, :, chan_vis],
                vmdl[:, :, chan_vis],
                wgt[:, :, chan_vis],
                ant1,
                ant2,
                gain,
                ch,
                niter,
                tol,
            )

        elif solver == "gain_substitution":
            raise ValueError(
                "solve_with_alternative_algorithm: "
                + f"solver {solver} cannot be used in this function",
            )
        else:
            raise ValueError(
                f"solve_with_alternative_algorithm: unknown solver: {solver}",
            )

    # _jones_phase_referencing(gain, refant)

    # update gain_table in case the data reference became a copy
    gain_table["gain"].data[row, ...] = gain

    return gain_table


def _jones_sub_solve(
    vobs,
    vmdl,
    wgt,
    ant1,
    ant2,
    gain,
    ch,
    niter,
    tol,
):
    """
    Solve this time and frequency slice of the gain table

    Within each iteration the solver performs an independent least-squares
    optimisation for each antenna Jones matrix. It is based on the equivalent
    solver in the MWA RealTime System (Mitchell et at., 2008, IEEE JSTSP, 2,
    JSTSP.2008.2005327).

    :param vobs: observed vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param vmdl: model vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param wgt: vis weights ndarray [ntime, nbaseline, nfreq]
    :param ant1: ndarray containing the first antenna in each baseline
    :param ant2: ndarray containing the second antenna in each baseline
    :param gain: gain ndarray to be updated [ntime, nbaseline, nfreq, 2, 2]
    :param ch: gain table channel to be updated
    :param niter: Maximum number of iterations
    :param tol: Iteration stops when the fractional change
        in the gain solution is below this tolerance
    """
    nants = gain.shape[0]

    I2 = numpy.eye(2)

    for it in range(niter):
        rawupdate = _jones_substitution(vobs, vmdl, wgt, nants, ant1, ant2)

        # alternativing the step size leads to better convergence
        nu = 1.0 - 0.5 * (it % 2)
        # nu = 0.5

        jonesupdate = I2 + nu * (rawupdate[..., :, :] - I2)

        # update model vis
        vmdl = numpy.einsum(
            "bpi,tbfij,bqj->tbfpq",
            jonesupdate[ant1],
            vmdl,
            jonesupdate[ant2].conj(),
        )

        # update cumulative Jones matrices
        gain[:, ch] = numpy.einsum(
            "...pi,...iq->...pq", jonesupdate, gain[:, ch]
        )

        change = numpy.max(numpy.abs(rawupdate[..., :, :] - I2))
        log.debug("_jones_sub_solve: iter %03d change: %.1e", it, change)

        if change < tol:
            break

    if change > tol:
        log.warning(
            "_jones_sub_solve: "
            + "gain solutions failed to converge for channel %d",
            ch,
        )
    else:
        log.debug(
            "_jones_sub_solve: "
            + "gain solutions converged in %d iterations for channel %d",
            it + 1,
            ch,
        )


def _jones_substitution(vobs, vmdl, wgt, nants, ant1, ant2):
    """
    Generate Jones matrix updates for the current solver iteration.
    Each update is a 2x2 matrix that multiplies on the LHS.

    Time and frequency splitting are done at a higher level, and here
    accumulation occurs in both of these dimensions.

    :param vobs: observed vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param vmdl: model vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param wgt: vis weights ndarray [ntime, nbaseline, nfreq]
    :param nants: number of antennas in the gain table
    :param ant1: ndarray containing the first antenna in each baseline
    :param ant2: ndarray containing the second antenna in each baseline
    :return: numpy array containing matrix updates [nant, nrec, nrec]
    """
    Som = numpy.zeros((nants, 2, 2), "complex")
    Smm = numpy.zeros((nants, 2, 2), "complex")

    for ant in range(nants):
        # accumulate Som and Smm products over all baselines containing
        # antenna ant and all available times and frequencies
        gen_coherency_products(
            Som[ant], Smm[ant], vobs, vmdl, wgt, ant, ant1, ant2
        )

    update = numpy.zeros((nants, 2, 2), "complex")
    for ant in range(nants):
        if numpy.linalg.matrix_rank(Smm[ant]) != 2:
            # set a flag or zero a weight
            continue
        update[ant] = Som[ant] @ numpy.linalg.inv(Smm[ant])

    return update


def _normal_equation_solve(
    vobs,
    vmdl,
    wgt,
    ant1,
    ant2,
    gain,
    ch,
    niter,
    tol,
):
    """
    Solve this time and frequency slice of the gain table

    Within each iteration the system of antenna-based gain and leakage terms
    is linearised into a least-squares design matrix and solved. It is based
    on the Yandasoft solver. Multiple passes over the data are required for
    each time and frequency solution interval, so this solver is most
    appropriate when the solution intervals are short, such as during
    real-time calibration.

    See the CalIm presentation by Maxim Voronkov for more details:
    https://indico.skatelescope.org/event/171/contributions/1007/

    :param vobs: observed vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param vmdl: model vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param wgt: vis weights ndarray [ntime, nbaseline, nfreq]
    :param ant1: ndarray containing the first antenna in each baseline
    :param ant2: ndarray containing the second antenna in each baseline
    :param gain: gain ndarray to be updated [ntime, nbaseline, nfreq, 2, 2]
    :param ch: gain table channel to be updated
    :param niter: Maximum number of iterations
    :param tol: Iteration stops when the fractional change
        in the gain solution is below this tolerance
    """
    nants = gain.shape[0]

    gainNew = numpy.zeros((nants, 2, 2), "complex")

    for it in range(niter):
        # reset the model vis, free params each iteration to relinearise
        gX = numpy.ones(nants, "complex")
        gY = numpy.ones(nants, "complex")
        dXY = numpy.zeros(nants, "complex")
        dYX = numpy.zeros(nants, "complex")

        param_update = _calc_and_solve_normal_equations(
            gX, gY, dXY, dYX, vobs, vmdl, wgt, ant1, ant2
        )

        # save some param info for convergence checks
        paramabs = numpy.abs(numpy.array([gX, gY, dXY, dYX]))
        diffabs = numpy.abs(numpy.array(param_update))

        nu = 1.0
        gX += nu * param_update[0]
        gY += nu * param_update[1]
        dXY += nu * param_update[2]
        dYX += nu * param_update[3]

        # update calibration Jones matrices
        gainNew[:, 0, 0] = gX
        gainNew[:, 0, 1] = gX * dXY
        gainNew[:, 1, 0] = -gY * dYX
        gainNew[:, 1, 1] = gY

        # update model vis (relinearise normal equations)
        vmdl = numpy.einsum(
            "bpi,tbfij,bqj->tbfpq", gainNew[ant1], vmdl, gainNew[ant2].conj()
        )

        # update cumulative Jones matrices
        gain[:, ch] = numpy.einsum("...pi,...iq->...pq", gainNew, gain[:, ch])

        pmask = paramabs > 0.0
        change = numpy.max(diffabs[pmask] / paramabs[pmask])
        log.debug("_normal_equation_solve: iter %03d change: %.1e", it, change)

        if change < tol:
            break

    if change > tol:
        log.warning(
            "_normal_equation_solve: "
            + "gain solutions failed to converge for channel %d",
            ch,
        )
    else:
        log.debug(
            "_normal_equation_solve: "
            + "gain solutions converged in %d iterations for channel %d",
            it + 1,
            ch,
        )


def _calc_and_solve_normal_equations(
    gX,
    gY,
    dXY,
    dYX,
    vobs,
    vmdl,
    wgt,
    ant1,
    ant2,
):
    """
    Calculate and solve normal equations for linearised gain and leakage
    terms for the current solver iteration.

    Time and frequency splitting are done at a higher level, and here
    accumulation occurs in both of these dimensions.

    :param gX,gY,dXY,dYX: 2D numpy ndarrays containing the initial complex
        gain and leakage estimates [nant, nchan]
    :param vobs: observed vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param vmdl: model vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param wgt: vis weights ndarray [ntime, nbaseline, nfreq]
    :param ant1: ndarray containing the first antenna in each baseline
    :param ant2: ndarray containing the second antenna in each baseline
    :return [gX,gY,dXY,dYX]: 2D ndarrays containing the complex gain and
        leakage updates [nant, nchan]
    """
    nants = len(gX)
    ntime, nbl, nchan_vis, _, _ = vobs.shape

    gX_update = numpy.zeros(nants, "complex")
    gY_update = numpy.zeros(nants, "complex")
    dXY_update = numpy.zeros(nants, "complex")
    dYX_update = numpy.zeros(nants, "complex")

    # for performance reasons, average all times and frequencies into these
    # matrices (rather than stacking different times and frequencies in
    # separate rows). For larger averages with variability in time or
    # frequency, use _calc_and_solve_normal_equations_with_presumming
    A = numpy.zeros((8 * nbl, 8 * nants))
    dv = numpy.zeros(8 * nbl)

    for f in range(nchan_vis):
        for t in range(ntime):
            # If the model is changing over the averaging time or frequency,
            # these arrays should either have extra rows for time and frequency
            # or be averaged into the AA and Av arrays within the time and
            # frequency loops
            # A = numpy.zeros([8 * nbl, 8 * nants])
            # dv = numpy.zeros([8 * nbl, 1])

            # numpy.einsum("tbf,tbfpq->tbfpq", wgt[t, :, f], vmdl[t, :, f])

            update_design_matrix(
                A,
                dv,
                numpy.einsum("b,bpq->bpq", wgt[t, :, f], vobs[t, :, f]),
                numpy.einsum("b,bpq->bpq", wgt[t, :, f], vmdl[t, :, f]),
                gX,
                gY,
                dXY,
                dYX,
                ant1,
                ant2,
            )

    # solve the normal equations
    gfit = lsmr(csc_matrix(A, dtype=float), dv)[0]

    gX_update = (
        gfit[0 * nants : 2 * nants - 1 : 2]
        + 1j * gfit[0 * nants + 1 : 2 * nants : 2]
    )
    gY_update = (
        gfit[2 * nants : 4 * nants - 1 : 2]
        + 1j * gfit[2 * nants + 1 : 4 * nants : 2]
    )
    dXY_update = (
        gfit[4 * nants : 6 * nants - 1 : 2]
        + 1j * gfit[4 * nants + 1 : 6 * nants : 2]
    )
    dYX_update = (
        gfit[6 * nants : 8 * nants - 1 : 2]
        + 1j * gfit[6 * nants + 1 : 8 * nants : 2]
    )

    return [gX_update, gY_update, dXY_update, dYX_update]


def _normal_equation_solve_with_presumming(
    vobs,
    vmdl,
    wgt,
    ant1,
    ant2,
    gain,
    ch,
    niter,
    tol,
):
    """
    Solve this time and frequency slice of the gain table

    Within each iteration the system of antenna-based gain and leakage terms
    is linearised into least-squares Normal equations and solved. It is based
    on the Yandasoft solver. Pre-summation of visibility products is carried
    out within each time and frequency solution interval to avoid multiple
    passes over the data. This improves performance for large solution
    intervals.

    See the CalIm presentation by Maxim Voronkov for more details:
    https://indico.skatelescope.org/event/171/contributions/1007/

    :param vobs: observed vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param vmdl: model vis ndarray [ntime, nbaseline, nfreq, 2, 2]
    :param wgt: vis weights ndarray [ntime, nbaseline, nfreq]
    :param ant1: ndarray containing the first antenna in each baseline
    :param ant2: ndarray containing the second antenna in each baseline
    :param gain: gain ndarray to be updated [ntime, nbaseline, nfreq, 2, 2]
    :param ch: gain table channel to be updated
    :param niter: Maximum number of iterations
    :param tol: Iteration stops when the fractional change
        in the gain solution is below this tolerance
    """
    nants = gain.shape[0]
    ntime, nbl, nchan_vis, nrec1, nrec2 = vobs.shape
    assert nrec1 * nrec2 == 4

    gainNew = numpy.zeros((nants, 2, 2), "complex")

    # accumulation pre-sum matrix products
    vobs = vobs.reshape(ntime, nbl, nchan_vis, 4)
    vmdl = vmdl.reshape(ntime, nbl, nchan_vis, 4)
    Smo = numpy.einsum("tbf,tbfp,tbfq->bpq", wgt, vmdl.conj(), vobs)
    Smm = numpy.einsum("tbf,tbfp,tbfq->bpq", wgt, vmdl.conj(), vmdl)

    gX = numpy.ones(nants, "complex")
    gY = numpy.ones(nants, "complex")
    dXY = numpy.zeros(nants, "complex")
    dYX = numpy.zeros(nants, "complex")

    for it in range(niter):
        param_update = _calc_and_solve_normal_equations_with_presumming(
            gX, gY, dXY, dYX, Smo, Smm, ant1, ant2
        )

        # save some param info for convergence checks
        paramabs = numpy.abs(numpy.array([gX, gY, dXY, dYX]))
        diffabs = numpy.abs(numpy.array(param_update))

        nu = 1.0
        gX += nu * param_update[0]
        gY += nu * param_update[1]
        dXY += nu * param_update[2]
        dYX += nu * param_update[3]

        # update calibration Jones matrices
        gainNew[:, 0, 0] = gX
        gainNew[:, 0, 1] = gX * dXY
        gainNew[:, 1, 0] = -gY * dYX
        gainNew[:, 1, 1] = gY

        pmask = paramabs > 0.0
        change = numpy.max(diffabs[pmask] / paramabs[pmask])
        log.debug(
            "_normal_equation_solve_with_presumming: "
            + "iter %03d change: %.1e",
            it,
            change,
        )

        if change < tol:
            break

    # update cumulative Jones matrices
    gain[:, ch] = numpy.einsum("...pi,...iq->...pq", gainNew, gain[:, ch])

    if change > tol:
        log.warning(
            "_normal_equation_solve_with_presumming: "
            + "gain solutions failed to converge for channel %d",
            ch,
        )
    else:
        log.debug(
            "_normal_equation_solve_with_presumming: "
            + "gain solutions converged in %d iterations for channel %d",
            it + 1,
            ch,
        )


def _calc_and_solve_normal_equations_with_presumming(
    gX,
    gY,
    dXY,
    dYX,
    Smo,
    Smm,
    ant1,
    ant2,
):
    """
    Calculate and solve normal equations for linearised gain and leakage
    terms for the current solver iteration.

    Time and frequency splitting are done at a higher level, and here
    accumulation occurs in both of these dimensions.

    :param gX,gY,dXY,dYX: 2D numpy ndarrays containing the initial complex
        gain and leakage estimates [nant, nchan]
    :param Smo: weighted pre-sums of polarised vis products,
        model * observed [nbaseline, 4, 4]
    :param Smm: weighted pre-sums of polarised vis products,
        model * model [nbaseline, 4, 4]
    :param ant1: ndarray containing the first antenna in each baseline
    :param ant2: ndarray containing the second antenna in each baseline
    :return [gX,gY,dXY,dYX]: 2D ndarrays containing the complex gain and
        leakage updates [nant, nchan]
    """
    nants = gX.shape[0]
    nbl = Smo.shape[0]

    gX_update = numpy.zeros(nants, "complex")
    gY_update = numpy.zeros(nants, "complex")
    dXY_update = numpy.zeros(nants, "complex")
    dYX_update = numpy.zeros(nants, "complex")

    # accumulation space for normal eqautions products
    #  - all stations x 4 pol x real & imag
    AA = numpy.zeros([8 * nants, 8 * nants])
    Av = numpy.zeros([8 * nants, 1])

    # Note that some of the following loops have been replaced with more
    # efficient numpy.einsum calls, but more can be done

    for k in range(nbl):
        if ant1[k] == ant2[k]:
            continue

        i = ant1[k]
        j = ant2[k]

        # parameter indices (i.e. row and column indices)
        iXX = 2 * i
        iYY = 2 * i + 2 * nants
        iXY = 2 * i + 4 * nants
        iYX = 2 * i + 6 * nants

        jXX = 2 * j
        jYY = 2 * j + 2 * nants
        jXY = 2 * j + 4 * nants
        jYX = 2 * j + 6 * nants

        # Generate a 4x4 Complex Diff matrix for each relevant gain
        # and leakage free parameter of baseline i-j.
        params = gen_cdm(
            gX[i], gY[i], dXY[i], dYX[i], gX[j], gY[j], dXY[j], dYX[j]
        )
        pRe = params[0:16:2]
        pIm = params[1:16:2]
        # Generate the 4x4 gain matrix for baseline i-j.
        values = gen_pol_matrix(
            gX[i], gY[i], dXY[i], dYX[i], gX[j], gY[j], dXY[j], dYX[j]
        )

        # move the following code to solver_utils

        pos = numpy.array([iXX, jXX, iYY, jYY, iXY, jXY, iYX, jYX])

        for param1 in range(8):
            pos1 = pos[param1]
            v_re = 0
            v_im = 0
            for p in range(0, 4):
                v_re += numpy.real(
                    numpy.einsum(
                        "p,p",
                        numpy.conj(pRe[param1][p, :]),
                        (
                            Smo[k][:, p]
                            - numpy.einsum("pq,q->p", Smm[k], values[p, :])
                        ),
                    )
                )
                v_im += numpy.real(
                    numpy.einsum(
                        "p,p",
                        numpy.conj(pIm[param1][p, :]),
                        (
                            Smo[k][:, p]
                            - numpy.einsum("pq,q->p", Smm[k], values[p, :])
                        ),
                    )
                )

            Av[pos[param1]] += v_re
            Av[pos[param1] + 1] += v_im
            AA[pos1, pos] += numpy.real(
                numpy.einsum("ip,xiq,pq->x", pRe[param1].conj(), pRe, Smm[k])
            )
            AA[pos1, pos + 1] += numpy.real(
                numpy.einsum("ip,xiq,pq->x", pRe[param1].conj(), pIm, Smm[k])
            )
            AA[pos1 + 1, pos] += numpy.real(
                numpy.einsum("ip,xiq,pq->x", pIm[param1].conj(), pRe, Smm[k])
            )
            AA[pos1 + 1, pos + 1] += numpy.real(
                numpy.einsum("ip,xiq,pq->x", pIm[param1].conj(), pIm, Smm[k])
            )

    gfit = lsmr(csc_matrix(AA, dtype=float), Av)[0]

    gX_update = (
        gfit[0 * nants : 2 * nants - 1 : 2]
        + 1j * gfit[0 * nants + 1 : 2 * nants : 2]
    )
    gY_update = (
        gfit[2 * nants : 4 * nants - 1 : 2]
        + 1j * gfit[2 * nants + 1 : 4 * nants : 2]
    )
    dXY_update = (
        gfit[4 * nants : 6 * nants - 1 : 2]
        + 1j * gfit[4 * nants + 1 : 6 * nants : 2]
    )
    dYX_update = (
        gfit[6 * nants : 8 * nants - 1 : 2]
        + 1j * gfit[6 * nants + 1 : 8 * nants : 2]
    )

    return [gX_update, gY_update, dXY_update, dYX_update]
