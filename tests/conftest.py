"""
Pytest fixtures
"""

import numpy
import pytest
from astropy import units
from astropy.convolution import convolve_fft
from astropy.convolution.kernels import Gaussian2DKernel
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.configuration import create_named_configuration
from ska_sdp_datamodels.image import create_image
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.sky_model import SkyComponent
from ska_sdp_datamodels.visibility import create_visibility

from ska_sdp_func_python.sky_component.operations import insert_skycomponent

N_CHAN = 6


@pytest.fixture(scope="package", name="phase_centre")
def phase_centre_fixture():
    """Phase Centre fixture"""
    phase_centre = SkyCoord(
        ra=+180.0 * units.deg,
        dec=-35.0 * units.deg,
        frame="icrs",
        equinox="J2000",
    )
    return phase_centre


@pytest.fixture(scope="package", name="comp_direction")
def comp_direction_fixture():
    """
    Component Absolute direction fixture
    """
    # The phase centre (given by phase_centre fixture)
    # is absolute and the component
    # is specified relative (for now).
    # This means that the component should end up at
    # the position phase_centre+comp_redirection
    comp_abs_direction = SkyCoord(
        ra=+181.0 * units.deg,
        dec=-35.0 * units.deg,
        frame="icrs",
        equinox="J2000",
    )
    return comp_abs_direction


@pytest.fixture(scope="package", name="comp_dft")
def component_dft_fixture(phase_centre, comp_direction):
    """
    SkyComponent list fixture to be used with DFT tests
    """
    n_comp = 20
    phase_centre_offset = phase_centre.skyoffset_frame()
    comp_rel_direction = comp_direction.transform_to(phase_centre_offset)

    flux = numpy.array(N_CHAN * [100.0, 20.0, -10.0, 1.0]).reshape([N_CHAN, 4])
    frequency = numpy.linspace(1.0e8, 1.1e8, N_CHAN)

    comp = n_comp * [
        SkyComponent(
            direction=comp_rel_direction,
            frequency=frequency,
            flux=flux,
        )
    ]
    return comp


@pytest.fixture(scope="package", name="visibility")
def vis_fixture(phase_centre, request):
    """
    Visibility fixture
    """
    n_times = 2
    low_core = create_named_configuration("LOW")
    times = (numpy.pi / 43200.0) * numpy.linspace(0.0, 300.0, n_times)
    frequency = numpy.linspace(1.0e8, 1.1e8, N_CHAN)
    channel_bandwidth = numpy.array([frequency[1] - frequency[0]] * N_CHAN)

    try:
        polarisation = request.param
    except AttributeError:
        # when no param is supplied
        polarisation = "linear"

    vis = create_visibility(
        low_core,
        times,
        frequency,
        phasecentre=phase_centre,
        channel_bandwidth=channel_bandwidth,
        weight=1.0,
        polarisation_frame=PolarisationFrame(polarisation),
    )
    return vis


@pytest.fixture(scope="package", name="image")
def image_fixt(visibility, comp_direction):
    """
    Image fixture
    """
    n_pixels = 512
    cell_size = 0.00015
    im = create_image(
        n_pixels,
        cell_size,
        comp_direction,
        # pylint: disable=protected-access
        polarisation_frame=PolarisationFrame(visibility._polarisation_frame),
        frequency=visibility.frequency.data[0],
        channel_bandwidth=visibility.channel_bandwidth.data[0],
        nchan=visibility.visibility_acc.nchan,
    )
    return im


# ----------------------- Deconv ------------------------
# TODO: review
# these are needed for deconvolution test; the visibility
# is also needed for skymodel tests; revise and see if
# these can be replaced with above vis; if not, move to
# their directories into a lower level conftest.py
@pytest.fixture(scope="package", name="phase_centre_30")
def phase_centre_fixture_2():
    """
    Phase Centre fixture for deconvolution test
    """
    phase_centre = SkyCoord(
        ra=+30.0 * units.deg,
        dec=-60.0 * units.deg,
        frame="icrs",
        equinox="J2000",
    )
    return phase_centre


@pytest.fixture(scope="package", name="comp_direction_30")
def com_direction_fixture_2():
    """
    Component direction fixture for deconvolution test
    """
    comp_abs_direction = SkyCoord(
        ra=+30.0 * units.deg,
        dec=-61.0 * units.deg,
        frame="icrs",
        equinox="J2000",
    )
    return comp_abs_direction


@pytest.fixture(scope="package", name="visibility_deconv")
def vis_deconv_fixture(phase_centre_30):
    """
    Visibility fixture for deconvolution tests
    """
    ntimes = 3

    # Choose the interval so that the maximum change in w is smallish
    integration_time = numpy.pi * (24 / (12 * 60))
    times = numpy.linspace(
        -integration_time * (ntimes // 2),
        integration_time * (ntimes // 2),
        ntimes,
    )

    frequency = numpy.array([1.0e8])
    channelwidth = numpy.array([4e7])

    low = create_named_configuration("LOW", rmax=300.0)
    vis = create_visibility(
        low,
        times,
        frequency,
        phase_centre_30,
        channel_bandwidth=channelwidth,
        weight=1.0,
        polarisation_frame=PolarisationFrame("stokesI"),
        zerow=True,
        times_are_ha=True,
    )
    return vis


@pytest.fixture(scope="package", name="image_with_component")
def image_fixt_with_component():
    """Fixture for the image with additional
    SkyComponent inserted.
    This is also used tests in in test_imaging.py
    """
    npixel = 256
    frequency = numpy.array([1e8])
    channelwidth = numpy.array([1e6])
    f = numpy.array([100.0])
    flux = numpy.array(
        [f * numpy.power(freq / 1e8, -0.7) for freq in frequency]
    )
    phase_centre = SkyCoord(
        ra=+180.0 * units.deg,
        dec=-45.0 * units.deg,
        frame="icrs",
        equinox="J2000",
    )
    phase_centre_2nd = SkyCoord(
        ra=+180.2 * units.deg,
        dec=-45.2 * units.deg,
        frame="icrs",
        equinox="J2000",
    )

    model = create_image(
        npixel,
        0.0001,
        phase_centre,
        frequency=frequency[0],
        channel_bandwidth=channelwidth[0],
        nchan=len(frequency),
    )
    model["pixels"].data = numpy.zeros(
        shape=model["pixels"].data.shape, dtype=float
    )

    components = SkyComponent(
        phase_centre,
        frequency,
        name="imaging_sc",
        flux=flux,
        polarisation_frame=PolarisationFrame("stokesI"),
    )

    components1 = SkyComponent(
        phase_centre_2nd,
        frequency,
        name="imaging_sc1",
        flux=flux,
        polarisation_frame=PolarisationFrame("stokesI"),
    )

    model = insert_skycomponent(model, [components, components1])

    width = 1.0
    kernel = Gaussian2DKernel(width)
    kernel.normalize(mode="peak")
    model["pixels"].data[0, 0] = convolve_fft(
        model["pixels"].data[0, 0, :, :],
        kernel,
        normalize_kernel=False,
        allow_huge=True,
        boundary="wrap",
    ).astype("float")

    params = {
        "components": [components, components1],
        "image": model,
    }
    return params
