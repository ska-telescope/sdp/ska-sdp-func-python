"""
Test the xradio-compatible inversion code.
"""

import math

import numpy as np
import pytest
import xarray

from ska_sdp_func_python.xradio.imaging import (
    DuccInvert,
    WtowersInvert,
    generate_psf,
    invert,
)
from ska_sdp_func_python.xradio.imaging.image_params import ImageParams
from ska_sdp_func_python.xradio.imaging.invert_backend import InvertBackend

PIXEL_SIZE_ASEC = 11.0
NUM_PIXELS_L = 1024
NUM_PIXELS_M = 1024

PIXEL_SIZE_RADIANS = math.radians(PIXEL_SIZE_ASEC / 3600.0)

BACKENDS = [DuccInvert(), WtowersInvert()]
BACKEND_IDS = ["ducc", "w-towers"]


@pytest.fixture(
    name="image_cube",
    params=BACKENDS,
    ids=BACKEND_IDS,
    scope="module",
)
def fixture_image_cube(
    mkt_ecdfs25_nano_chunked: xarray.Dataset, request
) -> xarray.Dataset:
    """
    Image cube of the chunked test dataset for every backend.
    """
    backend = request.param
    return invert(
        mkt_ecdfs25_nano_chunked,
        backend,
        num_pixels_l=NUM_PIXELS_L,
        num_pixels_m=NUM_PIXELS_M,
        pixel_size_radians=PIXEL_SIZE_RADIANS,
    ).compute()


@pytest.mark.parametrize(
    "backend",
    BACKENDS,
    ids=BACKEND_IDS,
)
def test_generate_psf(
    mkt_ecdfs25_nano: xarray.Dataset, backend: InvertBackend
):
    """
    Test that the generated PSF peak at the reference pixel with a value
    numerically close to 1.
    """
    psf = generate_psf(
        mkt_ecdfs25_nano,
        backend,
        num_pixels_l=NUM_PIXELS_L,
        num_pixels_m=NUM_PIXELS_M,
        pixel_size_radians=PIXEL_SIZE_RADIANS,
    )
    psf = psf.compute()

    # PSF must have a value of 1 at the reference pixel (l = m = 0) in
    # every pol channel.
    # This selects the reference pixel in every polarization channel,
    # so we get an array with `npol` elements.
    reference_pixel_values = psf.SKY.sel({"l": 0.0, "m": 0.0})
    assert np.allclose(reference_pixel_values, 1.0)

    # PSF must peak at the reference pixel in every pol channel
    max_pixel_values = psf.SKY.max(dim=["l", "m"])
    assert np.array_equal(reference_pixel_values, max_pixel_values)


@pytest.mark.parametrize(
    "backend",
    BACKENDS,
    ids=BACKEND_IDS,
)
def test_invert_correct_on_offcentre_point_source_visibilities(
    mkt_ecdfs25_nano: xarray.Dataset, backend: InvertBackend
):
    """
    First-principles test where we generate the exact visibilities associated
    to a point source at arbitrary coordinates (l0, m0). This test allows us
    to check multiple things in one go:
    - That the `l` and `m` axes are correctly labeled
    - That the `w` correction is applied with the correct sign
    """
    image_params = ImageParams(NUM_PIXELS_L, NUM_PIXELS_M, PIXEL_SIZE_RADIANS)

    # Choose the pixel indices in which we place the point source.
    # NOTE: When using values large enough to make the w-term significant,
    # this present test enables detecting whether the w-correction is performed
    # with the correct sign.
    l_shift_pixels = 203
    m_shift_pixels = 317
    l_shift = l_shift_pixels * image_params.pixel_size_lm
    m_shift = m_shift_pixels * image_params.pixel_size_lm
    n_shift = np.sqrt(1.0 - (l_shift**2 + m_shift**2))

    c = 299792458.0
    # NOTE: assuming frequency in Hz
    uvw_lambda = mkt_ecdfs25_nano.UVW * mkt_ecdfs25_nano.frequency / c
    u_lambda = uvw_lambda.sel({"uvw_label": "u"})
    v_lambda = uvw_lambda.sel({"uvw_label": "v"})
    w_lambda = uvw_lambda.sel({"uvw_label": "w"})

    # These are the visibilities associated with a point source at
    # coordinates (l_shift, m_shift), using the standard Fourier sign
    # convention fundamentally assumed in MSv2. It means that going from image
    # to visibilities corresponds a positive-signed Fourier transform.
    # NOTE: This is OPPOSITE to the TMS / Wikipedia convention which is a lot
    # more widespread / "standard"!
    point_source_vis = (1.0 / n_shift) * np.exp(
        +2.0j
        * np.pi
        * (u_lambda * l_shift + v_lambda * m_shift + w_lambda * (n_shift - 1))
    ).astype("complex64")

    with xarray.set_options(keep_attrs=True):
        new_vis_dataarray = (
            xarray.ones_like(mkt_ecdfs25_nano.VISIBILITY) * point_source_vis
        )
    xds_point_source = mkt_ecdfs25_nano.assign(
        {
            "VISIBILITY": new_vis_dataarray,
        }
    )

    image = invert(
        xds_point_source,
        backend,
        num_pixels_l=NUM_PIXELS_L,
        num_pixels_m=NUM_PIXELS_M,
        pixel_size_radians=PIXEL_SIZE_RADIANS,
    )
    image = image.compute()

    # Assert that the image peaks at the pixel closest to (l_shift, m_shift)
    pixel_value_at_expected_peak_coords = image.SKY.sel(
        {"l": l_shift, "m": m_shift}, method="nearest"
    )
    image_max_value = image.SKY.max(dim=["l", "m"])
    assert np.array_equal(pixel_value_at_expected_peak_coords, image_max_value)


def test_wtowers_and_ducc_dirty_image_are_close(
    mkt_ecdfs25_nano: xarray.Dataset,
):
    """
    Self-explanatory.
    """

    image_ducc = invert(
        mkt_ecdfs25_nano,
        DuccInvert(nthreads=1),
        num_pixels_l=NUM_PIXELS_L,
        num_pixels_m=NUM_PIXELS_M,
        pixel_size_radians=PIXEL_SIZE_RADIANS,
    ).compute()

    image_wtowers = invert(
        mkt_ecdfs25_nano,
        WtowersInvert(nthreads=1),
        num_pixels_l=NUM_PIXELS_L,
        num_pixels_m=NUM_PIXELS_M,
        pixel_size_radians=PIXEL_SIZE_RADIANS,
    ).compute()

    squared_difference = (image_wtowers - image_ducc).SKY ** 2

    # All these are DataArrays with one dimension: polarization
    rms_distance = (
        squared_difference.mean(dim=["time", "frequency", "l", "m"]) ** 0.5
    )
    peak_value = image_ducc.SKY.max()
    ratio = rms_distance / peak_value

    # NOTE: threshold has been manually adjusted for this test case, this is
    # not a universal value.
    assert ratio.values.max() < 2.0e-4


def test_baseline_id_chunking_is_not_allowed(
    mkt_ecdfs25_nano: xarray.Dataset,
):
    """
    Self-explanatory.
    """

    with pytest.raises(ValueError):
        invert(
            mkt_ecdfs25_nano.chunk({"baseline_id": 2}),
            DuccInvert(),
            num_pixels_l=NUM_PIXELS_L,
            num_pixels_m=NUM_PIXELS_M,
            pixel_size_radians=PIXEL_SIZE_RADIANS,
        )


def test_invert_result_insensitive_to_polarization_chunking(
    mkt_ecdfs25_nano: xarray.Dataset,
):
    """
    Test that result is identical whether the polarization axis is chunked
    or not.
    """

    def execute_invert(num_pol_chunks: int) -> xarray.Dataset:
        return invert(
            mkt_ecdfs25_nano.chunk({"polarization": num_pol_chunks}),
            # NOTE: use 1 thread for reproducibility, no guarantee that
            # 2+ threads produce the exact same result on different calls
            DuccInvert(nthreads=1),
            num_pixels_l=NUM_PIXELS_L,
            num_pixels_m=NUM_PIXELS_M,
            pixel_size_radians=PIXEL_SIZE_RADIANS,
        ).compute()

    result_1 = execute_invert(1)
    result_4 = execute_invert(4)
    assert xarray.Dataset.identical(result_1, result_4)


def test_image_cube_dims_correct(image_cube: xarray.Dataset):
    """
    Self-explanatory.
    """
    assert image_cube.sizes == {
        "time": 3,
        "frequency": 4,
        "polarization": 4,
        "l": NUM_PIXELS_L,
        "m": NUM_PIXELS_M,
    }


def test_image_cube_coordinates_correct(image_cube: xarray.Dataset):
    """
    Self-explanatory.
    """
    assert list(image_cube["polarization"]) == ["XX", "XY", "YX", "YY"]

    # Expected results were generated manually and printed to 15 decimal places
    expected_times = [
        1.5617815659831367e9,
        1.5617816779357738e9,
        1.5617817738951774e9,
    ]
    assert np.allclose(image_cube["time"], expected_times, rtol=1e-14, atol=0)

    expected_frequencies = [
        9.599697265625e8,
        9.608056640625e8,
        9.616416015625e8,
        9.624775390625e8,
    ]
    assert np.allclose(
        image_cube["frequency"], expected_frequencies, rtol=1e-14, atol=0
    )


def test_image_cube_attributes_correct(image_cube: xarray.Dataset):
    """
    Self-explanatory.
    """
    assert image_cube.attrs["direction"]["reference"] == {
        "type": "sky_coord",
        "frame": "fk5",
        "units": ["rad", "rad"],
        "value": [0.9084896905559134, -0.4814927073939356],
    }

    assert image_cube.time.attrs == {
        "format": "UNIX",
        "scale": "utc",
        "type": "time",
        "units": ["s"],
    }

    assert image_cube.frequency.attrs == {
        "frame": "TOPO",
        "type": "spectral_coord",
        "units": ["Hz"],
    }
