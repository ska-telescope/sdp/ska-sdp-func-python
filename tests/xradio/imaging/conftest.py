"""
Fixtures for imaging tests.
"""

import zipfile
from pathlib import Path

import pytest
import xarray
from xradio.vis import read_processing_set


@pytest.fixture(name="mkt_ecdfs25_nano", scope="module")
def fixture_mkt_ecdfs25_nano(
    tmp_path_factory: pytest.TempPathFactory,
) -> xarray.Dataset:
    """
    A very small MeerKAT dataset observed at L-Band.
    The dataset has 38 time samples, 4 freq channels and the 4 linear
    polarisation channels.
    """
    zipped_path = Path(__file__).parent / "data" / "mkt_ecdfs25_nano.zip"

    datasets_tmpdir = Path(tmp_path_factory.mktemp("xradio_datasets"))
    zipfile.ZipFile(zipped_path).extractall(datasets_tmpdir)
    unzipped_path = datasets_tmpdir / "mkt_ecdfs25_nano.xradio"

    ps = read_processing_set(str(unzipped_path.resolve()))
    return ps["mkt_ecdfs25_nano_0"]


@pytest.fixture(name="mkt_ecdfs25_nano_chunked", scope="module")
def fixture_mkt_ecdfs25_nano_chunked(
    mkt_ecdfs25_nano: xarray.Dataset,
) -> xarray.Dataset:
    """
    Same as "mkt_ecdfs25_nano", but chunked in time and frequency.
    Useful for testing dask-distributed functions.

    Chunks are as follows:
    - 3 time chunks of (14, 14, 10) time samples each
    - 4 frequency chunks of 1 frequency channel each

    The other dimensions are made of a single chunk.
    """
    return mkt_ecdfs25_nano.chunk({"time": 14, "frequency": 1})
