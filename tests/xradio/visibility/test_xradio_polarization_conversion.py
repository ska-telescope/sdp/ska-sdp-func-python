"""
Tests for convert_polarization function.
"""

import numpy as np
import pytest
import xarray
import xarray.testing
from numpy.typing import ArrayLike

from ska_sdp_func_python.xradio.visibility import convert_polarization


def create_xds(
    vis_vector: ArrayLike,
    weight_vector: ArrayLike,
    flag_vector: ArrayLike,
    polarization: list[str],
) -> xarray.Dataset:
    """
    Create a small xradio Dataset where the VISIBILITY, WEIGHT, and FLAG
    arrays are obtained by broadcasting the same visibility vector across
    all times, baselines and frequencies. Dummy attributes are also added
    to check they are preserved during conversion.
    """
    dims = ("time", "baseline_id", "frequency", "polarization")
    num_pols = len(polarization)
    shape = (5, 3, 2, num_pols)

    time = np.zeros(shape[0])
    baseline_id = np.arange(shape[1])
    frequency = np.linspace(1.0e9, 1.2e9, shape[2])

    vis = np.zeros(shape=shape, dtype="complex64")
    vis[..., :] = vis_vector

    weight = np.zeros(shape=shape, dtype="float32")
    weight[..., :] = weight_vector

    flag = np.zeros(shape=shape, dtype="bool")
    flag[..., :] = flag_vector

    result = xarray.Dataset(
        {
            "VISIBILITY": (
                dims,
                vis,
            ),
            "WEIGHT": (
                dims,
                weight,
            ),
            "FLAG": (
                dims,
                flag,
            ),
        },
        {
            "time": time,
            "baseline_id": baseline_id,
            "frequency": frequency,
            "polarization": polarization,
        },
        # Add dummy attributes
        attrs={
            "dummy_string": "wassup",
            "dummy_int": 42,
        },
    )
    # Also add attributes to one axis and one data variable
    result.time.attrs = {"type": "quantity", "units": ["s"]}
    result.VISIBILITY.attrs = {"type": "quanta", "units": ["unkown"]}
    return result


def assert_layout_and_attributes_identical(
    left: xarray.Dataset, right: xarray.Dataset
) -> None:
    """
    Assert that the attributes of two datasets are identical, including the
    attributes of their respective axes and data variables.
    """
    # Dataset attributes
    assert left.attrs == right.attrs

    # Name and attributes of coordinate axes
    assert set(left.coords.keys()) == set(right.coords.keys())
    for name in left.coords.keys():
        assert left.coords[name].attrs == right.coords[name].attrs

    # Name and attributes of data variables
    assert set(left.data_vars.keys()) == set(right.data_vars.keys())
    for name in left.data_vars.keys():
        assert left.data_vars[name].attrs == right.data_vars[name].attrs


# Test all 6 possible conversions + 1 case that reduces to just slicing.
# We have to define 6 cases because conversions of weights and flags is
# non-invertible (unlike conversion of visibilities).
# We always put in one zero weight to check their conversion is numerically
# stable.

# Conversions from linear
LINEAR_XDS = create_xds(
    vis_vector=[+1.0, +1.0j, -1.0, -1.0j],
    weight_vector=[2, 1, 1, 0],
    flag_vector=[False, False, False, True],
    polarization=["XX", "XY", "YX", "YY"],
)

EXPECTED_LINEAR_TO_DIAGONAL_LINEAR_XDS = create_xds(
    vis_vector=[+1.0, -1.0j],
    weight_vector=[2, 0],
    flag_vector=[False, True],
    polarization=["XX", "YY"],
)

EXPECTED_LINEAR_TO_STOKES_XDS = create_xds(
    vis_vector=[0.5 - 0.5j, 0.5 + 0.5j, -0.5 + 0.5j, 0.5 - 0.5j],
    weight_vector=[0, 0, 2, 2],
    flag_vector=[True, True, False, False],
    polarization=["I", "Q", "U", "V"],
)

# This allows us to check whether having fewer output pols and asking them
# in a non-standard order works as expected.
EXPECTED_LINEAR_TO_STOKES_QI_XDS = create_xds(
    vis_vector=[0.5 + 0.5j, 0.5 - 0.5j],
    weight_vector=[0, 0],
    flag_vector=[True, True],
    polarization=["Q", "I"],
)

EXPECTED_LINEAR_TO_CIRCULAR_XDS = create_xds(
    vis_vector=[1.0 - 1.0j, 0.0 + 0.0j, 1.0 + 1.0j, 0.0 + 0.0j],
    weight_vector=[0, 0, 0, 0],
    flag_vector=[True, True, True, True],
    polarization=["RR", "RL", "LR", "LL"],
)

# Conversions from stokes
STOKES_XDS = create_xds(
    vis_vector=[0.5 - 0.5j, 0.5 + 0.5j, -0.5 + 0.5j, 0.5 - 0.5j],
    weight_vector=[0, 2, 1, 1],
    flag_vector=[True, False, False, False],
    polarization=["I", "Q", "U", "V"],
)

EXPECTED_STOKES_TO_LINEAR_XDS = create_xds(
    vis_vector=[+1.0, +1.0j, -1.0, -1.0j],
    weight_vector=[0, 0.5, 0.5, 0],
    flag_vector=[True, False, False, True],
    polarization=["XX", "XY", "YX", "YY"],
)

EXPECTED_STOKES_TO_CIRCULAR_XDS = create_xds(
    vis_vector=[1.0 - 1.0j, 0.0 + 0.0j, 1.0 + 1.0j, 0.0 + 0.0j],
    weight_vector=[0, 2 / 3, 2 / 3, 0],
    flag_vector=[True, False, False, True],
    polarization=["RR", "RL", "LR", "LL"],
)

# Conversions from circular
CIRCULAR_XDS = create_xds(
    vis_vector=[+1.0, +1.0j, -1.0, -1.0j],
    weight_vector=[2, 1, 1, 0],
    flag_vector=[False, False, False, True],
    polarization=["RR", "RL", "LR", "LL"],
)

EXPECTED_CIRCULAR_TO_STOKES_XDS = create_xds(
    vis_vector=[0.5 - 0.5j, -0.5 + 0.5j, 0.5 - 0.5j, 0.5 + 0.5j],
    weight_vector=[0, 2, 2, 0],
    flag_vector=[True, False, False, True],
    polarization=["I", "Q", "U", "V"],
)

EXPECTED_CIRCULAR_TO_LINEAR_XDS = create_xds(
    vis_vector=[0.0 + 0.0j, 0.0 + 0.0j, 1.0 - 1.0j, 1.0 - 1.0j],
    weight_vector=[0, 0, 0, 0],
    flag_vector=[True, True, True, True],
    polarization=["XX", "XY", "YX", "YY"],
)

INPUTS_AND_EXPECTED_OUTPUTS = [
    (LINEAR_XDS, ["XX", "YY"], EXPECTED_LINEAR_TO_DIAGONAL_LINEAR_XDS),
    (LINEAR_XDS, ["I", "Q", "U", "V"], EXPECTED_LINEAR_TO_STOKES_XDS),
    (LINEAR_XDS, ["Q", "I"], EXPECTED_LINEAR_TO_STOKES_QI_XDS),
    (LINEAR_XDS, ["RR", "RL", "LR", "LL"], EXPECTED_LINEAR_TO_CIRCULAR_XDS),
    (STOKES_XDS, ["XX", "XY", "YX", "YY"], EXPECTED_STOKES_TO_LINEAR_XDS),
    (STOKES_XDS, ["RR", "RL", "LR", "LL"], EXPECTED_STOKES_TO_CIRCULAR_XDS),
    (CIRCULAR_XDS, ["I", "Q", "U", "V"], EXPECTED_CIRCULAR_TO_STOKES_XDS),
    (CIRCULAR_XDS, ["XX", "XY", "YX", "YY"], EXPECTED_CIRCULAR_TO_LINEAR_XDS),
]


def _conversion_test_id(xds_in: xarray.Dataset, output_pols: list[str]) -> str:
    input_pols = list(xds_in.polarization.values)
    return f"{input_pols} to {output_pols}"


@pytest.mark.parametrize(
    "xds_in, output_pols, expected_xds_out",
    INPUTS_AND_EXPECTED_OUTPUTS,
    ids=[
        _conversion_test_id(x, pols)
        for x, pols, __ in INPUTS_AND_EXPECTED_OUTPUTS
    ],
)
def test_polarization_conversion(
    xds_in: xarray.Dataset,
    output_pols: list[str],
    expected_xds_out: xarray.Dataset,
):
    """
    Test numerical correctness of pol conversion for visibilities,
    weights and flags.
    """
    xds_out = convert_polarization(xds_in, output_pols)
    assert_layout_and_attributes_identical(xds_out, expected_xds_out)
    xarray.testing.assert_allclose(
        xds_out, expected_xds_out, check_dim_order=True
    )


def test_visibilities_preserved_under_roundtrip_conversion():
    """
    Test that visibilities are preserved when converting:
    linear -> circular -> stokes -> linear
    linear -> stokes -> circular -> linear

    NOTE: weights and flags are NOT preserved under these conversion chains.
    """
    linear = ["XX", "XY", "YX", "YY"]
    # Also test robustness of code to arbitrary re-ordering of pols
    circular = ["RL", "LR", "LL", "RR"]
    stokes = ["V", "U", "Q", "I"]

    def _chain_convert(xds, *polframes):
        result = xds
        for pols in polframes:
            result = convert_polarization(result, pols)
        return result

    roundtrip1 = _chain_convert(LINEAR_XDS, circular, stokes, linear)
    assert_layout_and_attributes_identical(roundtrip1, LINEAR_XDS)
    xarray.testing.assert_allclose(
        roundtrip1.VISIBILITY,
        LINEAR_XDS.VISIBILITY,
        check_dim_order=True,
    )

    roundtrip2 = _chain_convert(LINEAR_XDS, stokes, circular, linear)
    assert_layout_and_attributes_identical(roundtrip2, LINEAR_XDS)
    xarray.testing.assert_allclose(
        roundtrip2.VISIBILITY,
        LINEAR_XDS.VISIBILITY,
        check_dim_order=True,
    )


INVALID_CONVERSIONS = [
    # Duplicate pols output forbidden
    (["XX", "YY"], ["I", "I"]),
    # Output pol code must exist
    (["XX", "YY"], ["LOL"]),
    # Mixed frames forbidden
    (["XX", "YY"], ["I", "XX"]),
    (["XX", "YY"], ["I", "RR"]),
    # Insufficient input pols to perform desired conversion
    (["XX", "YY"], ["XX", "XY", "YY"]),
    (["XX", "YY"], ["I", "V"]),
    (["RR", "LL"], ["I", "Q"]),
    (["I", "Q"], ["XX", "XY", "YX", "YY"]),
    (["I", "V"], ["RR", "RL", "LR", "LL"]),
]


@pytest.mark.parametrize(
    "input_pols, output_pols",
    INVALID_CONVERSIONS,
    ids=[f"{x} to {y}" for x, y in INVALID_CONVERSIONS],
)
def test_invalid_pol_conversions_are_rejected(
    input_pols: list[str], output_pols: list[str]
):
    """
    Self-explanatory.
    """
    npols_in = len(input_pols)
    input_xds = create_xds(
        vis_vector=np.zeros(npols_in),
        weight_vector=np.zeros(npols_in),
        flag_vector=np.zeros(npols_in),
        polarization=input_pols,
    )

    with pytest.raises(ValueError):
        convert_polarization(input_xds, output_pols)
