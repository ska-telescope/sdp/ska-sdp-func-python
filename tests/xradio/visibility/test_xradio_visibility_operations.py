"""
Unit tests for continuum subtraction
"""

import numpy as np
import xarray as xr

from ska_sdp_func_python.xradio.visibility.operations import (
    remove_continuum_visibility,
    subtract_visibility,
)


def generate_poly(x, coeff, with_noise=False):
    """
    Generate a polynomial for the unit-tests
    """
    polynomial = np.polyval(coeff, x)
    noise = np.random.randn(len(x))
    return polynomial + noise if with_noise else polynomial


def test_uvlin_basic_fit(result_msv4):
    """
    Given all visibilities are same, there should be a perfect
    fit, after subtraction the residual visibilites are all zero
    """
    nchan = result_msv4.frequency.shape[0]
    npol = result_msv4.polarization.shape[0]
    pulse = generate_poly(x=np.linspace(-1, 1, nchan), coeff=[1, 2]).reshape(
        nchan, npol
    )

    result_msv4["VISIBILITY"] = xr.DataArray(
        pulse * np.ones(result_msv4.VISIBILITY.shape, dtype=complex),
        dims=["time", "baseline_id", "frequency", "polarization"],
    )

    ps_cont_sub = remove_continuum_visibility(result_msv4)

    assert np.allclose(
        ps_cont_sub.VISIBILITY,
        np.zeros(ps_cont_sub.VISIBILITY.shape, dtype=complex),
    )


def test_uvlin_set_the_order_of_fit(result_msv4):
    """
    Should be able to fit with a higher order polynomial
    """
    nchan = result_msv4.frequency.shape[0]
    npol = result_msv4.polarization.shape[0]
    pulse = generate_poly(
        x=np.linspace(-1, 1, nchan), coeff=[1, 2, 3]
    ).reshape(nchan, npol)
    result_msv4["VISIBILITY"] = xr.DataArray(
        pulse * np.ones(result_msv4.VISIBILITY.shape, dtype=complex),
        dims=["time", "baseline_id", "frequency", "polarization"],
    )
    ps_cont_sub = remove_continuum_visibility(result_msv4, degree=2)

    assert np.allclose(
        ps_cont_sub.VISIBILITY,
        np.zeros(ps_cont_sub.VISIBILITY.shape, dtype=complex),
    )


def test_uvlin_consider_the_flag_column(result_msv4):
    """
    Given a msv4, should be able to consider the flag column
    """

    nchan = result_msv4.frequency.shape[0]
    npol = result_msv4.polarization.shape[0]
    pulse = generate_poly(x=np.linspace(-1, 1, nchan), coeff=[1, 2]).reshape(
        nchan, npol
    )

    data = pulse * np.ones(result_msv4.VISIBILITY.shape, dtype=complex)
    data = np.where(result_msv4.FLAG, 0.0, data)
    result_msv4["VISIBILITY"] = xr.DataArray(
        data,
        dims=["time", "baseline_id", "frequency", "polarization"],
    )
    ps_cont_sub = remove_continuum_visibility(result_msv4)

    assert np.allclose(
        np.where(ps_cont_sub.FLAG, 0.0, ps_cont_sub.VISIBILITY),
        np.zeros(ps_cont_sub.VISIBILITY.shape, dtype=complex),
    )


def test_uvlin_should_flag_the_outliers(result_msv4):
    """
    Given an order one polynomial, should be able to flag the outliers
    """
    nchan = result_msv4.frequency.shape[0]
    npol = result_msv4.polarization.shape[0]
    pulse = generate_poly(x=np.linspace(-1, 1, nchan), coeff=[1, 2]).reshape(
        nchan, npol
    )
    expected = np.zeros((nchan, npol), dtype=complex)
    expected[2] = 10000 - pulse[2]
    pulse[2] = 10000
    expected[6] = -10000 - pulse[6]
    pulse[6] = -10000
    pulse = pulse + 1j * pulse
    expected = expected + 1j * expected

    result_msv4["VISIBILITY"] = xr.DataArray(
        pulse * np.ones(result_msv4.VISIBILITY.shape, dtype=complex),
        dims=["time", "baseline_id", "frequency", "polarization"],
    )
    expected = xr.DataArray(
        expected * np.ones(result_msv4.VISIBILITY.shape, dtype=complex),
        dims=["time", "baseline_id", "frequency", "polarization"],
    )

    ps_cont_sub = remove_continuum_visibility(result_msv4, threshold=3)

    assert np.allclose(ps_cont_sub.VISIBILITY, expected)


def test_uvlin_all_channels_are_flagged(result_msv4):
    """
    Should not modify visibilities when all the channels in a row are flagged
    """
    nchan = result_msv4.frequency.shape[0]
    npol = result_msv4.polarization.shape[0]
    pulse = generate_poly(x=np.linspace(-1, 1, nchan), coeff=[1, 2]).reshape(
        nchan, npol
    )
    result_msv4["VISIBILITY"] = xr.DataArray(
        pulse * np.ones(result_msv4.VISIBILITY.shape, dtype=complex),
        dims=["time", "baseline_id", "frequency", "polarization"],
    )

    flag = np.zeros(result_msv4.FLAG.shape, dtype=bool)
    flag[2:4, 2:4, :, :] = True
    result_msv4["FLAG"] = xr.DataArray(
        flag,
        dims=["time", "baseline_id", "frequency", "polarization"],
    )

    ps_cont_sub = remove_continuum_visibility(result_msv4)
    expected = np.where(flag, result_msv4.VISIBILITY, 0)

    assert np.allclose(ps_cont_sub.VISIBILITY, expected)


def test_uvlin_exclude_line_free_channel(result_msv4):
    """
    Should be able to exclude the line free channels from the fit
    """
    nchan = result_msv4.frequency.shape[0]
    npol = result_msv4.polarization.shape[0]
    pulse = generate_poly(x=np.linspace(-1, 1, nchan), coeff=[1, 2]).reshape(
        nchan, npol
    )
    result_msv4["VISIBILITY"] = xr.DataArray(
        pulse * np.ones(result_msv4.VISIBILITY.shape, dtype=complex),
        dims=["time", "baseline_id", "frequency", "polarization"],
    )

    mask = slice(1, 3)

    # Adding noise to the visibility to check the filtering.
    visibility = np.copy(result_msv4["VISIBILITY"])
    visibility[:, :, mask, :] = 2
    expected = visibility - result_msv4["VISIBILITY"]
    result_msv4["VISIBILITY"] = xr.DataArray(
        visibility,
        dims=["time", "baseline_id", "frequency", "polarization"],
    )

    ps_cont_sub = remove_continuum_visibility(result_msv4, mask=mask)

    assert np.allclose(
        ps_cont_sub.VISIBILITY,
        expected,
    )


def test_remove_continuum_visibility_subtract_model(result_msv4):
    """
    Should be able to subtract the model column from the VISIBILITY column.
    """
    visibility_with_attr = result_msv4.VISIBILITY.assign_attrs(
        {"test": "value"}
    )
    model_visibility = xr.DataArray(
        (1 + 3j) * np.ones(result_msv4.VISIBILITY.shape, dtype=complex),
        dims=["time", "baseline_id", "frequency", "polarization"],
    )

    output_obs = subtract_visibility(
        result_msv4.assign({"VISIBILITY": visibility_with_attr}),
        result_msv4.assign({"VISIBILITY": model_visibility}),
    )

    assert np.all(output_obs.VISIBILITY == (0 - 1j))
    assert visibility_with_attr.attrs == output_obs.VISIBILITY.attrs
