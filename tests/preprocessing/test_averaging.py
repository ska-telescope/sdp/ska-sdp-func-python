"""
Unit tests for real-time averaging
"""

import numpy

from ska_sdp_func_python.preprocessing.averaging import (
    averaging_frequency,
    averaging_time,
)


def test_averaging_frequency_indivisible(result_visibility):
    """
    Test averaging in the frequency direction in the case that
    the number of channels is not divisible by the frequency step.

    Check the correctness of visibilities, flags,
    frequency and channel_bandwidth after averaging.
    """
    frequency_step = 3
    expected_num_times = 8
    expected_num_baselines = 21
    expected_num_freqs = 3
    expected_num_pols = 1

    vis = result_visibility.copy(deep=True)

    result_vis_avg = averaging_frequency(vis, frequency_step)
    vis_avg = result_vis_avg["vis"].values
    chan_freq_avg = result_vis_avg["frequency"].values
    channel_banwidth_avg = result_vis_avg["channel_bandwidth"].values
    weights_avg = result_vis_avg["weight"].values
    flags_avg = result_vis_avg["flags"].values.astype(bool)

    assert vis_avg.shape[0] == expected_num_times
    assert vis_avg.shape[1] == expected_num_baselines
    assert vis_avg.shape[2] == expected_num_freqs
    assert vis_avg.shape[3] == expected_num_pols

    expected_chan_freq = numpy.array([1.02e8, 1.05e8, 1.075e8])
    expected_channel_bandwidth = numpy.array([3.0e6, 3.0e6, 2.0e6])
    assert (expected_chan_freq == chan_freq_avg).all()
    assert (expected_channel_bandwidth == channel_banwidth_avg).all()

    expected_vis = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    ).astype(complex)
    expected_vis[:, :, 0, :] = 1 + 2j
    expected_vis[:, :, 1, :] = 1 + 2j
    expected_vis[:, :, 2, :] = 1 + 2j
    expected_vis[2:4, :, 1, :] = 1 + 2j
    expected_vis[2:4, :, 2, :] = 1 + 2j

    assert (expected_vis == vis_avg).all()

    expected_weights = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    )
    expected_weights[:, :, 0, :] = 1.5
    expected_weights[:, :, 1, :] = 1.5
    expected_weights[:, :, 2, :] = 1.0
    expected_weights[2:4, :, 1, :] = 0.5
    expected_weights[2:4, :, 2, :] = 0.5

    assert numpy.allclose(expected_weights, weights_avg)

    expected_flags = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    ).astype(bool)
    expected_flags[2:4, :, 1, :] = True
    expected_flags[2:4, :, 2, :] = True

    assert (expected_flags == flags_avg).all()


def test_averaging_time_indivisible(result_visibility):
    """
    Test averaging in the time direction

    Check the correctness of visibilities, weights, uvws,
    flags, time and integration_time after averaging
    """
    timestep = 3
    expected_num_times = 3
    expected_num_baselines = 21
    expected_num_freqs = 8
    expected_num_pols = 1

    vis = result_visibility.copy(deep=True)

    result_vis_avg = averaging_time(vis, timestep)
    vis_avg = result_vis_avg["vis"].values
    weights_avg = result_vis_avg["weight"].values
    flags_avg = result_vis_avg["flags"].values.astype(bool)
    uvw_avg = result_vis_avg["uvw"].values
    time_avg = result_vis_avg["time"].values
    integration_time_avg = result_vis_avg["integration_time"].values

    assert vis_avg.shape[0] == expected_num_times
    assert vis_avg.shape[1] == expected_num_baselines
    assert vis_avg.shape[2] == expected_num_freqs
    assert vis_avg.shape[3] == expected_num_pols

    expected_vis = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    ).astype(complex)
    expected_vis[0, :, :, :] = 1 + 2j
    expected_vis[1, :, :, :] = 1 + 2j
    expected_vis[2, :, :, :] = 1 + 2j
    expected_vis[0, :, 4:7, :] = 1 + 2j
    expected_vis[1, :, 4:7, :] = 1 + 2j

    assert (expected_vis == vis_avg).all()

    expected_weights = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    )
    expected_weights[0, :, :, :] = 1.5
    expected_weights[1, :, :, :] = 1.5
    expected_weights[2, :, :, :] = 1.0
    expected_weights[0, :, 4:7, :] = 1.0
    expected_weights[1, :, 4:7, :] = 1.0

    assert numpy.allclose(expected_weights, weights_avg)

    expected_flags = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    ).astype(bool)
    expected_flags[0, :, 4:7, :] = True
    expected_flags[1, :, 4:7, :] = True

    assert (expected_flags == flags_avg).all()

    expected_uvw = numpy.zeros(
        shape=(expected_num_times, expected_num_baselines, 3)
    )
    for t in range(expected_num_times):
        for b in range(expected_num_baselines):
            if t == 0:
                expected_uvw[t, b, 0] = 1.2 + b * 0.1
                expected_uvw[t, b, 1] = 1.3 + b * 0.1
                expected_uvw[t, b, 2] = 1.4 + b * 0.1
            if t == 1:
                expected_uvw[t, b, 0] = 1.5 + b * 0.1
                expected_uvw[t, b, 1] = 1.6 + b * 0.1
                expected_uvw[t, b, 2] = 1.7 + b * 0.1
            if t == 2:
                expected_uvw[t, b, 0] = 1.75 + b * 0.1
                expected_uvw[t, b, 1] = 1.85 + b * 0.1
                expected_uvw[t, b, 2] = 1.95 + b * 0.1

    assert numpy.allclose(expected_uvw, uvw_avg)

    expected_time = numpy.array([20, 50, 75])
    expected_integration_time = numpy.array([30, 30, 20])
    assert (expected_time == time_avg).all()
    assert (expected_integration_time == integration_time_avg).all()


def test_averaging_frequency_divisible(result_visibility):
    """
    Test averaging in the frequency direction in the case that
    the number of channels is not divisible by the frequency step.

    Check the correctness of visibilities, flags, frequency and
    channel_bandwidth after averaging
    """
    frequency_step = 4
    expected_num_times = 8
    expected_num_baselines = 21
    expected_num_freqs = 2
    expected_num_pols = 1

    vis = result_visibility.copy(deep=True)

    result_vis_avg = averaging_frequency(vis, frequency_step)
    vis_avg = result_vis_avg["vis"].values
    chan_freq_avg = result_vis_avg["frequency"].values
    channel_banwidth_avg = result_vis_avg["channel_bandwidth"].values
    weights_avg = result_vis_avg["weight"].values
    flags_avg = result_vis_avg["flags"].values.astype(bool)

    assert vis_avg.shape[0] == expected_num_times
    assert vis_avg.shape[1] == expected_num_baselines
    assert vis_avg.shape[2] == expected_num_freqs
    assert vis_avg.shape[3] == expected_num_pols

    expected_chan_freq = numpy.array([1.025e8, 1.065e8])
    expected_channel_bandwidth = numpy.array([4.0e6, 4.0e6])
    assert (expected_chan_freq == chan_freq_avg).all()
    assert (expected_channel_bandwidth == channel_banwidth_avg).all()

    expected_vis = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    ).astype(complex)
    expected_vis[:, :, 0, :] = 1 + 2j
    expected_vis[:, :, 1, :] = 1 + 2j

    expected_vis[2:4, :, 1, :] = 1 + 2j

    assert (expected_vis == vis_avg).all()

    expected_weights = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    )
    expected_weights[:, :, 0, :] = 2
    expected_weights[:, :, 1, :] = 2
    expected_weights[2:4, :, 1, :] = 0.5

    assert (numpy.abs(expected_weights - weights_avg) < 0.00001).all()

    expected_flags = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    ).astype(bool)
    expected_flags[2:4, :, 1, :] = True

    assert (expected_flags == flags_avg).all()


def test_averaging_time_divisible(result_visibility):
    """
    Test averaging in the time direction

    Check the correctness of visibilities, weights, uvws,
    flags, time and integration_time after averaging
    """
    timestep = 4
    expected_num_times = 2
    expected_num_baselines = 21
    expected_num_freqs = 8
    expected_num_pols = 1

    vis = result_visibility.copy(deep=True)

    result_vis_avg = averaging_time(vis, timestep)
    vis_avg = result_vis_avg["vis"].values
    weights_avg = result_vis_avg["weight"].values
    flags_avg = result_vis_avg["flags"].values.astype(bool)
    uvw_avg = result_vis_avg["uvw"].values
    time_avg = result_vis_avg["time"].values
    integration_time_avg = result_vis_avg["integration_time"].values

    assert vis_avg.shape[0] == expected_num_times
    assert vis_avg.shape[1] == expected_num_baselines
    assert vis_avg.shape[2] == expected_num_freqs
    assert vis_avg.shape[3] == expected_num_pols

    expected_vis = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    ).astype(complex)
    expected_vis[0, :, :, :] = 1 + 2j
    expected_vis[1, :, :, :] = 1 + 2j

    expected_vis[0, :, 4:7, :] = 1 + 2j

    assert (expected_vis == vis_avg).all()

    expected_weights = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    )
    expected_weights[0, :, :, :] = 2
    expected_weights[1, :, :, :] = 2
    expected_weights[0, :, 4:7, :] = 1

    for t in range(expected_num_times):
        for b in range(expected_num_baselines):
            for c in range(expected_num_freqs):
                for p in range(expected_num_pols):
                    if expected_weights[t, b, c, p] != weights_avg[t, b, c, p]:
                        print(t, "  ", b, "  ", c, "  ", p)
                        print(expected_weights[t, b, c, p])
                        print(weights_avg[t, b, c, p])

    assert (numpy.abs(expected_weights - weights_avg) < 0.00001).all()

    expected_flags = numpy.zeros(
        shape=(
            expected_num_times,
            expected_num_baselines,
            expected_num_freqs,
            expected_num_pols,
        )
    ).astype(bool)
    expected_flags[0, :, 4:7, :] = True

    assert (expected_flags == flags_avg).all()

    expected_uvw = numpy.zeros(
        shape=(expected_num_times, expected_num_baselines, 3)
    )
    for t in range(expected_num_times):
        for b in range(expected_num_baselines):
            if t == 0:
                expected_uvw[t, b, 0] = 1.25 + b * 0.1
                expected_uvw[t, b, 1] = 1.35 + b * 0.1
                expected_uvw[t, b, 2] = 1.45 + b * 0.1
            if t == 1:
                expected_uvw[t, b, 0] = 1.65 + b * 0.1
                expected_uvw[t, b, 1] = 1.75 + b * 0.1
                expected_uvw[t, b, 2] = 1.85 + b * 0.1

    assert numpy.allclose(expected_uvw, uvw_avg)

    expected_time = numpy.array([25, 65])
    expected_integration_time = numpy.array([40, 40])
    assert (expected_time == time_avg).all()
    assert (expected_integration_time == integration_time_avg).all()
