"""
Unit tests for rfi static masks
"""

import numpy

from ska_sdp_func_python.preprocessing.rfi_masks import apply_rfi_masks


def test_no_masks(result_visibility):
    """
    Tests if the function works if no mask is
    provided.
    """

    num_times = 8
    num_baselines = 21
    num_freqs = 8
    num_pols = 1

    vis = result_visibility.copy(deep=True)

    no_mask_visibility = apply_rfi_masks(vis)
    flags_no_mask = no_mask_visibility["flags"].values
    expected_flags = (
        numpy.zeros(shape=(num_times, num_baselines, num_freqs, num_pols))
        .astype(int)
        .astype(bool)
    )
    expected_flags[2:4, :, 4:7, :] = True

    assert (flags_no_mask == expected_flags).all()


def test_empty_masks(result_visibility):
    """
    Tests if the function works if the masks
    array is empty.
    """

    num_times = 8
    num_baselines = 21
    num_freqs = 8
    num_pols = 1

    expected_flags = (
        numpy.zeros(shape=(num_times, num_baselines, num_freqs, num_pols))
        .astype(int)
        .astype(bool)
    )

    vis = result_visibility.copy(deep=True)

    expected_flags[2:4, :, 4:7, :] = True
    empty_masked_visibility = apply_rfi_masks(vis, rfi_frequency_masks=[])
    flags_empty_mask = empty_masked_visibility["flags"].values

    assert (expected_flags == flags_empty_mask).all()


def test_apply_rfi_masks(result_visibility):
    """
    Tests if the RFI masks presented in the for of
    frequency ranges are correctly translated to
    channel indices and applied correctly to flags.
    """

    num_times = 8
    num_baselines = 21
    num_freqs = 8
    num_pols = 1

    vis = result_visibility.copy(deep=True)

    rfi_mask = numpy.array(
        [
            [1.04e8, 1.06e8],
            [0.98e8, 1.01e8],
            [0.84e8, 0.91e8],
            [1.08e8, 1.12e8],
            [1.19e8, 1.26e8],
        ]
    )

    expected_flags = (
        numpy.zeros(shape=(num_times, num_baselines, num_freqs, num_pols))
        .astype(int)
        .astype(bool)
    )

    masked_visibility = apply_rfi_masks(vis, rfi_mask)

    expected_flags[2:4, :, 4:7, :] = True  # flags from the input data
    expected_flags[:, :, 3:6, :] = True
    expected_flags[:, :, 0, :] = True
    expected_flags[:, :, 7, :] = True

    flags = masked_visibility["flags"].values

    assert (expected_flags == flags).all()
