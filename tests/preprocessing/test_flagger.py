"""
Unit tests for dynamic RFI flagging
"""

import numpy
import pytest

from ska_sdp_func_python.preprocessing.flagger import rfi_flagger

pytest.importorskip(
    modname="ska_sdp_func", reason="ska-sdp-func is an optional dependency"
)


def test_flagger(rfi_visibility):
    """
    Test FluctuFlagger RFI flagging algorithm.
    The test checks for correct flags.
    """

    vis = rfi_visibility.copy(deep=True)

    flagged_data = rfi_flagger(
        data=vis,
        alpha=0.5,
        threshold_magnitude=3.5,
        threshold_variation=3.5,
        threshold_broadband=3.5,
        sampling=2,
        window=2,
        window_median_history=10,
    )
    flags = flagged_data["flags"].values
    expected_flags = numpy.zeros(shape=flags.shape).astype(bool)

    expected_flags[9:19, :, 46:54, 0] = True
    expected_flags[17, 14, 6:11, 0] = True
    expected_flags[24, :, :, 0] = True

    assert (flags == expected_flags).all()
