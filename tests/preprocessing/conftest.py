"""
Fixtures for real-time functions
"""

import numpy
import pytest
from ska_sdp_datamodels.configuration import create_named_configuration
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility.vis_model import Visibility


@pytest.fixture(scope="package", name="result_visibility")
def fixture_visibility(phase_centre):
    """
    Generate a visibility object using Visibility.constructor
    """
    low_core = create_named_configuration("LOW")
    frequency = numpy.array(
        [1.01e8, 1.02e8, 1.03e8, 1.04e8, 1.05e8, 1.06e8, 1.07e8, 1.08e8]
    )
    channel_bandwidth = numpy.array(
        [1.0e6, 1.0e6, 1.0e6, 1.0e6, 1.0e6, 1.0e6, 1.0e6, 1.0e6]
    )
    num_times = 8
    num_baselines = 21
    num_pols = 1

    time = numpy.array([10.0, 20.0, 30.0, 40.0, 50.0, 60, 70, 80])
    vis = (1 + 2j) * numpy.ones(
        shape=(len(time), num_baselines, len(frequency), num_pols)
    ).astype(complex)
    weights = 0.5 * numpy.ones(
        shape=(len(time), num_baselines, len(frequency), num_pols)
    )
    flags = numpy.zeros(
        shape=(len(time), num_baselines, len(frequency), num_pols)
    ).astype(bool)
    flags[2:4, :, 4:7, :] = True

    uvw = numpy.zeros((num_times, num_baselines, 3))
    for t in range(num_times):
        for b in range(num_baselines):
            uvw[t, b, 0] = 1.1 + t * 0.1 + b * 0.1
            uvw[t, b, 1] = 1.2 + t * 0.1 + b * 0.1
            uvw[t, b, 2] = 1.3 + t * 0.1 + b * 0.1

    integration_time = numpy.array([10.0, 10.0, 10.0, 10.0, 10.0, 10, 10, 10])
    baselines = numpy.ones(21)
    polarisation_frame = PolarisationFrame("stokesI")
    source = "anonymous"
    low_precision = "float64"
    scan_id = 1
    scan_intent = "target"
    execblock_id = 1

    visibility = Visibility.constructor(
        frequency=frequency,
        channel_bandwidth=channel_bandwidth,
        phasecentre=phase_centre,
        configuration=low_core,
        uvw=uvw,
        time=time,
        vis=vis,
        weight=weights,
        integration_time=integration_time,
        flags=flags,
        baselines=baselines,
        polarisation_frame=polarisation_frame,
        source=source,
        scan_id=scan_id,
        scan_intent=scan_intent,
        execblock_id=execblock_id,
        meta=None,
        low_precision=low_precision,
    )

    return visibility


@pytest.fixture(scope="package", name="rfi_visibility")
def fixture_rfi_visibility(phase_centre):
    """
    Generate a visibility object using Visibility.constructor
    """
    low_core = create_named_configuration("LOW")

    frequency = numpy.linspace(1.0e7, 2.0e7, 100)
    channel_bandwidth = 1.0e05 * numpy.ones(100)

    num_times = 50
    num_baselines = 21
    num_freqs = len(frequency)
    num_pols = 1

    time = numpy.linspace(10, 500, 50)

    vis = (
        (1 + 2j) * numpy.ones((num_times, num_baselines, num_freqs, num_pols))
    ).astype(complex)
    vis[10, :, 48:52, 0] = 2000 + 2000j
    vis[17, 14, 8, 0] = 1.1 + 2j
    vis[24, :, :, 0] = 3000 + 3000j

    weights = 0.5 * numpy.ones(
        shape=(len(time), num_baselines, len(frequency), num_pols)
    )
    flags = numpy.zeros(
        shape=(len(time), num_baselines, len(frequency), num_pols), dtype=bool
    )

    uvw = numpy.zeros((num_times, num_baselines, 3))
    for t in range(num_times):
        for b in range(num_baselines):
            uvw[t, b, 0] = 1.1 + t * 0.1 + b * 0.1
            uvw[t, b, 1] = 1.2 + t * 0.1 + b * 0.1
            uvw[t, b, 2] = 1.3 + t * 0.1 + b * 0.1

    integration_time = 10 * numpy.ones(50)
    baselines = numpy.ones(21)
    polarisation_frame = PolarisationFrame("stokesI")
    source = "anonymous"
    low_precision = "float64"
    scan_id = 1
    scan_intent = "target"
    execblock_id = 1

    visibility = Visibility.constructor(
        frequency=frequency,
        channel_bandwidth=channel_bandwidth,
        phasecentre=phase_centre,
        configuration=low_core,
        uvw=uvw,
        time=time,
        vis=vis,
        weight=weights,
        integration_time=integration_time,
        flags=flags,
        baselines=baselines,
        polarisation_frame=polarisation_frame,
        source=source,
        scan_id=scan_id,
        scan_intent=scan_intent,
        execblock_id=execblock_id,
        meta=None,
        low_precision=low_precision,
    )

    return visibility
