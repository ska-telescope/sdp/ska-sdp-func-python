"""
Unit tests for imaging using GPU version of nifty gridder
"""

import os
import tempfile

import numpy
import pytest
from astropy import units as u
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.configuration.config_create import (
    create_named_configuration,
)
from ska_sdp_datamodels.image.image_create import create_image
from ska_sdp_datamodels.science_data_model.polarisation_model import (
    PolarisationFrame,
)
from ska_sdp_datamodels.visibility.vis_create import create_visibility

from ska_sdp_func_python.imaging.ng import invert_ng, predict_ng
from ska_sdp_func_python.imaging.wg import invert_wg, predict_wg

# Skip tests if required modules are not available.
pytest.importorskip("cupy", reason="cupy not available")
pytest.importorskip("ska_sdp_func", reason="ska_sdp_func not available")


@pytest.fixture(scope="module", name="result_wg")
def wg_fixture():
    """Fixture to generate inputs for tested functions"""
    persist = os.getenv("FUNC_PYTHON_PERSIST")
    verbosity = 0
    npixel = 256
    low = create_named_configuration("LOWBD2", rmax=750.0)
    ntimes = 5
    times = numpy.linspace(-3.0, +3.0, ntimes) * numpy.pi / 12.0
    frequency = numpy.array([1e8])
    channelwidth = numpy.array([1e6])

    vis_pol = PolarisationFrame("stokesI")

    phase_centre = SkyCoord(
        ra=+180.0 * u.deg, dec=-45.0 * u.deg, frame="icrs", equinox="J2000"
    )
    vis = create_visibility(
        low,
        times,
        frequency,
        phase_centre,
        channelwidth,
        polarisation_frame=vis_pol,
    )

    model = create_image(
        npixel,
        0.00015,
        phase_centre,
        nchan=1,
    )

    params = {
        "model": model,
        "persist": persist,
        "verbosity": verbosity,
        "visibility": vis,
    }
    return params


def test_predict_wg(result_wg):
    """Test predict_wg against predict_ng"""
    vis = result_wg["visibility"]
    model = result_wg["model"]
    persist = result_wg["persist"]
    i_pix = model["pixels"].data.shape
    i_pix = tuple(t_i // 4 for t_i in i_pix)
    model["pixels"].data[i_pix] = 1.0
    assert numpy.max(numpy.abs(model["pixels"].data)), "Image is empty"

    # Create reference data using predict_ng.
    vis_ref = predict_ng(vis, model)

    # Create data using predict_wg.
    vis = predict_wg(vis, model)

    # Check maximum of difference.
    assert numpy.max(numpy.abs(vis_ref["vis"].data - vis["vis"].data)) < 1e-5

    # Generate image of difference.
    vis["vis"].data = vis["vis"].data - vis_ref["vis"].data
    dirty = invert_wg(vis, model, dopsf=False, normalise=True)

    if persist:
        with tempfile.TemporaryDirectory() as tempdir:
            dirty[0].image_acc.export_to_fits(
                f"{tempdir}/test_imaging_ng_predict_wg_residual.fits"
            )


def test_invert_wg(result_wg):
    """Test invert_wg against invert_ng"""
    vis = result_wg["visibility"]
    vis["vis"].data = numpy.random.random_sample(vis["vis"].data.shape)
    model = result_wg["model"]
    persist = result_wg["persist"]

    # Create reference image using invert_ng.
    dirty_ref = invert_ng(vis, model, normalise=True)

    # Create image using invert_wg.
    dirty = invert_wg(vis, model, normalise=True)

    # Check maximum of difference.
    diff = dirty_ref[0]["pixels"].data - dirty[0]["pixels"].data
    assert numpy.max(numpy.abs(diff)) < 1e-5

    if persist:
        with tempfile.TemporaryDirectory() as tempdir:
            dirty[0].image_acc.export_to_fits(
                f"{tempdir}/test_imaging_ng_invert_wg_dirty.fits"
            )
    assert numpy.max(numpy.abs(dirty[0]["pixels"].data)), "Image is empty"


def test_invert_wg_psf(result_wg):
    """Test invert_wg for PSF only"""
    vis = result_wg["visibility"]
    model = result_wg["model"]
    persist = result_wg["persist"]
    dirty = invert_wg(vis, model, normalise=True, dopsf=True)

    if persist:
        with tempfile.TemporaryDirectory() as tempdir:
            dirty[0].image_acc.export_to_fits(
                f"{tempdir}/test_imaging_ng_invert_wg_psf_dirty.fits"
            )
    assert numpy.max(numpy.abs(dirty[0]["pixels"].data)), "Image is empty"
