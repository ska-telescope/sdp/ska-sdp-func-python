"""
Fixtures for imaging tests
"""

import numpy
import pytest
from astropy import units as u
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.configuration import create_named_configuration
from ska_sdp_datamodels.science_data_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility

from ska_sdp_func_python.imaging import create_image_from_visibility


@pytest.fixture(scope="package", name="model")
def model_fixt(visibility):
    """
    Model image fixture
    """
    model = create_image_from_visibility(
        visibility,
        npixel=512,
        cellsize=0.0005,
        nchan=visibility.visibility_acc.nchan,
        polarisation_frame=PolarisationFrame("stokesIQUV"),
    )
    return model


@pytest.fixture(scope="module", name="vis_data")
def visibility_fixt(request):
    """Create a Visibility object for testing"""
    params = request.param
    configuration = params.get("configuration", "MID")
    dec = params.get("dec", -35.0)
    rmax = params.get("rmax", 1e3)
    channel_bandwidth = params.get("channel_bandwidth", [1e6])
    freq = params.get("freq", numpy.array([1.3e9]))
    config = create_named_configuration(configuration)
    times = numpy.linspace(-300.0, 300.0, 3) * numpy.pi / 43200.0
    config = create_named_configuration(configuration, rmax=rmax)
    phasecentre = SkyCoord(
        ra=+15 * u.deg, dec=dec * u.deg, frame="icrs", equinox="J2000"
    )
    vis = create_visibility(
        config,
        times,
        freq,
        channel_bandwidth=channel_bandwidth,
        phasecentre=phasecentre,
        weight=1.0,
        polarisation_frame=PolarisationFrame("stokesI"),
    )

    return vis
