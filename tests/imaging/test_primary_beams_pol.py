"""Unit tests for primary beam application with polarisation"""

import numpy
import pytest
from astropy import units as u
from astropy.coordinates import SkyCoord
from numpy.testing import assert_array_almost_equal
from ska_sdp_datamodels.image.image_model import Image
from ska_sdp_datamodels.science_data_model.polarisation_model import (
    PolarisationFrame,
)
from ska_sdp_datamodels.sky_model.sky_model import SkyComponent
from ska_sdp_datamodels.visibility import create_visibility

from ska_sdp_func_python.imaging import (
    create_image_from_visibility,
    dft_skycomponent_visibility,
    idft_visibility_skycomponent,
)
from ska_sdp_func_python.imaging.primary_beams import create_pb, create_vp
from ska_sdp_func_python.sky_component import (
    apply_beam_to_skycomponent,
    apply_voltage_pattern_to_skycomponent,
)

FOV = 8
NPIXEL = 256


@pytest.mark.parametrize("vis_data", [{}], indirect=True)
def test_apply_primary_beam_imageplane(vis_data):
    """Test application of primary beam to image plane"""
    cellsize = numpy.pi * FOV / (NPIXEL * 180.0)
    times = numpy.linspace(-300.0, 300.0, 3) * numpy.pi / 43200.0
    telescope = "MID"
    lflux = numpy.array([[100.0, 1.0, -10.0, +60.0]])
    cflux = numpy.array([[100.0, 60.0, -10.0, +1.0]])
    for flux, vpol in (
        (lflux, PolarisationFrame("linear")),
        (cflux, PolarisationFrame("circular")),
    ):
        bvis = create_visibility(
            config=vis_data.configuration,
            times=times,
            frequency=vis_data["frequency"],
            channel_bandwidth=vis_data["channel_bandwidth"],
            phasecentre=vis_data.phasecentre,
            weight=1.0,
            polarisation_frame=vpol,
            zerow=True,
        )

        component_centre = SkyCoord(
            ra=+15.5 * u.deg,
            dec=-35.0 * u.deg,
            frame="icrs",
            equinox="J2000",
        )
        component = SkyComponent(
            direction=component_centre,
            flux=flux,
            frequency=vis_data["frequency"],
            polarisation_frame=PolarisationFrame("stokesIQUV"),
        )
        model = create_image_from_visibility(
            bvis,
            cellsize=cellsize,
            npixel=NPIXEL,
            override_cellsize=False,
            polarisation_frame=PolarisationFrame("stokesIQUV"),
        )
        pb = create_pb(model, telescope=telescope, use_local=False)
        pbcomp = apply_beam_to_skycomponent(component, pb)
        bvis = dft_skycomponent_visibility(bvis, pbcomp)
        inverse_comp = apply_beam_to_skycomponent(pbcomp, pb, inverse=True)
        assert numpy.array_equal(component.flux, inverse_comp.flux)


@pytest.mark.parametrize(
    "flux",
    [
        numpy.array([[100.0, 0.0, 0.0, 0.0]]),
        numpy.array([[100.0, 100.0, 0.0, 0.0]]),
        numpy.array([[100.0, 0.0, 100.0, 0.0]]),
        numpy.array([[100.0, 0.0, 0.0, 100.0]]),
        numpy.array([[100.0, 1.0, -10.0, +60.0]]),
    ],
)
@pytest.mark.parametrize("vis_data", [{}], indirect=True)
def test_apply_voltage_pattern_dft(vis_data, flux):
    """Test application of voltage pattern to sky component using DFT"""
    telescope = "MID_FEKO_B2"
    times = numpy.linspace(-300.0, 300.0, 3) * numpy.pi / 43200.0
    vpol = PolarisationFrame("linear")
    bvis = create_visibility(
        vis_data.configuration,
        times=times,
        frequency=vis_data["frequency"],
        channel_bandwidth=vis_data["channel_bandwidth"],
        phasecentre=vis_data.phasecentre,
        weight=1.0,
        polarisation_frame=vpol,
    )

    component_centre = SkyCoord(
        ra=+16.0 * u.deg,
        dec=-35.0 * u.deg,
        frame="icrs",
        equinox="J2000",
    )
    component = SkyComponent(
        direction=component_centre,
        flux=flux,
        frequency=vis_data["frequency"],
        polarisation_frame=PolarisationFrame("stokesIQUV"),
    )
    cellsize = numpy.pi * FOV / (NPIXEL * 180.0)
    model = create_image_from_visibility(
        bvis,
        cellsize=cellsize,
        npixel=NPIXEL,
        override_cellsize=False,
        polarisation_frame=PolarisationFrame("stokesIQUV"),
    )
    vpbeam = create_vp(model, telescope=telescope, use_local=False)
    vpbeam_wcs = vpbeam.image_acc.wcs
    vpbeam_wcs.wcs.ctype[0] = "RA---SIN"
    vpbeam_wcs.wcs.ctype[1] = "DEC--SIN"
    vpbeam_wcs.wcs.crval[0] = model.image_acc.wcs.wcs.crval[0]
    vpbeam_wcs.wcs.crval[1] = model.image_acc.wcs.wcs.crval[1]
    vpbeam = Image.constructor(
        data=vpbeam["pixels"],
        polarisation_frame=vpbeam.image_acc.polarisation_frame,
        wcs=vpbeam_wcs,
    )
    assert component.polarisation_frame == PolarisationFrame("stokesIQUV")
    vpcomp = apply_voltage_pattern_to_skycomponent(component, vpbeam)
    assert vpcomp.polarisation_frame == vpbeam.image_acc.polarisation_frame
    bvis = dft_skycomponent_visibility(bvis, vpcomp)
    vpcomp = idft_visibility_skycomponent(bvis, vpcomp)[0][0]
    assert vpcomp.polarisation_frame == bvis.visibility_acc.polarisation_frame
    inv_vpcomp = apply_voltage_pattern_to_skycomponent(
        vpcomp, vpbeam, inverse=True
    )
    assert vpcomp.polarisation_frame == PolarisationFrame("stokesIQUV")
    assert_array_almost_equal(flux, numpy.real(inv_vpcomp.flux), 9)
