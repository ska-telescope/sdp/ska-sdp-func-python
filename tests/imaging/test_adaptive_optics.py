"""Tests for adaptive_optics module"""

import numpy
import pytest

from ska_sdp_func_python.imaging.adaptive_optics import (
    circle,
    zernike_nm,
    zernikeRadialFunc,
    zernIndex,
)


@pytest.mark.parametrize(
    "j, expected",
    [
        (1, [0, 0]),
        (2, [1, 1]),
        (3, [1, -1]),
        (4, [2, 0]),
        (5, [2, -2]),
        (6, [2, 2]),
        (7, [3, -1]),
        (8, [3, 1]),
        (9, [3, -3]),
        (10, [3, 3]),
    ],
)
def test_zernIndex(j, expected):
    """Test Zernike index calculation"""
    assert zernIndex(j) == expected


@pytest.mark.parametrize(
    "radius, size, circle_centre, origin, expected",
    [
        (
            1,
            5,
            (0, 0),
            "middle",
            numpy.array(
                [
                    [0, 0, 0, 0, 0],
                    [0, 0, 1, 0, 0],
                    [0, 1, 1, 1, 0],
                    [0, 0, 1, 0, 0],
                    [0, 0, 0, 0, 0],
                ]
            ),
        ),
        (
            2,
            5,
            (0, 0),
            "middle",
            numpy.array(
                [
                    [0, 0, 1, 0, 0],
                    [0, 1, 1, 1, 0],
                    [1, 1, 1, 1, 1],
                    [0, 1, 1, 1, 0],
                    [0, 0, 1, 0, 0],
                ]
            ),
        ),
        (
            1,
            4,
            (0.5, 0.5),
            "middle",
            numpy.array(
                [[0, 0, 0, 0], [0, 0, 1, 0], [0, 1, 1, 1], [0, 0, 1, 0]]
            ),
        ),
        (
            1,
            4,
            (0.5, 0.5),
            "corner",
            numpy.array(
                [[1, 1, 0, 0], [1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
            ),
        ),
    ],
)
def test_circle(radius, size, circle_centre, origin, expected):
    """Test circle function"""
    result = circle(radius, size, circle_centre, origin)
    assert numpy.array_equal(
        result, expected
    ), f"Expected:\n{expected}\nBut got:\n{result}"


@pytest.mark.parametrize(
    "n, m, r, expected",
    [
        (
            0,
            0,
            numpy.array([[0, 0.5, 1], [0, 0.5, 1], [0, 0.5, 1]]),
            numpy.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]]),
        ),
        (
            1,
            1,
            numpy.array([[0, 0.5, 1], [0, 0.5, 1], [0, 0.5, 1]]),
            numpy.array([[0, 0.5, 1], [0, 0.5, 1], [0, 0.5, 1]]),
        ),
        (
            2,
            0,
            numpy.array([[0, 0.5, 1], [0, 0.5, 1], [0, 0.5, 1]]),
            numpy.array([[-1, -0.5, 1], [-1, -0.5, 1], [-1, -0.5, 1]]),
        ),
        (
            2,
            2,
            numpy.array([[0, 0.5, 1], [0, 0.5, 1], [0, 0.5, 1]]),
            numpy.array([[0, 0.25, 1], [0, 0.25, 1], [0, 0.25, 1]]),
        ),
    ],
)
def test_zernikeRadialFunc(n, m, r, expected):
    """Test Zernike radial function"""
    result = zernikeRadialFunc(n, m, r)
    assert numpy.allclose(
        result, expected
    ), f"Expected:\n{expected}\nBut got:\n{result}"


@pytest.mark.parametrize(
    "n, m, N, expected_shape",
    [
        (0, 0, 5, (5, 5)),
        (1, 1, 5, (5, 5)),
        (2, 0, 5, (5, 5)),
        (2, 2, 5, (5, 5)),
        (3, -3, 5, (5, 5)),
    ],
)
def test_zernike_nm(n, m, N, expected_shape):
    """Test Zernike polynomial creation"""
    result = zernike_nm(n, m, N)
    assert (
        result.shape == expected_shape
    ), f"Expected shape: {expected_shape}, but got: {result.shape}"
    assert numpy.all(
        numpy.isfinite(result)
    ), "Result contains non-finite values"
