"""Unit tests for testing support"""

import logging

import numpy
import pytest

from ska_sdp_func_python.imaging import create_image_from_visibility
from ska_sdp_func_python.imaging.primary_beams import create_vp_generic_numeric

log = logging.getLogger("func-python-logger")
log.setLevel(logging.WARNING)


@pytest.mark.parametrize(
    "vis_data",
    [
        {
            "channel_bandwidth": numpy.array([2.5e7, 2.5e7, 2.5e7]),
            "freq": numpy.linspace(1.4e9, 1.5 * 1.4e9, 3),
        }
    ],
    indirect=True,
)
def test_create_voltage_patterns_zernike(vis_data):
    """Test creation of voltage patterns with Zernike polynomials"""
    cellsize = 8 * numpy.pi / 180.0 / 280
    model = create_image_from_visibility(
        vis_data, npixel=512, cellsize=cellsize, override_cellsize=False
    )
    for noll in range(1, 50):
        zernikes = [{"coeff": 1.0, "noll": noll}]
        vp = create_vp_generic_numeric(
            model,
            pointingcentre=None,
            diameter=15.0,
            blockage=0.0,
            taper="gaussian",
            edge=0.03162278,
            zernikes=zernikes,
            padding=2,
            use_local=True,
        )
        vp_data = vp["pixels"].data
        vp["pixels"].data = numpy.real(vp_data)
        if noll == 1:
            assert numpy.isclose(
                numpy.sum(vp["pixels"].data), 5835.536099443175
            )
        elif noll == 49:
            assert numpy.isclose(
                numpy.sum(vp["pixels"].data), 17236.722597365497
            )
