"""Unit tests for testing support"""

import numpy
import pytest
from ska_sdp_datamodels.science_data_model.polarisation_model import (
    PolarisationFrame,
)

from ska_sdp_func_python.image.operations import scale_and_rotate_image
from ska_sdp_func_python.imaging import (
    convert_azelvp_to_radec,
    create_image_from_visibility,
    create_low_test_vp,
    create_mid_allsky,
    create_pb,
    create_vp,
)

FOV = 8
NPIXEL = 1024


def check_max_min(im, flux_max, flux_min, context):
    """
    Check the maximum and minimum of an image
    """
    qa = im.image_acc.qa_image()
    numpy.testing.assert_allclose(
        qa.data["max"], flux_max, atol=1e-7, err_msg=f"{context} {qa}"
    )
    numpy.testing.assert_allclose(
        qa.data["min"], flux_min, atol=1e-7, err_msg=f"{context} {qa}"
    )


@pytest.mark.parametrize(
    "telescope, flux_max, flux_min",
    [
        ("VLA", 0.9896588738559976, 0.0),
        ("ASKAP", 0.9861593364197507, 0.0),
        ("MID", 1.0, 0.0),
        ("LOW", 1.0, 0.0),
    ],
)
@pytest.mark.parametrize("vis_data", [{}], indirect=True)
def test_create_primary_beams_RADEC(vis_data, telescope, flux_max, flux_min):
    """Test creation of primary beams"""
    cellsize = numpy.pi * FOV / (NPIXEL * 180.0)
    model = create_image_from_visibility(
        vis=vis_data,
        cellsize=cellsize,
        npixel=NPIXEL,
        override_cellsize=False,
    )
    beam = create_pb(model, telescope=telescope, use_local=False)
    assert numpy.max(beam["pixels"].data) > 0.0
    check_max_min(beam, flux_max, flux_min, telescope)


@pytest.mark.parametrize(
    "telescope, flux_max, flux_min",
    [
        ("VLA", 0.9896588738559976, 0.0),
        ("ASKAP", 0.9861593364197507, 0.0),
        ("MID", 1.0, 0.0),
        ("MID_GAUSS", 1.0, 0.0),
        ("MID_FEKO_B1", 1.0, 0.0),
        ("MID_FEKO_B2", 1.0, 0.0),
        ("MID_FEKO_Ku", 1.0, 0.0),
        ("LOW", 1.0, 0.0),
    ],
)
@pytest.mark.parametrize("vis_data", [{}], indirect=True)
def test_create_primary_beams_AZELGEO(vis_data, telescope, flux_max, flux_min):
    """Test creation of primary beams"""
    cellsize = numpy.pi * FOV / (NPIXEL * 180.0)
    model = create_image_from_visibility(
        vis=vis_data,
        cellsize=cellsize,
        npixel=NPIXEL,
        override_cellsize=False,
    )
    beam = create_pb(model, telescope=telescope, use_local=True)
    check_max_min(beam, flux_max, flux_min, telescope)


@pytest.mark.parametrize(
    "telescope, flux_max, flux_min",
    [
        ("VLA", 0.9948159999999988 + 0j, -0.13737537430017216 + 0j),
        ("ASKAP", 0.9930555555555544 + 0j, -0.13906616397447308 + 0j),
        ("LOW", 0.9999999999999988 + 0j, -0.132279301631194 + 0j),
    ],
)
@pytest.mark.parametrize("vis_data", [{}], indirect=True)
def test_create_voltage_patterns(vis_data, telescope, flux_max, flux_min):
    """Test creation of voltage patterns"""

    cellsize = numpy.pi * FOV / (NPIXEL * 180.0)
    model = create_image_from_visibility(
        vis=vis_data,
        cellsize=cellsize,
        npixel=NPIXEL,
        override_cellsize=False,
    )
    beam = create_vp(model, telescope=telescope)
    assert numpy.max(numpy.abs(beam["pixels"].data.real)) > 0.0, telescope
    assert numpy.max(numpy.abs(beam["pixels"].data.imag)) < 1e-15, numpy.max(
        numpy.abs(beam["pixels"].data.imag)
    )
    check_max_min(beam, flux_max, flux_min, telescope)


@pytest.mark.parametrize("vis_data", [{}], indirect=True)
def test_create_voltage_patterns_MID_GAUSS(vis_data):
    """Test creation of voltage patterns"""
    cellsize = numpy.pi * FOV / (NPIXEL * 180.0)
    model = create_image_from_visibility(
        vis=vis_data,
        npixel=NPIXEL,
        cellsize=cellsize,
        override_cellsize=False,
    )
    telescope = "MID_GAUSS"
    beam = create_vp(model, telescope=telescope, padding=4)
    beam_data = beam["pixels"].data
    beam["pixels"].data = numpy.real(beam_data)
    check_max_min(beam, 1.0, -0.04908413672703686, telescope)
    beam["pixels"].data = numpy.imag(beam_data)
    check_max_min(beam, 0.0, 0.0, telescope)


@pytest.mark.parametrize("vis_data", [{}], indirect=True)
def test_create_voltage_pattern_MID_allsky(vis_data):
    """Test creation of voltage patterns"""
    telescope = "MID_GAUSS"
    beam = create_mid_allsky(frequency=vis_data["vis"].frequency)

    beam_data = beam["pixels"].data
    beam["pixels"].data = numpy.real(beam_data)
    check_max_min(beam, 1.0, -0.13220304339601227, telescope)

    beam["pixels"].data = numpy.imag(beam_data)
    check_max_min(beam, 0.0, 0.0, telescope)


@pytest.mark.parametrize(
    "telescope, flux_max, flux_min",
    [
        ("MID", 1.0, -0.13227948455466312),
        ("MID_FEKO_B1", 1.0, -0.12867465048776316),
        ("MID_FEKO_B2", 0.9999995585656044, -0.14500584762256105),
        ("MID_FEKO_Ku", 1.0, -0.1162932799217111),
        ("MEERKAT_B2", 1.0, -0.09590146287902526),
    ],
)
@pytest.mark.parametrize("vis_data", [{}], indirect=True)
def test_create_voltage_patterns_MID(vis_data, telescope, flux_max, flux_min):
    """Test creation of voltage patterns"""
    cellsize = numpy.pi * FOV / (NPIXEL * 180.0)
    model = create_image_from_visibility(
        vis=vis_data,
        npixel=NPIXEL,
        cellsize=cellsize,
        override_cellsize=False,
    )

    beam = create_vp(model, telescope=telescope, padding=4)
    beam_data = beam["pixels"].data
    beam["pixels"].data = numpy.real(beam_data)
    beam.image_acc.wcs.wcs.crval[0] = 0.0
    beam.image_acc.wcs.wcs.crval[1] = 90.0

    check_max_min(beam, flux_max, flux_min, telescope)


@pytest.mark.parametrize(
    "telescope, flux_max, flux_min",
    [
        ("MID_FEKO_B1", 1.000088108038104, -0.1286805307650631),
        ("MID_FEKO_B2", 1.0000627107836275, -0.1450148783145079),
        ("MID_FEKO_Ku", 0.9999126796425732, -0.11606096603175388),
    ],
)
@pytest.mark.parametrize("vis_data", [{"freq": [1.4e9]}], indirect=True)
def test_create_voltage_patterns_MID_rotate(
    vis_data, telescope, flux_max, flux_min
):
    """Test creation of voltage patterns"""
    cellsize = numpy.pi * FOV / (NPIXEL * 180.0)
    model = create_image_from_visibility(
        vis=vis_data,
        npixel=NPIXEL,
        cellsize=cellsize,
        polarisation_frame=PolarisationFrame("stokesIQUV"),
        override_cellsize=False,
    )

    beam = create_vp(telescope=telescope)
    beam = scale_and_rotate_image(beam, scale=[1.2, 0.8])

    beam_radec = convert_azelvp_to_radec(beam, model, numpy.pi / 4.0)

    beam_data = beam_radec["pixels"].data
    beam_radec["pixels"].data = numpy.real(beam_data)
    check_max_min(beam_radec, flux_max, flux_min, telescope)


@pytest.mark.parametrize(
    "az, el, flux_max, flux_min",
    [
        (60.0, 45.0, 0.9999999999999988 + 0j, -0.13227948739290077 + 0j),
        (-60.0, 45.0, 0.9999999999999988 + 0j, -0.13227948739290077 + 0j),
    ],
)
@pytest.mark.parametrize("vis_data", [{"freq": [1.0e8]}], indirect=True)
def test_create_voltage_patterns_LOW(vis_data, az, el, flux_max, flux_min):
    """Test creation of voltage patterns"""
    cellsize = numpy.pi * FOV / (NPIXEL * 180.0)
    model = create_image_from_visibility(
        vis=vis_data,
        npixel=NPIXEL,
        cellsize=cellsize * 10.0,
        polarisation_frame=PolarisationFrame("stokesIQUV"),
        override_cellsize=False,
    )
    beam = create_low_test_vp(
        model,
        use_local=False,
        azel=(numpy.deg2rad(az), numpy.deg2rad(el)),
    )
    check_max_min(beam, flux_max, flux_min, f"{az} {el}")
