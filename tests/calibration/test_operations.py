"""
Unit tests for calibration operations
"""

import numpy
import pytest
from ska_sdp_datamodels.calibration.calibration_create import (
    create_gaintable_from_visibility,
)
from ska_sdp_datamodels.science_data_model.polarisation_model import (
    PolarisationFrame,
)
from ska_sdp_datamodels.visibility.vis_create import create_visibility

from ska_sdp_func_python.calibration.operations import (
    apply_gaintable,
    concatenate_gaintables,
    multiply_gaintables,
)


@pytest.fixture(name="second_vis")
def second_vis_fxt(visibility):
    """
    Visibility object with weight=2.0 based on the
    visibility fixture
    """
    vis = visibility.copy(deep=True)
    return create_visibility(
        vis.attrs["configuration"],
        # times is same as times for visibility fixture
        # unfortunately create_visibility doesn't pass the times as is,
        # but converts them so cannot take it directly from visibility
        times=(numpy.pi / 43200.0) * numpy.linspace(0.0, 300.0, 2),
        frequency=vis.coords["frequency"].data,
        channel_bandwidth=vis["channel_bandwidth"].data,
        polarisation_frame=PolarisationFrame(vis.attrs["_polarisation_frame"]),
        phasecentre=vis.attrs["phasecentre"],
        weight=2.0,
    )


@pytest.mark.parametrize("visibility", ["linear", "stokesI"], indirect=True)
def test_apply_gaintable(visibility, second_vis):
    """
    Unit test for the apply_gaintable function, using flags
    TODO: The property of use_flags is not properly tested,
          need to revisit this test

    Running with stokesI tests that npol=1 and nchan>1 doesn't fail.
    There was an IndexError previously due to a bug in that branch
    (smueller1 array was not indexed correctly)
    """
    vis = visibility.copy(deep=True)
    vis2 = second_vis.copy(deep=True)
    gain_table = create_gaintable_from_visibility(vis2)

    result = apply_gaintable(
        vis,
        gain_table,
        use_flags=True,
    )
    assert (result["vis"].data == vis2["vis"].data).all()
    assert (result["weight"].data == vis2["weight"].data / 2).all()


def test_multiply_gaintables(visibility, second_vis):
    """
    Unit test for the multiply_gaintable function
    """
    gt = create_gaintable_from_visibility(  # pylint: disable=invalid-name
        second_vis.copy(deep=True)
    )
    dgt = create_gaintable_from_visibility(visibility.copy(deep=True))

    result = multiply_gaintables(gt, dgt)

    assert (
        result["gain"].data
        == numpy.einsum(
            "...ik,...ij->...kj", gt["gain"].data, dgt["gain"].data
        )
    ).all()
    assert (
        result["weight"].data == (gt["weight"].data * dgt["weight"].data)
    ).all()


def test_concatenate_gaintables(visibility, second_vis):
    """
    Unit test for the multiply_gaintable function
    """

    gt_list = [
        create_gaintable_from_visibility(  # pylint: disable=invalid-name
            second_vis.copy(deep=True)
        ),
        create_gaintable_from_visibility(visibility.copy(deep=True)),
    ]

    result = concatenate_gaintables(gt_list)

    assert len(result) == len(gt_list[0])
    assert len(result) == len(gt_list[1])
