"""
Unit tests for the alternative calibration solvers
These solvers will typically be called via solve_gaintable
"""

import numpy
import pytest
from ska_sdp_datamodels.calibration.calibration_create import (
    create_gaintable_from_visibility,
)

from ska_sdp_func_python.calibration.alternative_solvers import (
    solve_with_alternative_algorithm,
)
from tests.testing_utils import (
    simulate_gaintable,
    vis_with_component_data,
    vis_with_widefield_components,
)


def apply_gaintable_local(vis, gain):
    """
    Temporary local version of operations.apply_gaintable.
    The main version doesn't seem to be transposing the rightmost Jones matrix.

    Does not properly handle sub-bands and partial solution intervals
    """

    ntimes, nbaselines, nfrequency, npol = vis.vis.shape
    assert npol == 4

    # pylint: disable=too-many-function-args
    vis.vis.data = numpy.einsum(
        "tbfpi,tbfij,tbfqj->tbfpq",
        gain.gain.data[:, vis.antenna1.data],
        vis.vis.data.reshape(ntimes, nbaselines, nfrequency, 2, 2),
        gain.gain.data[:, vis.antenna2.data].conj(),
    ).reshape(ntimes, nbaselines, nfrequency, npol)

    return vis


def rotate_time_steps(vis):
    """
    Rotate each time step to a different parallactic angle.

    A single polarised source at a single parallactic angle cannot
    fully constrain the polarised problem.
    """

    # rotate each time step to a different parallactic angle
    ntimes, nbaselines, nfrequency, npol = vis.vis.shape
    assert npol == 4

    for time in range(ntimes):
        phi = time / ntimes * numpy.pi
        rot = numpy.array(
            [
                [numpy.cos(phi), numpy.sin(phi)],
                [-numpy.sin(phi), numpy.cos(phi)],
            ]
        )
        # pylint: disable=too-many-function-args
        vis.vis.data[time] = numpy.einsum(
            "pi,bfij,qj->bpq",
            rot,
            vis.vis.data[time].reshape(nbaselines, nfrequency, 2, 2),
            rot,
        ).reshape(nbaselines, nfrequency, npol)


@pytest.mark.parametrize(
    "sky_pol_frame, data_pol_frame, flux_array",
    [
        ("stokesIQUV", "linear", [100.0, 50.0, 10.0, -20.0]),
    ],
)
def test_pol_rotation(sky_pol_frame, data_pol_frame, flux_array):
    """
    Test solve_with_alternative_algorithm a polarised source observed at
    multiple parallactic angles. A single polarised source at a single
    parallactic angle cannot fully constrain the polarised problem.
    """
    jones_type = "G"

    numpy.random.seed(int(1e8))

    # generate visibilties at multiple times containing a polarised source
    vis = vis_with_component_data(sky_pol_frame, data_pol_frame, flux_array)

    # rotate each time step to a different parallactic angle
    rotate_time_steps(vis)

    # set the calibration interval to include all pol angle rotations
    full_interval = numpy.max(vis.time.data) - numpy.min(vis.time.data)
    gain_table = create_gaintable_from_visibility(
        vis, jones_type=jones_type, timeslice=full_interval
    )
    assert len(gain_table.gain.time) == 1

    # copy the default table for use as the model before generating gains
    default_table = gain_table.copy(deep=True)
    gain_table = simulate_gaintable(
        gain_table,
        phase_error=0.1,
        amplitude_error=0.1,
        leakage=0.2,
    )

    # copy the default vis for use as the model before applying gains
    original = vis.copy(deep=True)
    vis = apply_gaintable_local(vis, gain_table)

    # phase referencing is not currently enabled, since it can break some of
    # the polarised constraints. Here we expect a full phase lock between
    # all of the unknown parameters, so can simple phase reference against
    # a single Jones matrix element of a single antenna
    refant = 3
    ref0 = numpy.angle(gain_table.gain.data[0, refant, 0, 0, 0])

    # test using the jones_substitution algorithm
    #  - it is fast but not quite as accurate in the polarised case
    result_gain_table = default_table.copy(deep=True)
    solve_with_alternative_algorithm(
        "jones_substitution",
        vis,
        original,
        result_gain_table,
        niter=100,
        row=0,
        tol=1e-6,
    )
    ref1 = numpy.angle(result_gain_table.gain.data[0, refant, 0, 0, 0])
    assert (
        numpy.max(
            numpy.abs(
                result_gain_table.gain.data * numpy.exp(-1j * ref1)
                - gain_table.gain.data * numpy.exp(-1j * ref0)
            )
        )
        < 1e-5
    )

    # test using the normal_equations algorithm
    #  - usually very accruate, but for performance reasons there is an
    #    average over all times and frequencies in the solution interval.
    #    This does not typically matter for short intervals, but suffers
    #    from depolarisation in this test because of the artificial pol
    #    angle rotation between time steps.
    result_gain_table = default_table.copy(deep=True)
    solve_with_alternative_algorithm(
        "normal_equations",
        vis,
        original,
        result_gain_table,
        niter=100,
        row=0,
        tol=1e-6,
    )
    ref1 = numpy.angle(result_gain_table.gain.data[0, refant, 0, 0, 0])
    assert (
        numpy.max(
            numpy.abs(
                result_gain_table.gain.data * numpy.exp(-1j * ref1)
                - gain_table.gain.data * numpy.exp(-1j * ref0)
            )
        )
        < 1e-1
    )

    # test using the normal_equations_presum algorithm
    #  - accruate but slow. This algorithm is optimised for large datasets
    #    and large solution intervals.
    result_gain_table = default_table.copy(deep=True)
    solve_with_alternative_algorithm(
        "normal_equations_presum",
        vis,
        original,
        result_gain_table,
        niter=100,
        row=0,
        tol=1e-6,
    )
    ref1 = numpy.angle(result_gain_table.gain.data[0, refant, 0, 0, 0])
    assert (
        numpy.max(
            numpy.abs(
                result_gain_table.gain.data * numpy.exp(-1j * ref1)
                - gain_table.gain.data * numpy.exp(-1j * ref0)
            )
        )
        < 1e-11
    )


def test_widefield():
    """
    Test solve_with_alternative_algorithm with a wide field of unpolarised
    sources. The instrumental polarisation helps to constrain the unknowns.
    """
    jones_type = "G"

    numpy.random.seed(int(1e8))

    vis = vis_with_widefield_components(nchan=4)

    # set the calibration interval to include all pol angle rotations
    full_interval = numpy.max(vis.time.data) - numpy.min(vis.time.data)
    gain_table = create_gaintable_from_visibility(
        vis, jones_type=jones_type, timeslice=full_interval
    )
    assert len(gain_table.gain.time) == 1

    # copy the default table for use as the model before generating gains
    default_table = gain_table.copy(deep=True)
    gain_table = simulate_gaintable(
        gain_table,
        phase_error=0.1,
        amplitude_error=0.1,
        leakage=0.2,
    )

    # copy the default vis for use as the model before applying gains
    original = vis.copy(deep=True)
    vis = apply_gaintable_local(vis, gain_table)

    # phase referencing is not currently enabled, since it can break some of
    # the polarised constraints. Here we expect a full phase lock between
    # all of the unknown parameters, so can simple phase reference against
    # a single Jones matrix element of a single antenna
    refant = 3
    ref0 = numpy.angle(gain_table.gain.data[0, refant, 0, 0, 0])

    # test using the jones_substitution algorithm
    #  - it is fast but not quite as accurate in the polarised case
    result_gain_table = default_table.copy(deep=True)
    solve_with_alternative_algorithm(
        "jones_substitution",
        vis,
        original,
        result_gain_table,
        niter=100,
        row=0,
        tol=1e-4,
    )
    ref1 = numpy.angle(result_gain_table.gain.data[0, refant, 0, 0, 0])
    assert (
        numpy.max(
            numpy.abs(
                result_gain_table.gain.data * numpy.exp(-1j * ref1)
                - gain_table.gain.data * numpy.exp(-1j * ref0)
            )
        )
        < 1e-1
    )

    # test using the normal_equations algorithm
    #  - accruate and faster than the pre-sum version for short soln intervals
    result_gain_table = default_table.copy(deep=True)
    solve_with_alternative_algorithm(
        "normal_equations",
        vis,
        original,
        result_gain_table,
        niter=100,
        row=0,
        tol=1e-6,
    )
    ref1 = numpy.angle(result_gain_table.gain.data[0, refant, 0, 0, 0])
    assert (
        numpy.max(
            numpy.abs(
                result_gain_table.gain.data * numpy.exp(-1j * ref1)
                - gain_table.gain.data * numpy.exp(-1j * ref0)
            )
        )
        < 1e-12
    )

    # test using the normal_equations_presum algorithm
    #  - accruate but slow. This algorithm is optimised for large datasets
    #    and large solution intervals.
    result_gain_table = default_table.copy(deep=True)
    solve_with_alternative_algorithm(
        "normal_equations_presum",
        vis,
        original,
        result_gain_table,
        niter=100,
        row=0,
        tol=1e-6,
    )
    ref1 = numpy.angle(result_gain_table.gain.data[0, refant, 0, 0, 0])
    assert (
        numpy.max(
            numpy.abs(
                result_gain_table.gain.data * numpy.exp(-1j * ref1)
                - gain_table.gain.data * numpy.exp(-1j * ref0)
            )
        )
        < 1e-9
    )
