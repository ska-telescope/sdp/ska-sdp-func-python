"""
Test download FITS from Gitlab
"""

import os
import tempfile

from ska_sdp_func_python.util.file_download import download_with_retry


def test_download_fits_file():
    """Test download FITS from Gitlab"""

    with tempfile.TemporaryDirectory() as temp_dir:
        url = "https://gitlab.com/ska-telescope/external/rascil-main/"
        url += "-/raw/master/data/models/SKA1_LOW_beam.fits?ref_type=heads"

        local_file = os.path.join(temp_dir, "SKA1_LOW_beam.fits")
        download_with_retry(url, local_file)
        assert os.path.exists(local_file)
